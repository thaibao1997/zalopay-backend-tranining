/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.database;

import baolt.hotel.api.dto.Checkout;
import static baolt.hotel.api.util.APIHelper.convertDateToTimestamp;
import static baolt.hotel.api.util.APIHelper.convertTimestampToDate;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author baolt
 */
public class CheckoutDB {

    private static final String TABLE_NAME = "CheckOut";
    private static final String BOOKING_ID_COL = "bookingId";
    private static final String ROOM_ID_COL = "roomId";
    private static final String CHECK_OUT_DATE_COL = "checkOutDate";
    private static final String TOTAL_AMOUNT_COL = "totalAmount";

    private DataSource dataSource = null;

    public CheckoutDB() {
        dataSource = DataSource.getInstance();

    }

    private Checkout resultSetToCheckout(ResultSet rs) throws SQLException {
        Checkout checkout = new Checkout();
        checkout.setBookingId(rs.getString(BOOKING_ID_COL));
        checkout.setRoomId(rs.getString(ROOM_ID_COL));
        Timestamp date = rs.getTimestamp(CHECK_OUT_DATE_COL);
        checkout.setCheckOutDate(convertTimestampToDate(date));
        checkout.setTotalAmount(rs.getLong(TOTAL_AMOUNT_COL));
        return checkout;
    }

    public ArrayList<Checkout> getAllCheckout() throws SQLException {
        ArrayList<Checkout> checkouts = new ArrayList<>();

        try (Connection conn = dataSource.getConnection()){
            String query = "Select * from " +TABLE_NAME;
            PreparedStatement st = conn.prepareStatement(query);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Checkout checkout = resultSetToCheckout(rs);
                checkouts.add(checkout);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CheckoutDB.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
        return checkouts;
    }

    public Checkout getCheckout(String bookingId, String roomId) throws SQLException {
        Checkout checkout = null;
        try (Connection conn = dataSource.getConnection()){
            String query = "Select * from " +TABLE_NAME
                    + " Where bookingId Like ? and roomId like ?";
            PreparedStatement st = conn.prepareStatement(query);
            st.setString(1, bookingId);
            st.setString(2, roomId);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                checkout =resultSetToCheckout(rs);
                return checkout;

            }
        } catch (SQLException ex) {
            Logger.getLogger(CheckoutDB.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
        return checkout;
    }

    public boolean deleteCheckout(String bookingId, String roomId) throws SQLException {
        try (Connection conn = dataSource.getConnection()){
            String query = "delete from "+TABLE_NAME+"  Where bookingId Like ? and roomId like ?";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, bookingId);
            pst.setString(2, roomId);
            return pst.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(CheckoutDB.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }

    }

    public boolean addCheckout(Checkout checkout) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {

            String query = "insert into "+TABLE_NAME+" values (?,?,?,?)";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, checkout.getBookingId());
            pst.setString(2, checkout.getRoomId());
            pst.setLong(3, checkout.getTotalAmount());
            Date date = checkout.getCheckOutDate();
            pst.setTimestamp(4, convertDateToTimestamp(date));

            return pst.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(CheckoutDB.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }

    }

    public boolean updateCheckout(Checkout checkout) throws SQLException {
        try(Connection conn = dataSource.getConnection()) {
            String query = "update "+TABLE_NAME+" set totalAmount = ?, checkOutDate = ? "
                    + "Where bookingId Like ? and roomId like ?";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setLong(1, checkout.getTotalAmount());
            Date date = checkout.getCheckOutDate();
            pst.setTimestamp(2, convertDateToTimestamp(date));
            pst.setString(3, checkout.getBookingId());
            pst.setString(4, checkout.getRoomId());
            return pst.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(CheckoutDB.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }

    }

}
