/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.dto;

/**
 *
 * @author baolt
 */
public enum RoomType {
    SINGLE, DOUBLE, THREE, TAPTHE;

    public static RoomType fromInt(int num) {
        switch (num) {
            case 1:
                return SINGLE;
            case 2:
                return DOUBLE;
            case 3:
                return THREE;
            case 4:
                return TAPTHE;
        }
        return null;
    }

    public String toString() {
        switch (this) {
            case SINGLE:
                return "Single";
            case DOUBLE:
                return "Dobule";
            case THREE:
                return "three";
            case TAPTHE:
                return "Tap The";
        }
        return "";
    }
    
    public int toInt(){
        switch (this) {
            case SINGLE:
                return 1;
            case DOUBLE:
                return 2;
            case THREE:
                return 3;
            case TAPTHE:
                return 4;
        }
        return 0;
    }

}
