/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.database;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.ini4j.Wini;

/**
 *
 * @author baolt
 */
public class DataSource {

    private static String DRIVER_NAME = "com.mysql.cj.jdbc.Driver";

    private static String DEFAULT_MAIN_DB_NAME = "hotel_api";
    private static String DEFAULT_TEST_DB_NAME = "hotel_api_test";

    private static String HOST_DB = "localhost";
    private static int PORT_DB = 3306;
    private static String DB_NAME = "hotel_api";
    private static String USERNAME_DB = "root";
    private static String PASSWORD_DB = "123456";

    private static final String INI_FILE = "config/config.ini";
    private static final String SECTION_NAME = "database";
    private static final String TEST_SECTION_NAME = "test_database";

    static {
        initConfigFile();
        swicthToMainDatabase();
    }

    private static DataSource instance = new DataSource();

    public static DataSource getInstance() {
        return instance;
    }

    public static void initConfigFile() {
        File iniFile = new File(INI_FILE);
        if (!iniFile.exists()) {
            try {
                File configFolder = new File("config");
                configFolder.mkdir();
                iniFile.createNewFile();
                Wini ini = new Wini(iniFile);

                ini.put(SECTION_NAME, "HOST_DB", HOST_DB);
                ini.put(SECTION_NAME, "PORT_DB", PORT_DB);
                ini.put(SECTION_NAME, "DB_NAME", DEFAULT_MAIN_DB_NAME);
                ini.put(SECTION_NAME, "USERNAME_DB", USERNAME_DB);
                ini.put(SECTION_NAME, "PASSWORD_DB", PASSWORD_DB);

                ini.put(TEST_SECTION_NAME, "HOST_DB", HOST_DB);
                ini.put(TEST_SECTION_NAME, "PORT_DB", PORT_DB);
                ini.put(TEST_SECTION_NAME, "DB_NAME", DEFAULT_TEST_DB_NAME);
                ini.put(TEST_SECTION_NAME, "USERNAME_DB", USERNAME_DB);
                ini.put(TEST_SECTION_NAME, "PASSWORD_DB", PASSWORD_DB);

                ini.store();
            } catch (IOException ex) {
                Logger.getLogger(DataSource.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }

    public static void readConfigFile(boolean isTest) {
        File iniFile = new File(INI_FILE);
        if (iniFile.exists()) {
            try {
                String section = SECTION_NAME;
                if (isTest) {
                    section = TEST_SECTION_NAME;
                }
                Wini ini = new Wini(iniFile);
                HOST_DB = ini.get(section, "HOST_DB", String.class);
                PORT_DB = ini.get(section, "PORT_DB", int.class);
                DB_NAME = ini.get(section, "DB_NAME", String.class);
                USERNAME_DB = ini.get(section, "USERNAME_DB", String.class);
                PASSWORD_DB = ini.get(section, "PASSWORD_DB", String.class);
            } catch (IOException ex) {
                Logger.getLogger(DataSource.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void swicthToMainDatabase() {
        readConfigFile(false);
        if (instance != null) {

            instance.cpds.close();
        }
        instance = new DataSource();

    }

    public static void switchToTestDatabase() {
        readConfigFile(true);
        if (instance != null) {
            instance.cpds.close();
        }
        instance = new DataSource();

    }

    //NON_STATIC
    private ComboPooledDataSource cpds;

    private DataSource() {

        cpds = new ComboPooledDataSource();
        try {
            cpds.setDriverClass(DRIVER_NAME);

            cpds.setJdbcUrl("jdbc:mysql://" + HOST_DB + "/" + DB_NAME);
            cpds.setUser(USERNAME_DB);
            cpds.setPassword(PASSWORD_DB);

            cpds.setMinPoolSize(5);
            cpds.setAcquireIncrement(5);
            cpds.setMaxPoolSize(20);
            cpds.setMaxStatements(180);

        } catch (PropertyVetoException ex) {
            Logger.getLogger(DataSource.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Connection getConnection() throws SQLException {
        return cpds.getConnection();
    }

    public void dropAllData() {
        try (Connection connection = getConnection()) {

            Statement st = connection.createStatement();
            String query = "DELETE FROM CheckOut WHERE True";
            st.execute(query);

            st = connection.createStatement();
            query = "DELETE FROM Charging WHERE True";
            st.execute(query);

            st = connection.createStatement();
            query = "DELETE FROM Menu WHERE True";
            st.execute(query);

            st = connection.createStatement();
            query = "DELETE FROM Booking WHERE True";
            st.execute(query);

            st = connection.createStatement();
            query = "DELETE FROM Room WHERE True";
            st.execute(query);

            st = connection.createStatement();
            query = "DELETE FROM Customer WHERE True";
            st.execute(query);

        } catch (SQLException ex) {
            Logger.getLogger(DataSource.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
