/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.app.model;

import baolt.hotel.api.grpc.charging.ChargingServicesGrpc;
import baolt.hotel.api.grpc.charging.GetPaymentStatusParams;
import baolt.hotel.api.grpc.checkout.CheckoutMsg;
import static baolt.hotel.app.model.CheckoutModel.convertCheckoutMsgToJSON;
import static baolt.hotel.app.model.ModelHelper.createErrorJSONObject;
import io.grpc.ManagedChannel;
import java.util.Iterator;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class ChargingModel {

    private final ManagedChannel channel;
    private ChargingServicesGrpc.ChargingServicesBlockingStub blockingStub;

    public ChargingModel() {
        this.channel = ModelHelper.getChannel();
        this.blockingStub = ChargingServicesGrpc.newBlockingStub(channel);
    }

    public JSONObject getPaymentStatus(String chargingId) {
        if (channel.isShutdown() || channel.isTerminated()) {
            return createErrorJSONObject("API Server died");
        }
        GetPaymentStatusParams params = GetPaymentStatusParams
                .newBuilder()
                .setChargeId(chargingId)
                .build();
        try {
            CheckoutMsg reply = blockingStub.getPaymentStatus(params);
            return convertCheckoutMsgToJSON(reply);

        } catch (Exception ex) {
            return createErrorJSONObject(ex.getMessage());
        }
    }
}
