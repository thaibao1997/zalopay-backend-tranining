/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.dto;

import baolt.hotel.api.util.APIHelper;
import static baolt.hotel.api.util.APIHelper.getDateFromStr;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class Customer implements JSONable , Serializable{

    String customerId;
    String fullName;
    String address;
    Date dayOfBirth;
    String phoneNumber;
    Date createDate;

    public Customer() {
        createDate = new Date();
    }

    public String getCustomerId() {
        return customerId.toUpperCase();
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getDayOfBirth() {
        return dayOfBirth;
    }

    public void setDayOfBirth(Date dayOfBirth) {
        this.dayOfBirth = dayOfBirth;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    @Override
    public JSONObject toJsonObject() {
        JSONObject jsonO = new JSONObject(this);
        jsonO.put("createDate", APIHelper.getDateStr(this.createDate));
        jsonO.put("dayOfBirth", APIHelper.getShortDateStr(this.dayOfBirth));
        return jsonO;
    }

    @Override
    public void fromJsonString(String jsonStr) throws JSONException, IllegalArgumentException {
        JSONObject jsonObject = new JSONObject(jsonStr);
        String id = jsonObject.getString("customerId");
        String name = jsonObject.getString("fullName");
        String address = jsonObject.getString("address");
        String doBStr = jsonObject.getString("dayOfBirth");
        Date doB = getDateFromStr(doBStr);
        String phoneN = jsonObject.getString("phoneNumber");
        if (jsonObject.has("createDate")) {
            String createStr = jsonObject.getString("createDate");
            Date dateTime = getDateFromStr(createStr);
            this.setCreateDate(dateTime);
        }else{
            this.setCreateDate(new Date());
        }

        this.setCustomerId(id);
        this.setFullName(name);
        this.setAddress(address);
        this.setDayOfBirth(doB);
        this.setPhoneNumber(phoneN);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash + Objects.hashCode(this.customerId);
        hash = 59 * hash + Objects.hashCode(this.fullName);
        hash = 59 * hash + Objects.hashCode(this.address);
        hash = 59 * hash + Objects.hashCode(this.dayOfBirth);
        hash = 59 * hash + Objects.hashCode(this.phoneNumber);
        hash = 59 * hash + Objects.hashCode(this.createDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Customer other = (Customer) obj;
        if (!Objects.equals(this.customerId, other.customerId)) {
            return false;
        }
        if (!Objects.equals(this.fullName, other.fullName)) {
            return false;
        }
        if (!Objects.equals(this.address, other.address)) {
            return false;
        }
        if (!Objects.equals(this.phoneNumber, other.phoneNumber)) {
            return false;
        }
         if ( Math.abs(this.dayOfBirth.getTime() - other.dayOfBirth.getTime()) > 1000) {
            return false;
        }
         if ( Math.abs(this.createDate.getTime() - other.createDate.getTime()) > 1000) {
            return false;
        }
        return true;
    }

}
