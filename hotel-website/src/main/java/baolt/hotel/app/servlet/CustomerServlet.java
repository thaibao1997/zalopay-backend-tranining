/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.app.servlet;

import baolt.hotel.app.model.CustomerModel;
import baolt.hotel.app.model.RoomModel;
import static baolt.hotel.app.utils.ServerHelper.*;
import baolt.hotel.app.view.CustomerView;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class CustomerServlet extends RestHttpServlet {
    
    CustomerModel customerModel = new CustomerModel();

    @Override
    protected void doPatch(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String id = req.getParameter("id");
            if (id == null || id.isEmpty()) {
                resp.sendError(400);
                return;
            }
            String reqBody = getRequestStr(req);
            JSONObject jsonObject = new JSONObject(reqBody);
            System.out.println("Client update customer: " + jsonObject.toString());
            String res = customerModel.updateCustomer(jsonObject).toString();
            resp.getWriter().print(res);
        } catch (Exception e) {
            resp.sendError(500);

        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        if (id == null || id.isEmpty()) {
            resp.sendError(400);
            return;
        }
        try {
            String res = customerModel.deleteCustomer(id).toString();
            resp.getWriter().print(res);
        } catch (Exception e) {
            resp.sendError(500);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String reqBody = getRequestStr(req);
        System.out.println("Client add new customer: " + reqBody);
        JSONObject jsonObject = new JSONObject(reqBody);
        try {
            String result = customerModel.addCustomer(jsonObject).toString();
            System.out.println(result);
            resp.getWriter().print(result);
        } catch (Exception e) {
            resp.sendError(500);
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String jsonStr = customerModel.getAllCustomer().toString();
            JSONArray jsonArray = new JSONArray(jsonStr);
            resp.getWriter().print(CustomerView.getCustomerView(jsonArray,getUserName(req)));
        } catch (Exception e) {
            resp.sendError(500);
        }
    }

}
