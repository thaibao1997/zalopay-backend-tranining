function formatInputDate(datestr) {
    datestr = datestr.split("-");
    return datestr[2] + "/" + datestr[1] + "/" + datestr[0];
}

function formatResponseDate(datestr) {
    datestr = datestr.split(" ");
    datestr = datestr[0].split("/");
    return datestr[2] + "-" + datestr[1] + "-" + datestr[0];
}

var table;

$(document).ready(function () {

    table = $('#contentTable').DataTable();

    $('#addForm').on('submit', function (e) {
        e.preventDefault();
        var bookingId = $('#bookingId').val();
        var roomId = $('#roomId').val();
        var customerId = $('#customerId').val();
        var fromDate = formatInputDate($('#fromDate').val());
        var toDate = formatInputDate($('#toDate').val());
        var object = {
            bookingId: bookingId,
            customerId: customerId,
            roomId: roomId,
            fromDate: fromDate,
            toDate: toDate
        }

        $.ajax({
            type: 'POST',
            data: JSON.stringify(object),
            url: '../booking',
            success: function (result) {
                console.log(result);
                var obj = JSON.parse(result);
                if (obj.error) {
                    alert(obj.errorMsg);
                } else {
                    var html = " <tr id= :bookingId>" +
                        "<td>:bookingId</td>" +
                        "<td>:customerId</td>" +
                        "<td>:roomId</td>" +
                        "<td>:bookingDate</td>" +
                        "<td>:fromDate</td>" +
                        "<td>:toDate</td>" +
                        "<td>:bookingStatus</td>" +
                        '<td><button onclick="tinhTien(this)"  class="btn btn-success">' +
                        '<i class="far fa-credit-card"></i> Checkout</button>' +
                        '<button onclick="updateClick(this)" data-toggle="modal" data-target="#updateModal" class="btn btn-primary">' +
                        '<i class="fas fa-pencil-alt"></i> Edit</button>' +
                        '<button class="btn btn-danger" onclick="deleteClick(this)">' +
                        '<i class="fas fa-trash-alt"></i> Delete</button></td></tr>';
                    console.log(obj);
                    html = html.replace(':bookingId', "");
                    html = html.replace(':bookingId', obj.bookingId);
                    html = html.replace(':customerId', obj.customerId);
                    html = html.replace(':roomId', obj.roomId);
                    html = html.replace(':bookingDate', obj.bookingDate);
                    html = html.replace(':fromDate', obj.fromDate);
                    html = html.replace(':toDate', obj.toDate);
                    html = html.replace(':bookingStatus', obj.bookingStatus === 1 ? "Active" : "Da Tra");
                    table.row.add($(html)).draw();
                    $('#closeModal').click();
                }
            }
        });

    });

    $('#updateForm').on('submit', function (e) {
        e.preventDefault();
        var bookingId = $('#bookingIdE').val();
        var roomId = $('#roomIdE').val();
        var customerId = $('#customerIdE').val();
        var fromDate = formatInputDate($('#fromDateE').val());
        var toDate = formatInputDate($('#toDateE').val());
        var object = {
            bookingId: bookingId,
            customerId: customerId,
            roomId: roomId,
            fromDate: fromDate,
            toDate: toDate
        }
        $.ajax({
            type: 'PATCH',
            data: JSON.stringify(object),
            url: '../booking?id=' + bookingId,
            success: function (result) {
                var obj = JSON.parse(result);
                if (obj.error) {
                    alert(obj.errorMsg);
                } else {
                    alert("sussess");
                    var node = document.getElementById(bookingId);
                    var nodeList = node.getElementsByTagName("td");
                    nodeList[1].innerText = obj.customerId;
                    nodeList[2].innerText = obj.roomId;
                    nodeList[3].innerText = obj.bookingDate;
                    nodeList[4].innerText = obj.fromDate;
                    nodeList[5].innerText = obj.toDate;
                    $('#closeUpdateModal').click();
                }
            }
        });
    });

});

function deleteClick(e) {
    var id = e.parentNode.parentNode.getAttributeNode('id').value;
    if (confirm("Are you sure to delete booking: " + id)) {

        $.ajax({
            type: 'DELETE',
            url: '../booking?id=' + id,
            success: function (result) {
                var obj = JSON.parse(result);
                if (obj.error) {
                    alert(obj.errorMsg);
                } else {
                    table.row('#' + id).remove().draw();
                }
            }

        });
    }
}


function updateClick(e) {
    var parentTr = e.parentNode.parentNode;
    var nodeList = parentTr.getElementsByTagName("td");
    console.log(nodeList);
    $('#bookingIdE').val(nodeList[0].innerText.toString().trim());
    $("#customerIdE").val(nodeList[1].innerText.toString().trim());
    $("#roomIdE").val(nodeList[2].innerText.toString().trim());

    var date = formatResponseDate(nodeList[4].innerText.toString().trim());
    $('#fromDateE').val(date);
    date = formatResponseDate(nodeList[5].innerText.toString().trim());
    console.log(date);
    $('#toDateE').val(date);
}

function tinhTien(e) {
    var parentTr = e.parentNode.parentNode;
    var nodeList = parentTr.getElementsByTagName("td");
    var bookingId = nodeList[0].innerText;
    console.log("Checkout booking: " + bookingId);
    $.ajax({
        type: 'POST',
        url: '../booking/checkout?id=' + bookingId,
        success: function (result) {
            //console.log(result);
            var obj = JSON.parse(result);
            if (obj.error) {
                alert(obj.errorMsg);
            } else {
                var chargeId = obj.chargeId;
                var QRCode = obj.QRCode;
                var expireTime = obj.expireTime;
                QRCode = 'data:image/png;base64,' + QRCode;
                $('#QRCodeImg').prop('src', QRCode)
                $('#QRCodeModal').modal({
                    backdrop: "static",
                    keyboard: false
                });
                countdownQRCode(expireTime,e,chargeId);
            }
        }
    })
}


function countdownQRCode(time,envent,chargeId) {
    function downTime() {
        time--;
        $('#QRCodeTime').text(time);
        if (time <= 0) {
            $('#QRCodeModal').modal('hide');
        } else {
            $.ajax({
                type: 'POST',
                url: '../charging?id=' + chargeId,
                success: function (result) {
                    var obj = JSON.parse(result);
                    console.log(obj);
                    if (!obj.error) {
                        time=0;
                        $('#QRCodeModal').modal('hide');

                        $('#cobookingId').val(obj.bookingId);
                        $('#coroomId').val(obj.roomId);
                        $('#coAmount').val(obj.totalAmount);
                        $('#coDate').val(obj.checkOutDate);
                        $('#checkoutModal').modal('show');

                        var parentTr = envent.parentNode.parentNode;
                        var nodeList = parentTr.getElementsByTagName("td");
                        nodeList[6].innerText = "Da Tra";
                        envent.disabled =true;
                    }
                }
            })
            setTimeout(
                downTime, 1000);
        }
    };
    downTime();



}