package baolt.hotel.api.grpc.menu;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.13.0-SNAPSHOT)",
    comments = "Source: Menu.proto")
public final class MenuServicesGrpc {

  private MenuServicesGrpc() {}

  public static final String SERVICE_NAME = "MenuServices";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getGetAllMethod()} instead. 
  public static final io.grpc.MethodDescriptor<baolt.hotel.api.grpc.menu.MenuGetAllParams,
      baolt.hotel.api.grpc.menu.MenuMsg> METHOD_GET_ALL = getGetAllMethodHelper();

  private static volatile io.grpc.MethodDescriptor<baolt.hotel.api.grpc.menu.MenuGetAllParams,
      baolt.hotel.api.grpc.menu.MenuMsg> getGetAllMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.menu.MenuGetAllParams,
      baolt.hotel.api.grpc.menu.MenuMsg> getGetAllMethod() {
    return getGetAllMethodHelper();
  }

  private static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.menu.MenuGetAllParams,
      baolt.hotel.api.grpc.menu.MenuMsg> getGetAllMethodHelper() {
    io.grpc.MethodDescriptor<baolt.hotel.api.grpc.menu.MenuGetAllParams, baolt.hotel.api.grpc.menu.MenuMsg> getGetAllMethod;
    if ((getGetAllMethod = MenuServicesGrpc.getGetAllMethod) == null) {
      synchronized (MenuServicesGrpc.class) {
        if ((getGetAllMethod = MenuServicesGrpc.getGetAllMethod) == null) {
          MenuServicesGrpc.getGetAllMethod = getGetAllMethod = 
              io.grpc.MethodDescriptor.<baolt.hotel.api.grpc.menu.MenuGetAllParams, baolt.hotel.api.grpc.menu.MenuMsg>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(
                  "MenuServices", "getAll"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.menu.MenuGetAllParams.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.menu.MenuMsg.getDefaultInstance()))
                  .setSchemaDescriptor(new MenuServicesMethodDescriptorSupplier("getAll"))
                  .build();
          }
        }
     }
     return getGetAllMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getAddMethod()} instead. 
  public static final io.grpc.MethodDescriptor<baolt.hotel.api.grpc.menu.MenuMsg,
      baolt.hotel.api.grpc.menu.MenuMsg> METHOD_ADD = getAddMethodHelper();

  private static volatile io.grpc.MethodDescriptor<baolt.hotel.api.grpc.menu.MenuMsg,
      baolt.hotel.api.grpc.menu.MenuMsg> getAddMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.menu.MenuMsg,
      baolt.hotel.api.grpc.menu.MenuMsg> getAddMethod() {
    return getAddMethodHelper();
  }

  private static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.menu.MenuMsg,
      baolt.hotel.api.grpc.menu.MenuMsg> getAddMethodHelper() {
    io.grpc.MethodDescriptor<baolt.hotel.api.grpc.menu.MenuMsg, baolt.hotel.api.grpc.menu.MenuMsg> getAddMethod;
    if ((getAddMethod = MenuServicesGrpc.getAddMethod) == null) {
      synchronized (MenuServicesGrpc.class) {
        if ((getAddMethod = MenuServicesGrpc.getAddMethod) == null) {
          MenuServicesGrpc.getAddMethod = getAddMethod = 
              io.grpc.MethodDescriptor.<baolt.hotel.api.grpc.menu.MenuMsg, baolt.hotel.api.grpc.menu.MenuMsg>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "MenuServices", "add"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.menu.MenuMsg.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.menu.MenuMsg.getDefaultInstance()))
                  .setSchemaDescriptor(new MenuServicesMethodDescriptorSupplier("add"))
                  .build();
          }
        }
     }
     return getAddMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getUpdateMethod()} instead. 
  public static final io.grpc.MethodDescriptor<baolt.hotel.api.grpc.menu.MenuMsg,
      baolt.hotel.api.grpc.menu.MenuMsg> METHOD_UPDATE = getUpdateMethodHelper();

  private static volatile io.grpc.MethodDescriptor<baolt.hotel.api.grpc.menu.MenuMsg,
      baolt.hotel.api.grpc.menu.MenuMsg> getUpdateMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.menu.MenuMsg,
      baolt.hotel.api.grpc.menu.MenuMsg> getUpdateMethod() {
    return getUpdateMethodHelper();
  }

  private static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.menu.MenuMsg,
      baolt.hotel.api.grpc.menu.MenuMsg> getUpdateMethodHelper() {
    io.grpc.MethodDescriptor<baolt.hotel.api.grpc.menu.MenuMsg, baolt.hotel.api.grpc.menu.MenuMsg> getUpdateMethod;
    if ((getUpdateMethod = MenuServicesGrpc.getUpdateMethod) == null) {
      synchronized (MenuServicesGrpc.class) {
        if ((getUpdateMethod = MenuServicesGrpc.getUpdateMethod) == null) {
          MenuServicesGrpc.getUpdateMethod = getUpdateMethod = 
              io.grpc.MethodDescriptor.<baolt.hotel.api.grpc.menu.MenuMsg, baolt.hotel.api.grpc.menu.MenuMsg>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "MenuServices", "update"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.menu.MenuMsg.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.menu.MenuMsg.getDefaultInstance()))
                  .setSchemaDescriptor(new MenuServicesMethodDescriptorSupplier("update"))
                  .build();
          }
        }
     }
     return getUpdateMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getDeleteMethod()} instead. 
  public static final io.grpc.MethodDescriptor<baolt.hotel.api.grpc.menu.MenuDeleteParams,
      baolt.hotel.api.grpc.menu.MenuMsg> METHOD_DELETE = getDeleteMethodHelper();

  private static volatile io.grpc.MethodDescriptor<baolt.hotel.api.grpc.menu.MenuDeleteParams,
      baolt.hotel.api.grpc.menu.MenuMsg> getDeleteMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.menu.MenuDeleteParams,
      baolt.hotel.api.grpc.menu.MenuMsg> getDeleteMethod() {
    return getDeleteMethodHelper();
  }

  private static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.menu.MenuDeleteParams,
      baolt.hotel.api.grpc.menu.MenuMsg> getDeleteMethodHelper() {
    io.grpc.MethodDescriptor<baolt.hotel.api.grpc.menu.MenuDeleteParams, baolt.hotel.api.grpc.menu.MenuMsg> getDeleteMethod;
    if ((getDeleteMethod = MenuServicesGrpc.getDeleteMethod) == null) {
      synchronized (MenuServicesGrpc.class) {
        if ((getDeleteMethod = MenuServicesGrpc.getDeleteMethod) == null) {
          MenuServicesGrpc.getDeleteMethod = getDeleteMethod = 
              io.grpc.MethodDescriptor.<baolt.hotel.api.grpc.menu.MenuDeleteParams, baolt.hotel.api.grpc.menu.MenuMsg>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "MenuServices", "delete"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.menu.MenuDeleteParams.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.menu.MenuMsg.getDefaultInstance()))
                  .setSchemaDescriptor(new MenuServicesMethodDescriptorSupplier("delete"))
                  .build();
          }
        }
     }
     return getDeleteMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static MenuServicesStub newStub(io.grpc.Channel channel) {
    return new MenuServicesStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static MenuServicesBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new MenuServicesBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static MenuServicesFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new MenuServicesFutureStub(channel);
  }

  /**
   */
  public static abstract class MenuServicesImplBase implements io.grpc.BindableService {

    /**
     */
    public void getAll(baolt.hotel.api.grpc.menu.MenuGetAllParams request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.menu.MenuMsg> responseObserver) {
      asyncUnimplementedUnaryCall(getGetAllMethodHelper(), responseObserver);
    }

    /**
     */
    public void add(baolt.hotel.api.grpc.menu.MenuMsg request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.menu.MenuMsg> responseObserver) {
      asyncUnimplementedUnaryCall(getAddMethodHelper(), responseObserver);
    }

    /**
     */
    public void update(baolt.hotel.api.grpc.menu.MenuMsg request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.menu.MenuMsg> responseObserver) {
      asyncUnimplementedUnaryCall(getUpdateMethodHelper(), responseObserver);
    }

    /**
     */
    public void delete(baolt.hotel.api.grpc.menu.MenuDeleteParams request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.menu.MenuMsg> responseObserver) {
      asyncUnimplementedUnaryCall(getDeleteMethodHelper(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetAllMethodHelper(),
            asyncServerStreamingCall(
              new MethodHandlers<
                baolt.hotel.api.grpc.menu.MenuGetAllParams,
                baolt.hotel.api.grpc.menu.MenuMsg>(
                  this, METHODID_GET_ALL)))
          .addMethod(
            getAddMethodHelper(),
            asyncUnaryCall(
              new MethodHandlers<
                baolt.hotel.api.grpc.menu.MenuMsg,
                baolt.hotel.api.grpc.menu.MenuMsg>(
                  this, METHODID_ADD)))
          .addMethod(
            getUpdateMethodHelper(),
            asyncUnaryCall(
              new MethodHandlers<
                baolt.hotel.api.grpc.menu.MenuMsg,
                baolt.hotel.api.grpc.menu.MenuMsg>(
                  this, METHODID_UPDATE)))
          .addMethod(
            getDeleteMethodHelper(),
            asyncUnaryCall(
              new MethodHandlers<
                baolt.hotel.api.grpc.menu.MenuDeleteParams,
                baolt.hotel.api.grpc.menu.MenuMsg>(
                  this, METHODID_DELETE)))
          .build();
    }
  }

  /**
   */
  public static final class MenuServicesStub extends io.grpc.stub.AbstractStub<MenuServicesStub> {
    private MenuServicesStub(io.grpc.Channel channel) {
      super(channel);
    }

    private MenuServicesStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MenuServicesStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new MenuServicesStub(channel, callOptions);
    }

    /**
     */
    public void getAll(baolt.hotel.api.grpc.menu.MenuGetAllParams request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.menu.MenuMsg> responseObserver) {
      asyncServerStreamingCall(
          getChannel().newCall(getGetAllMethodHelper(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void add(baolt.hotel.api.grpc.menu.MenuMsg request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.menu.MenuMsg> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getAddMethodHelper(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void update(baolt.hotel.api.grpc.menu.MenuMsg request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.menu.MenuMsg> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getUpdateMethodHelper(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void delete(baolt.hotel.api.grpc.menu.MenuDeleteParams request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.menu.MenuMsg> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getDeleteMethodHelper(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class MenuServicesBlockingStub extends io.grpc.stub.AbstractStub<MenuServicesBlockingStub> {
    private MenuServicesBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private MenuServicesBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MenuServicesBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new MenuServicesBlockingStub(channel, callOptions);
    }

    /**
     */
    public java.util.Iterator<baolt.hotel.api.grpc.menu.MenuMsg> getAll(
        baolt.hotel.api.grpc.menu.MenuGetAllParams request) {
      return blockingServerStreamingCall(
          getChannel(), getGetAllMethodHelper(), getCallOptions(), request);
    }

    /**
     */
    public baolt.hotel.api.grpc.menu.MenuMsg add(baolt.hotel.api.grpc.menu.MenuMsg request) {
      return blockingUnaryCall(
          getChannel(), getAddMethodHelper(), getCallOptions(), request);
    }

    /**
     */
    public baolt.hotel.api.grpc.menu.MenuMsg update(baolt.hotel.api.grpc.menu.MenuMsg request) {
      return blockingUnaryCall(
          getChannel(), getUpdateMethodHelper(), getCallOptions(), request);
    }

    /**
     */
    public baolt.hotel.api.grpc.menu.MenuMsg delete(baolt.hotel.api.grpc.menu.MenuDeleteParams request) {
      return blockingUnaryCall(
          getChannel(), getDeleteMethodHelper(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class MenuServicesFutureStub extends io.grpc.stub.AbstractStub<MenuServicesFutureStub> {
    private MenuServicesFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private MenuServicesFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected MenuServicesFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new MenuServicesFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<baolt.hotel.api.grpc.menu.MenuMsg> add(
        baolt.hotel.api.grpc.menu.MenuMsg request) {
      return futureUnaryCall(
          getChannel().newCall(getAddMethodHelper(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<baolt.hotel.api.grpc.menu.MenuMsg> update(
        baolt.hotel.api.grpc.menu.MenuMsg request) {
      return futureUnaryCall(
          getChannel().newCall(getUpdateMethodHelper(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<baolt.hotel.api.grpc.menu.MenuMsg> delete(
        baolt.hotel.api.grpc.menu.MenuDeleteParams request) {
      return futureUnaryCall(
          getChannel().newCall(getDeleteMethodHelper(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_ALL = 0;
  private static final int METHODID_ADD = 1;
  private static final int METHODID_UPDATE = 2;
  private static final int METHODID_DELETE = 3;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final MenuServicesImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(MenuServicesImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_ALL:
          serviceImpl.getAll((baolt.hotel.api.grpc.menu.MenuGetAllParams) request,
              (io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.menu.MenuMsg>) responseObserver);
          break;
        case METHODID_ADD:
          serviceImpl.add((baolt.hotel.api.grpc.menu.MenuMsg) request,
              (io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.menu.MenuMsg>) responseObserver);
          break;
        case METHODID_UPDATE:
          serviceImpl.update((baolt.hotel.api.grpc.menu.MenuMsg) request,
              (io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.menu.MenuMsg>) responseObserver);
          break;
        case METHODID_DELETE:
          serviceImpl.delete((baolt.hotel.api.grpc.menu.MenuDeleteParams) request,
              (io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.menu.MenuMsg>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class MenuServicesBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    MenuServicesBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return baolt.hotel.api.grpc.menu.MenuProto.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("MenuServices");
    }
  }

  private static final class MenuServicesFileDescriptorSupplier
      extends MenuServicesBaseDescriptorSupplier {
    MenuServicesFileDescriptorSupplier() {}
  }

  private static final class MenuServicesMethodDescriptorSupplier
      extends MenuServicesBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    MenuServicesMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (MenuServicesGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new MenuServicesFileDescriptorSupplier())
              .addMethod(getGetAllMethodHelper())
              .addMethod(getAddMethodHelper())
              .addMethod(getUpdateMethodHelper())
              .addMethod(getDeleteMethodHelper())
              .build();
        }
      }
    }
    return result;
  }
}
