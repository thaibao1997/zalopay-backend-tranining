/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.database;

import baolt.hotel.api.dataacess.CustomerDAO;
import baolt.hotel.api.dto.Customer;
import baolt.hotel.api.dto.Menu;
import static baolt.hotel.api.util.APIHelper.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author baolt
 */
public class CustomerDB {

    static private final String TABLE_NAME = "Customer";
    static private final String ID_COL = "customerId";
    static private final String FULL_NAME_COL = "fullName";
    static private final String ADDRESS_COL = "address";
    static private final String PHONE_NUMBER_COL = "phoneNumber";
    static private final String CREATE_DATE_COL = "createDate";
    static private final String DOB_COL = "dayOfBirth";

    private DataSource dataSource = null;

    public CustomerDB() {
        dataSource = DataSource.getInstance();

    }

    private Customer resultSetToCustomer(ResultSet rs) throws SQLException {
        Customer customer = new Customer();
        customer.setCustomerId(rs.getString(ID_COL));
        customer.setFullName(rs.getString(FULL_NAME_COL));
        customer.setAddress(rs.getString(ADDRESS_COL));
        customer.setPhoneNumber(rs.getString(PHONE_NUMBER_COL));
        Timestamp date = rs.getTimestamp(CREATE_DATE_COL);
        customer.setCreateDate(convertTimestampToDate(date));
        date = rs.getTimestamp(DOB_COL);
        customer.setDayOfBirth(convertTimestampToDate(date));
        return customer;
    }

    public ArrayList<Customer> getAllCustomers() throws SQLException {
        ArrayList<Customer> customers = new ArrayList<>();
        try  (Connection conn = dataSource.getConnection()){
            String query = "Select * from " + TABLE_NAME;
            PreparedStatement st = conn.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Customer customer = resultSetToCustomer(rs);
                customers.add(customer);
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } 
        return customers;
    }

    public Customer getCustomer(String id) throws SQLException {
        Customer customer = null;
        try (Connection conn = dataSource.getConnection()){
            String query = "Select * from " + TABLE_NAME + " Where customerId Like ?";
            PreparedStatement st = conn.prepareStatement(query);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                customer = resultSetToCustomer(rs);
                return customer;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }

    public boolean addCustomer(Customer customer) throws SQLException {
        try(Connection conn = dataSource.getConnection()){
            String query = "Insert into " + TABLE_NAME + " values (?,?,?,?,?,?)";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, customer.getCustomerId());
            pst.setString(2, customer.getFullName());
            pst.setString(3, customer.getAddress());
            Date doB = customer.getDayOfBirth();
            pst.setTimestamp(4, convertDateToTimestamp(doB));
            pst.setString(5, customer.getPhoneNumber());
            Date createDate = customer.getCreateDate();
            pst.setTimestamp(6, convertDateToTimestamp(createDate));

            return pst.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }

    public boolean deleteCustomer(String id) throws SQLException {
        try (Connection conn = dataSource.getConnection()){
            String query = "delete from " + TABLE_NAME + " where customerID like ?";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, id);
            return pst.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }

    public boolean updateCustomer(Customer customer) throws SQLException {
        try (Connection conn = dataSource.getConnection()){
            String query = "update " + TABLE_NAME + " set fullname= ? "
                    + ",address = ?,dayOfBirth = ? ,phoneNumber = ? "
                    + ",createDate = ? where customerId like ?";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, customer.getFullName());
            pst.setString(2, customer.getAddress());
            Date doB = customer.getDayOfBirth();
            pst.setTimestamp(3, convertDateToTimestamp(doB));
            pst.setString(4, customer.getPhoneNumber());
            Date createDate = customer.getCreateDate();
            pst.setTimestamp(5, convertDateToTimestamp(createDate));
            pst.setString(6, customer.getCustomerId());
            return pst.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
}
