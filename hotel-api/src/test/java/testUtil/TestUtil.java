/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testUtil;

import baolt.hotel.api.cache.*;

/**
 *
 * @author baolt
 */
public class TestUtil {

    public static void flushAllCaches() {
        RoomCache rc = RoomCache.getInstance();
        CustomerCache cc = CustomerCache.getInstance();
        MenuCache mc = MenuCache.getInstance();
        BookingCache bc = BookingCache.getInstance();
        CheckoutCache coc = CheckoutCache.getInstance();
        ChargingCache chc = ChargingCache.getInstance();

        rc.flushCaches();
        cc.flushCaches();
        mc.flushCaches();
        bc.flushCaches();
        coc.flushCaches();
        chc.flushCaches();

    }

}
