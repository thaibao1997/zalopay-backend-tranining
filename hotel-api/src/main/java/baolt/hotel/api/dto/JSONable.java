/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.dto;

import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public interface JSONable {
    public JSONObject toJsonObject();
    public void fromJsonString(String jsonStr) throws JSONException,IllegalArgumentException;
}
