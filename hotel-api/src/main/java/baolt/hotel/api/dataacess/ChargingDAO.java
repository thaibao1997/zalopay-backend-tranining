/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.dataacess;

import baolt.hotel.api.cache.ChargingCache;
import static baolt.hotel.api.dataacess.CheckoutDAO.getCheckoutLogString;
import baolt.hotel.api.database.ChargingDB;
import baolt.hotel.api.dto.Charging;
import baolt.hotel.api.dto.Checkout;
import baolt.hotel.api.kafka.KafkaConnect;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class ChargingDAO {

    private ChargingDB db;
    private ChargingCache cache;
    KafkaConnect kafka;

    public ChargingDAO() {
        db = new ChargingDB();
        cache = ChargingCache.getInstance();
        kafka = new KafkaConnect();
    }

    public Collection<Charging> getAllChargings() throws SQLException {
        if (cache.isExist()) {
            return cache.toCollection();
        }
        ArrayList<Charging> cus = db.getAllChargings();
        cache.fromList(cus);
        return cus;
    }

    public Charging getCharging(String id) throws SQLException {
        Charging charging = cache.findChargingByID(id);
        if (charging != null) {
            return charging;
        }
        charging = db.getCharging(id);
        if (!cache.addCharging(charging)) {
            return null;
        }
        return charging;
    }

    public boolean addCharging(Charging charging) {
        boolean res = cache.addCharging(charging);
        if (res) {
            Thread thread = new Thread(() -> {
                try {
                    db.addCharging(charging);
                } catch (SQLException ex) {
                    Logger.getLogger(ChargingDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            thread.start();
            kafka.writeLog(getChargingLogString(charging));
        }
        return res;
    }

    public boolean deleteCharging(String id) throws SQLException {
        boolean res = cache.deleteCharging(id);
        if (res) {
            Thread thread = new Thread(() -> {
                try {
                    db.deleteCharging(id);
                } catch (SQLException ex) {
                    Logger.getLogger(ChargingDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            thread.start();
        }
        return res;
    }

    public boolean updateCharging(Charging charging) throws SQLException {
        boolean res = cache.updateCharging(charging);
        if (res) {
            Thread thread = new Thread(() -> {
                try {
                    db.updateCharging(charging);
                } catch (SQLException ex) {
                    Logger.getLogger(ChargingDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            thread.start();
        }
        return res;
    }

    public static String getChargingLogString(Charging charging) {
        JSONObject json = new JSONObject();
        json.put("Operation", "Charging");
        json.put("Info", charging.toJsonObject());
        return json.toString();

    }
}
