/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.handle;

import baolt.hotel.api.business.BUSResult;
import baolt.hotel.api.business.RoomBUS;
import baolt.hotel.api.dto.Room;
import baolt.hotel.api.dto.RoomType;
import baolt.hotel.api.grpc.room.RoomDeleteParams;
import baolt.hotel.api.grpc.room.RoomMsg;
import baolt.hotel.api.grpc.room.RoomMsgType;
import baolt.hotel.api.grpc.room.RoomGetAllParams;
import baolt.hotel.api.grpc.room.RoomServicesGrpc;
import static baolt.hotel.api.handle.HandlerHelper.getRuntimeExceptionWithMessage;
import io.grpc.Status;
import io.grpc.stub.StreamObserver;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author baolt
 */
public class RoomGrpcHandler extends RoomServicesGrpc.RoomServicesImplBase {

    private RoomBUS roomBUS = new RoomBUS();
    
    

    @Override
    public void delete(RoomDeleteParams request, StreamObserver<RoomMsg> responseObserver) {
        BUSResult res = roomBUS.deleteRoom(request.getId());
        handleBUSResult(res, responseObserver);

    }

    @Override
    public void update(RoomMsg request, StreamObserver<RoomMsg> responseObserver) {
        Room room = convertRoomMsgToRoom(request);
        BUSResult res = roomBUS.updateRoom(room);
        handleBUSResult(res, responseObserver);

    }

    @Override
    public void add(RoomMsg request, StreamObserver<RoomMsg> responseObserver) {
        Room room = convertRoomMsgToRoom(request);
        BUSResult res = roomBUS.addRoom(room);
        handleBUSResult(res, responseObserver);
    }

    @Override
    public void getAll(RoomGetAllParams request, StreamObserver<RoomMsg> responseObserver) {
        Collection<Room> rooms = roomBUS.getAllRooms();
        if (rooms == null) {
            responseObserver
                    .onError(getRuntimeExceptionWithMessage("Database Error"));
        } else {
            for (Room room : rooms) {
                RoomMsg roomE = convertRoomToRoomMsg(room);
                responseObserver.onNext(roomE);
            }
            responseObserver.onCompleted();

        }
    }

    private static void handleBUSResult(BUSResult res, StreamObserver<RoomMsg> responseObserver) {
        if (res.err) {
            responseObserver
                    .onError(getRuntimeExceptionWithMessage(res.errMsg));
        } else {
            RoomMsg resp = convertRoomToRoomMsg((Room) res.returnObj);
            responseObserver.onNext(resp);
            responseObserver.onCompleted();
        }
    }

    public static RoomMsg convertRoomToRoomMsg(Room room) {
        RoomMsg roomE = RoomMsg.newBuilder()
                .setRoomId(room.getRoomId())
                .setRoomName(room.getRoomName())
                .setRoomType(RoomMsgType.forNumber(room.getRoomType().toInt() - 1))
                .setCreateDate(room.getCreateDate().getTime())
                .setDescription(room.getDescription())
                .setIsActive(room.isIsActive())
                .build();
        return roomE;
    }

    public static Room convertRoomMsgToRoom(RoomMsg roomE) {
        Room room = new Room();
        room.setRoomId(roomE.getRoomId());
        room.setRoomName(roomE.getRoomName());
        room.setIsActive(roomE.getIsActive());
        room.setCreateDate(new Date(roomE.getCreateDate()));
        room.setRoomType(RoomType.fromInt(roomE.getRoomType().getNumber() + 1));
        room.setDescription(roomE.getDescription());
        return room;
    }
}
