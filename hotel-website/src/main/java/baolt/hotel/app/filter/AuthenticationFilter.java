package baolt.hotel.app.filter;

import java.io.File;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author baolt
 */
public class AuthenticationFilter implements Filter {

    private RequestDispatcher defaultRequestDispatcher;

    @Override
    public void init(FilterConfig fc) throws ServletException {
        this.defaultRequestDispatcher
                = fc.getServletContext().getNamedDispatcher("default");

    }

    @Override
    public void doFilter(ServletRequest sr, ServletResponse sr1, FilterChain fc) throws IOException, ServletException {
        
        //defaultRequestDispatcher.forward(sr, sr1);
                
        HttpServletRequest req = (HttpServletRequest) sr;
        HttpServletResponse response = (HttpServletResponse) sr1;
        HttpSession session = req.getSession();

        String loginURI = req.getContextPath() + "/login";
        boolean loggedIn = session != null && session.getAttribute("user") != null;
        boolean loginRequest = req.getRequestURI().equals(loginURI);
       if (loggedIn || loginRequest) {
            fc.doFilter(sr, sr1);
        } else {
            File file = new File("public"+req.getRequestURI());
            if(file.exists() && file.isFile())
                defaultRequestDispatcher.forward(sr, sr1);
            else
                response.sendRedirect(loginURI);
        }
    }

    @Override
    public void destroy() {
    }
}
