/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.business;

import baolt.hotel.api.dataacess.BookingDAO;
import baolt.hotel.api.dataacess.CheckoutDAO;
import baolt.hotel.api.dataacess.RoomDAO;
import baolt.hotel.api.dto.Booking;
import baolt.hotel.api.dto.BookingStatus;
import baolt.hotel.api.dto.Checkout;
import baolt.hotel.api.dto.Room;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author baolt
 */
public class CheckoutBUS {

    public static final String FK_ERR_MSG = "Room or Booking not exist";
    public static final String ROOM_ERR_MSG = "Room not exist";
    public static final String BOOKING_ERR_MSG = "Booking not exist";

    private CheckoutDAO checkoutDAO;
    private RoomDAO roomDAO;
    private BookingDAO bookingDAO;

    public CheckoutBUS() {
        checkoutDAO = new CheckoutDAO();
        roomDAO = new RoomDAO();
        bookingDAO = new BookingDAO();
    }

    public Collection<Checkout> getAllCheckout() {
        try {
            return checkoutDAO.getAllCheckout();
        } catch (SQLException ex) {
            return null;
        }
    }

    public BUSResult getCheckoutByID(String bookingId, String roomId) {
        try {
            Checkout checkout = checkoutDAO.getCheckout(bookingId, roomId);
            if (checkout == null) {
                return BUSHelper.ID_NOT_FOUND_ERR;
            } else {
                return new BUSResult(checkout);
            }
        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, FK_ERR_MSG);
        }
    }

    public BUSResult checkBUS(Checkout checkout) throws SQLException {
        Booking booking = bookingDAO.getBooking(checkout.getBookingId());
        if (booking == null) {
            return new BUSResult(BOOKING_ERR_MSG);
        }
        Room room = roomDAO.getRoom(checkout.getRoomId());
        if (room == null) {
            return new BUSResult(ROOM_ERR_MSG);
        }

        return new BUSResult(null, false, null);

    }

    public BUSResult addCheckout(Checkout checkout) {

        try {
            BUSResult res = checkBUS(checkout);
            if (res.err) {
                return res;
            }
            
            if (checkoutDAO.addCheckout(checkout)) {
                String bookingId = checkout.getBookingId();
                if (bookingDAO.updateBookingStatus(bookingId, BookingStatus.DATRA)) {
                    return new BUSResult(checkout);
                }
            }
            return BUSHelper.DB_ERR;
        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, FK_ERR_MSG);
        }
    }

    public BUSResult deleteCheckout(String bookingId, String roomId) {
        try {
            Checkout checkout = checkoutDAO.getCheckout(bookingId, roomId);
            if (checkout == null) {
                return BUSHelper.ID_NOT_FOUND_ERR;
            }
            if (checkoutDAO.deleteCheckout(bookingId, roomId)) {
                return new BUSResult(checkout);
            }
        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, FK_ERR_MSG);
        }
        return BUSHelper.DB_ERR;
    }

    public BUSResult updateCheckout(Checkout checkout) {
        try {
            BUSResult res = checkBUS(checkout);
            if (res.err) {
                return res;
            }
            String bookingId = checkout.getBookingId();
            String roomId = checkout.getRoomId();

            Checkout checkout_ = checkoutDAO.getCheckout(bookingId, roomId);
            if (checkout_ == null) {
                return BUSHelper.ID_NOT_FOUND_ERR;
            }
            checkout.setCheckOutDate(checkout_.getCheckOutDate());
            if (checkoutDAO.updateCheckout(checkout)) {
                return new BUSResult(checkout);
            }
        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, FK_ERR_MSG);
        }
        return BUSHelper.DB_ERR;

    }
}
