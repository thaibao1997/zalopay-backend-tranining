/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.app.model;

import static baolt.hotel.app.model.ModelConstansts.API_ADDRESS;
import static baolt.hotel.app.model.ModelConstansts.API_PORT;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class ModelHelper {

    private static ManagedChannel CHANNEL;

    public static ManagedChannel getChannel() {
        if (CHANNEL == null || CHANNEL.isShutdown() || CHANNEL.isTerminated()) {
            CHANNEL = ManagedChannelBuilder.forAddress(API_ADDRESS, API_PORT)
                    .usePlaintext().build();
        }
        return CHANNEL;
    }

    public static JSONObject createErrorJSONObject(String msg) {
        return new JSONObject("{\"error\": true,\"errorMsg\" : \"" + msg + "\"}");
    }

}
