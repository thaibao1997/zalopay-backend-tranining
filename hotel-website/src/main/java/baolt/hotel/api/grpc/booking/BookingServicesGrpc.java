package baolt.hotel.api.grpc.booking;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.13.0-SNAPSHOT)",
    comments = "Source: Booking.proto")
public final class BookingServicesGrpc {

  private BookingServicesGrpc() {}

  public static final String SERVICE_NAME = "BookingServices";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getGetAllMethod()} instead. 
  public static final io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingGetAllParams,
      baolt.hotel.api.grpc.booking.BookingMsg> METHOD_GET_ALL = getGetAllMethodHelper();

  private static volatile io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingGetAllParams,
      baolt.hotel.api.grpc.booking.BookingMsg> getGetAllMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingGetAllParams,
      baolt.hotel.api.grpc.booking.BookingMsg> getGetAllMethod() {
    return getGetAllMethodHelper();
  }

  private static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingGetAllParams,
      baolt.hotel.api.grpc.booking.BookingMsg> getGetAllMethodHelper() {
    io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingGetAllParams, baolt.hotel.api.grpc.booking.BookingMsg> getGetAllMethod;
    if ((getGetAllMethod = BookingServicesGrpc.getGetAllMethod) == null) {
      synchronized (BookingServicesGrpc.class) {
        if ((getGetAllMethod = BookingServicesGrpc.getGetAllMethod) == null) {
          BookingServicesGrpc.getGetAllMethod = getGetAllMethod = 
              io.grpc.MethodDescriptor.<baolt.hotel.api.grpc.booking.BookingGetAllParams, baolt.hotel.api.grpc.booking.BookingMsg>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(
                  "BookingServices", "getAll"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.booking.BookingGetAllParams.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.booking.BookingMsg.getDefaultInstance()))
                  .setSchemaDescriptor(new BookingServicesMethodDescriptorSupplier("getAll"))
                  .build();
          }
        }
     }
     return getGetAllMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getAddMethod()} instead. 
  public static final io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingMsg,
      baolt.hotel.api.grpc.booking.BookingMsg> METHOD_ADD = getAddMethodHelper();

  private static volatile io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingMsg,
      baolt.hotel.api.grpc.booking.BookingMsg> getAddMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingMsg,
      baolt.hotel.api.grpc.booking.BookingMsg> getAddMethod() {
    return getAddMethodHelper();
  }

  private static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingMsg,
      baolt.hotel.api.grpc.booking.BookingMsg> getAddMethodHelper() {
    io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingMsg, baolt.hotel.api.grpc.booking.BookingMsg> getAddMethod;
    if ((getAddMethod = BookingServicesGrpc.getAddMethod) == null) {
      synchronized (BookingServicesGrpc.class) {
        if ((getAddMethod = BookingServicesGrpc.getAddMethod) == null) {
          BookingServicesGrpc.getAddMethod = getAddMethod = 
              io.grpc.MethodDescriptor.<baolt.hotel.api.grpc.booking.BookingMsg, baolt.hotel.api.grpc.booking.BookingMsg>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "BookingServices", "add"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.booking.BookingMsg.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.booking.BookingMsg.getDefaultInstance()))
                  .setSchemaDescriptor(new BookingServicesMethodDescriptorSupplier("add"))
                  .build();
          }
        }
     }
     return getAddMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getUpdateMethod()} instead. 
  public static final io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingMsg,
      baolt.hotel.api.grpc.booking.BookingMsg> METHOD_UPDATE = getUpdateMethodHelper();

  private static volatile io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingMsg,
      baolt.hotel.api.grpc.booking.BookingMsg> getUpdateMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingMsg,
      baolt.hotel.api.grpc.booking.BookingMsg> getUpdateMethod() {
    return getUpdateMethodHelper();
  }

  private static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingMsg,
      baolt.hotel.api.grpc.booking.BookingMsg> getUpdateMethodHelper() {
    io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingMsg, baolt.hotel.api.grpc.booking.BookingMsg> getUpdateMethod;
    if ((getUpdateMethod = BookingServicesGrpc.getUpdateMethod) == null) {
      synchronized (BookingServicesGrpc.class) {
        if ((getUpdateMethod = BookingServicesGrpc.getUpdateMethod) == null) {
          BookingServicesGrpc.getUpdateMethod = getUpdateMethod = 
              io.grpc.MethodDescriptor.<baolt.hotel.api.grpc.booking.BookingMsg, baolt.hotel.api.grpc.booking.BookingMsg>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "BookingServices", "update"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.booking.BookingMsg.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.booking.BookingMsg.getDefaultInstance()))
                  .setSchemaDescriptor(new BookingServicesMethodDescriptorSupplier("update"))
                  .build();
          }
        }
     }
     return getUpdateMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getDeleteMethod()} instead. 
  public static final io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingDeleteParams,
      baolt.hotel.api.grpc.booking.BookingMsg> METHOD_DELETE = getDeleteMethodHelper();

  private static volatile io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingDeleteParams,
      baolt.hotel.api.grpc.booking.BookingMsg> getDeleteMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingDeleteParams,
      baolt.hotel.api.grpc.booking.BookingMsg> getDeleteMethod() {
    return getDeleteMethodHelper();
  }

  private static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingDeleteParams,
      baolt.hotel.api.grpc.booking.BookingMsg> getDeleteMethodHelper() {
    io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingDeleteParams, baolt.hotel.api.grpc.booking.BookingMsg> getDeleteMethod;
    if ((getDeleteMethod = BookingServicesGrpc.getDeleteMethod) == null) {
      synchronized (BookingServicesGrpc.class) {
        if ((getDeleteMethod = BookingServicesGrpc.getDeleteMethod) == null) {
          BookingServicesGrpc.getDeleteMethod = getDeleteMethod = 
              io.grpc.MethodDescriptor.<baolt.hotel.api.grpc.booking.BookingDeleteParams, baolt.hotel.api.grpc.booking.BookingMsg>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "BookingServices", "delete"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.booking.BookingDeleteParams.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.booking.BookingMsg.getDefaultInstance()))
                  .setSchemaDescriptor(new BookingServicesMethodDescriptorSupplier("delete"))
                  .build();
          }
        }
     }
     return getDeleteMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getCheckoutMethod()} instead. 
  public static final io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingCheckoutParams,
      baolt.hotel.api.grpc.booking.BookingCheckoutMsg> METHOD_CHECKOUT = getCheckoutMethodHelper();

  private static volatile io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingCheckoutParams,
      baolt.hotel.api.grpc.booking.BookingCheckoutMsg> getCheckoutMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingCheckoutParams,
      baolt.hotel.api.grpc.booking.BookingCheckoutMsg> getCheckoutMethod() {
    return getCheckoutMethodHelper();
  }

  private static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingCheckoutParams,
      baolt.hotel.api.grpc.booking.BookingCheckoutMsg> getCheckoutMethodHelper() {
    io.grpc.MethodDescriptor<baolt.hotel.api.grpc.booking.BookingCheckoutParams, baolt.hotel.api.grpc.booking.BookingCheckoutMsg> getCheckoutMethod;
    if ((getCheckoutMethod = BookingServicesGrpc.getCheckoutMethod) == null) {
      synchronized (BookingServicesGrpc.class) {
        if ((getCheckoutMethod = BookingServicesGrpc.getCheckoutMethod) == null) {
          BookingServicesGrpc.getCheckoutMethod = getCheckoutMethod = 
              io.grpc.MethodDescriptor.<baolt.hotel.api.grpc.booking.BookingCheckoutParams, baolt.hotel.api.grpc.booking.BookingCheckoutMsg>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "BookingServices", "checkout"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.booking.BookingCheckoutParams.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.booking.BookingCheckoutMsg.getDefaultInstance()))
                  .setSchemaDescriptor(new BookingServicesMethodDescriptorSupplier("checkout"))
                  .build();
          }
        }
     }
     return getCheckoutMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static BookingServicesStub newStub(io.grpc.Channel channel) {
    return new BookingServicesStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static BookingServicesBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new BookingServicesBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static BookingServicesFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new BookingServicesFutureStub(channel);
  }

  /**
   */
  public static abstract class BookingServicesImplBase implements io.grpc.BindableService {

    /**
     */
    public void getAll(baolt.hotel.api.grpc.booking.BookingGetAllParams request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.booking.BookingMsg> responseObserver) {
      asyncUnimplementedUnaryCall(getGetAllMethodHelper(), responseObserver);
    }

    /**
     */
    public void add(baolt.hotel.api.grpc.booking.BookingMsg request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.booking.BookingMsg> responseObserver) {
      asyncUnimplementedUnaryCall(getAddMethodHelper(), responseObserver);
    }

    /**
     */
    public void update(baolt.hotel.api.grpc.booking.BookingMsg request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.booking.BookingMsg> responseObserver) {
      asyncUnimplementedUnaryCall(getUpdateMethodHelper(), responseObserver);
    }

    /**
     */
    public void delete(baolt.hotel.api.grpc.booking.BookingDeleteParams request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.booking.BookingMsg> responseObserver) {
      asyncUnimplementedUnaryCall(getDeleteMethodHelper(), responseObserver);
    }

    /**
     */
    public void checkout(baolt.hotel.api.grpc.booking.BookingCheckoutParams request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.booking.BookingCheckoutMsg> responseObserver) {
      asyncUnimplementedUnaryCall(getCheckoutMethodHelper(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetAllMethodHelper(),
            asyncServerStreamingCall(
              new MethodHandlers<
                baolt.hotel.api.grpc.booking.BookingGetAllParams,
                baolt.hotel.api.grpc.booking.BookingMsg>(
                  this, METHODID_GET_ALL)))
          .addMethod(
            getAddMethodHelper(),
            asyncUnaryCall(
              new MethodHandlers<
                baolt.hotel.api.grpc.booking.BookingMsg,
                baolt.hotel.api.grpc.booking.BookingMsg>(
                  this, METHODID_ADD)))
          .addMethod(
            getUpdateMethodHelper(),
            asyncUnaryCall(
              new MethodHandlers<
                baolt.hotel.api.grpc.booking.BookingMsg,
                baolt.hotel.api.grpc.booking.BookingMsg>(
                  this, METHODID_UPDATE)))
          .addMethod(
            getDeleteMethodHelper(),
            asyncUnaryCall(
              new MethodHandlers<
                baolt.hotel.api.grpc.booking.BookingDeleteParams,
                baolt.hotel.api.grpc.booking.BookingMsg>(
                  this, METHODID_DELETE)))
          .addMethod(
            getCheckoutMethodHelper(),
            asyncUnaryCall(
              new MethodHandlers<
                baolt.hotel.api.grpc.booking.BookingCheckoutParams,
                baolt.hotel.api.grpc.booking.BookingCheckoutMsg>(
                  this, METHODID_CHECKOUT)))
          .build();
    }
  }

  /**
   */
  public static final class BookingServicesStub extends io.grpc.stub.AbstractStub<BookingServicesStub> {
    private BookingServicesStub(io.grpc.Channel channel) {
      super(channel);
    }

    private BookingServicesStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected BookingServicesStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new BookingServicesStub(channel, callOptions);
    }

    /**
     */
    public void getAll(baolt.hotel.api.grpc.booking.BookingGetAllParams request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.booking.BookingMsg> responseObserver) {
      asyncServerStreamingCall(
          getChannel().newCall(getGetAllMethodHelper(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void add(baolt.hotel.api.grpc.booking.BookingMsg request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.booking.BookingMsg> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getAddMethodHelper(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void update(baolt.hotel.api.grpc.booking.BookingMsg request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.booking.BookingMsg> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getUpdateMethodHelper(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void delete(baolt.hotel.api.grpc.booking.BookingDeleteParams request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.booking.BookingMsg> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getDeleteMethodHelper(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void checkout(baolt.hotel.api.grpc.booking.BookingCheckoutParams request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.booking.BookingCheckoutMsg> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getCheckoutMethodHelper(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class BookingServicesBlockingStub extends io.grpc.stub.AbstractStub<BookingServicesBlockingStub> {
    private BookingServicesBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private BookingServicesBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected BookingServicesBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new BookingServicesBlockingStub(channel, callOptions);
    }

    /**
     */
    public java.util.Iterator<baolt.hotel.api.grpc.booking.BookingMsg> getAll(
        baolt.hotel.api.grpc.booking.BookingGetAllParams request) {
      return blockingServerStreamingCall(
          getChannel(), getGetAllMethodHelper(), getCallOptions(), request);
    }

    /**
     */
    public baolt.hotel.api.grpc.booking.BookingMsg add(baolt.hotel.api.grpc.booking.BookingMsg request) {
      return blockingUnaryCall(
          getChannel(), getAddMethodHelper(), getCallOptions(), request);
    }

    /**
     */
    public baolt.hotel.api.grpc.booking.BookingMsg update(baolt.hotel.api.grpc.booking.BookingMsg request) {
      return blockingUnaryCall(
          getChannel(), getUpdateMethodHelper(), getCallOptions(), request);
    }

    /**
     */
    public baolt.hotel.api.grpc.booking.BookingMsg delete(baolt.hotel.api.grpc.booking.BookingDeleteParams request) {
      return blockingUnaryCall(
          getChannel(), getDeleteMethodHelper(), getCallOptions(), request);
    }

    /**
     */
    public baolt.hotel.api.grpc.booking.BookingCheckoutMsg checkout(baolt.hotel.api.grpc.booking.BookingCheckoutParams request) {
      return blockingUnaryCall(
          getChannel(), getCheckoutMethodHelper(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class BookingServicesFutureStub extends io.grpc.stub.AbstractStub<BookingServicesFutureStub> {
    private BookingServicesFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private BookingServicesFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected BookingServicesFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new BookingServicesFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<baolt.hotel.api.grpc.booking.BookingMsg> add(
        baolt.hotel.api.grpc.booking.BookingMsg request) {
      return futureUnaryCall(
          getChannel().newCall(getAddMethodHelper(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<baolt.hotel.api.grpc.booking.BookingMsg> update(
        baolt.hotel.api.grpc.booking.BookingMsg request) {
      return futureUnaryCall(
          getChannel().newCall(getUpdateMethodHelper(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<baolt.hotel.api.grpc.booking.BookingMsg> delete(
        baolt.hotel.api.grpc.booking.BookingDeleteParams request) {
      return futureUnaryCall(
          getChannel().newCall(getDeleteMethodHelper(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<baolt.hotel.api.grpc.booking.BookingCheckoutMsg> checkout(
        baolt.hotel.api.grpc.booking.BookingCheckoutParams request) {
      return futureUnaryCall(
          getChannel().newCall(getCheckoutMethodHelper(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_ALL = 0;
  private static final int METHODID_ADD = 1;
  private static final int METHODID_UPDATE = 2;
  private static final int METHODID_DELETE = 3;
  private static final int METHODID_CHECKOUT = 4;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final BookingServicesImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(BookingServicesImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_ALL:
          serviceImpl.getAll((baolt.hotel.api.grpc.booking.BookingGetAllParams) request,
              (io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.booking.BookingMsg>) responseObserver);
          break;
        case METHODID_ADD:
          serviceImpl.add((baolt.hotel.api.grpc.booking.BookingMsg) request,
              (io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.booking.BookingMsg>) responseObserver);
          break;
        case METHODID_UPDATE:
          serviceImpl.update((baolt.hotel.api.grpc.booking.BookingMsg) request,
              (io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.booking.BookingMsg>) responseObserver);
          break;
        case METHODID_DELETE:
          serviceImpl.delete((baolt.hotel.api.grpc.booking.BookingDeleteParams) request,
              (io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.booking.BookingMsg>) responseObserver);
          break;
        case METHODID_CHECKOUT:
          serviceImpl.checkout((baolt.hotel.api.grpc.booking.BookingCheckoutParams) request,
              (io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.booking.BookingCheckoutMsg>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class BookingServicesBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    BookingServicesBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return baolt.hotel.api.grpc.booking.BookingProto.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("BookingServices");
    }
  }

  private static final class BookingServicesFileDescriptorSupplier
      extends BookingServicesBaseDescriptorSupplier {
    BookingServicesFileDescriptorSupplier() {}
  }

  private static final class BookingServicesMethodDescriptorSupplier
      extends BookingServicesBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    BookingServicesMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (BookingServicesGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new BookingServicesFileDescriptorSupplier())
              .addMethod(getGetAllMethodHelper())
              .addMethod(getAddMethodHelper())
              .addMethod(getUpdateMethodHelper())
              .addMethod(getDeleteMethodHelper())
              .addMethod(getCheckoutMethodHelper())
              .build();
        }
      }
    }
    return result;
  }
}
