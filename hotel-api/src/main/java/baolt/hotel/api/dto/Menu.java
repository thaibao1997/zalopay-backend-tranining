/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.dto;

import baolt.hotel.api.util.APIHelper;
import static baolt.hotel.api.util.APIHelper.getDateFromStr;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class Menu implements JSONable , Serializable{

    private String menuId;
    private String roomId;
    private long amount;
    private boolean isActive;
    private Date createDate;
    private String note = "";

    public Menu() {
        createDate = new Date();
    }

    public String getMenuId() {
        return menuId;
    }

    public void setMenuId(String menuId) {
        this.menuId = menuId.toUpperCase();
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId.toUpperCase();
    }

    public long getAmount() {
        return amount;
    }

    public void setAmount(long amount) {
        this.amount = amount;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public JSONObject toJsonObject() {
        JSONObject jsonO = new JSONObject(this);
        jsonO.put("createDate", APIHelper.getDateStr(this.createDate));
        return jsonO;
    }

    @Override
    public void fromJsonString(String jsonStr) throws JSONException, IllegalArgumentException {
        JSONObject jsonObject = new JSONObject(jsonStr);
        String menuId = jsonObject.getString("menuId");
        String roomId = jsonObject.getString("roomId");
        long amount = jsonObject.getLong("amount");

        if (jsonObject.has("createDate")) {
            String createStr = jsonObject.getString("createDate");
            Date dateTime = getDateFromStr(createStr);
            this.setCreateDate(dateTime);
        } else {
            this.setCreateDate(new Date());
        }

        if (jsonObject.has("note")) {
            this.setNote(jsonObject.getString("note"));
        } else {
            this.setNote(" ");
        }

        if (jsonObject.has("isActive")) {
            boolean isActive = jsonObject.getBoolean("isActive");
            this.setIsActive(isActive);

        } else {
            this.setIsActive(false);
        }

        this.setMenuId(menuId);
        this.setRoomId(roomId);
        this.setAmount(amount);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 29 * hash + Objects.hashCode(this.menuId);
        hash = 29 * hash + Objects.hashCode(this.roomId);
        hash = 29 * hash + (int) (this.amount ^ (this.amount >>> 32));
        hash = 29 * hash + (this.isActive ? 1 : 0);
        hash = 29 * hash + Objects.hashCode(this.createDate);
        hash = 29 * hash + Objects.hashCode(this.note);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Menu other = (Menu) obj;
        if (this.amount != other.amount) {
            return false;
        }
        if (this.isActive != other.isActive) {
            return false;
        }
        if (!Objects.equals(this.menuId, other.menuId)) {
            return false;
        }
        if (!Objects.equals(this.roomId, other.roomId)) {
            return false;
        }
        if (!Objects.equals(this.note, other.note)) {
            return false;
        }
        if (Math.abs(this.createDate.getTime() - other.createDate.getTime()) > 1000) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Menu{" + "menuId=" + menuId + ", roomId=" + roomId + ", amount=" + amount + ", isActive=" + isActive + ", createDate=" + createDate + ", note=" + note + '}';
    }

    
    
}
