package baolt.hotel.api.dto;

import baolt.hotel.api.util.APIHelper;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import org.json.JSONException;
import org.json.JSONObject;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author baolt
 */
public class Charging implements Serializable, JSONable {

    private String chargeId;
    private String bookingId;
    private long totalAmount;
    private Date chargeDate = new Date();
    private long tpeTransId;
    private int tpeReturnCode;
    private ChargeStatus chargeStatus;

    public String getChargeID() {
        return chargeId;
    }

    public void setChargeID(String chargeID) {
        this.chargeId = chargeID;
    }

    public String getBookingID() {
        return bookingId;
    }

    public void setBookingID(String bookingID) {
        this.bookingId = bookingID;
    }

    public long getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(long totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Date getChargeDate() {
        return chargeDate;
    }

    public void setChargeDate(Date chargeDate) {
        this.chargeDate = chargeDate;
    }

    public long getTpeTransID() {
        return tpeTransId;
    }

    public void setTpeTransID(long tpeTransID) {
        this.tpeTransId = tpeTransID;
    }

    public int getTpeReturnCode() {
        return tpeReturnCode;
    }

    public void setTpeReturnCode(int tpeReturnCode) {
        this.tpeReturnCode = tpeReturnCode;
    }

    public ChargeStatus getChargeStatus() {
        return chargeStatus;
    }

    public void setChargeStatus(ChargeStatus chargeStatus) {
        this.chargeStatus = chargeStatus;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 13 * hash + Objects.hashCode(this.chargeId);
        hash = 13 * hash + Objects.hashCode(this.bookingId);
        hash = 13 * hash + (int) (this.totalAmount ^ (this.totalAmount >>> 32));
        hash = 13 * hash + Objects.hashCode(this.chargeDate);
        hash = 13 * hash + (int) (this.tpeTransId ^ (this.tpeTransId >>> 32));
        hash = 13 * hash + this.tpeReturnCode;
        hash = 13 * hash + Objects.hashCode(this.chargeStatus);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Charging other = (Charging) obj;
        if (this.totalAmount != other.totalAmount) {
            return false;
        }
        if (this.tpeTransId != other.tpeTransId) {
            return false;
        }
        if (this.tpeReturnCode != other.tpeReturnCode) {
            return false;
        }
        if (!Objects.equals(this.chargeId, other.chargeId)) {
            return false;
        }
        if (!Objects.equals(this.bookingId, other.bookingId)) {
            return false;
        }
        if (Math.abs(this.chargeDate.getTime() - other.chargeDate.getTime()) > 1000) {
            return false;
        }
        if (this.chargeStatus != other.chargeStatus) {
            return false;
        }
        return true;
    }

    @Override
    public JSONObject toJsonObject() {
        JSONObject jsonO = new JSONObject(this);
        jsonO.put("chargeId", this.chargeId);
        jsonO.put("bookingId", this.bookingId);
        jsonO.put("chargeDate", APIHelper.getDateStr(chargeDate));
        jsonO.put("chargeStatus", this.chargeStatus.toInt());
        jsonO.put("totalAmount", this.totalAmount);
        jsonO.put("tpeReturnCode", this.tpeReturnCode);
        jsonO.put("tpeTransId", this.tpeTransId);
        return jsonO;
    }

    @Override
    public void fromJsonString(String jsonStr) throws JSONException, IllegalArgumentException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
