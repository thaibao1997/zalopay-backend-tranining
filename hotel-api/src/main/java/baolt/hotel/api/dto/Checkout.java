/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.dto;

import baolt.hotel.api.util.APIHelper;
import static baolt.hotel.api.util.APIHelper.getDateFromStr;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class Checkout implements JSONable, Serializable {

    private String bookingId;
    private String roomId;
    private long totalAmount;
    private Date checkOutDate;

    public Checkout() {
        checkOutDate = new Date();
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId.toUpperCase();
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId.toUpperCase();
    }

    public long getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(long totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Date getCheckOutDate() {
        return checkOutDate;
    }

    public void setCheckOutDate(Date checkOutDate) {
        this.checkOutDate = checkOutDate;
    }

    @Override
    public JSONObject toJsonObject() {
        JSONObject jsonO = new JSONObject(this);
        jsonO.put("bookingId", this.bookingId);
        jsonO.put("roomId", this.roomId);
        jsonO.put("totalAmount", this.totalAmount);
        jsonO.put("checkOutDate", APIHelper.getDateStr(this.checkOutDate));
        return jsonO;
    }

    @Override
    public void fromJsonString(String jsonStr) throws JSONException, IllegalArgumentException {
        JSONObject jsonObject = new JSONObject(jsonStr);
        String bookingId = jsonObject.getString("bookingId");
        String roomId = jsonObject.getString("roomId");
        long totalAmount = jsonObject.getLong("totalAmount");
        String dateStr = jsonObject.getString("checkoutDate");
        Date checkoutDate = getDateFromStr(dateStr);
        this.setCheckOutDate(checkoutDate);
        this.setBookingId(bookingId);
        this.setRoomId(roomId);
        this.setTotalAmount(totalAmount);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.bookingId);
        hash = 53 * hash + Objects.hashCode(this.roomId);
        hash = 53 * hash + (int) (this.totalAmount ^ (this.totalAmount >>> 32));
        hash = 53 * hash + Objects.hashCode(this.checkOutDate);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Checkout other = (Checkout) obj;
        if (this.totalAmount != other.totalAmount) {
            return false;
        }
        if (!Objects.equals(this.bookingId, other.bookingId)) {
            return false;
        }
        if (!Objects.equals(this.roomId, other.roomId)) {
            return false;
        }
        if (Math.abs(this.checkOutDate.getTime() - other.checkOutDate.getTime()) > 1000) {
            return false;
        }
        return true;
    }

}
