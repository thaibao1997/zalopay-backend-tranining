var table;

$(document).ready(function () {
    table= $('#contentTable').DataTable();

    $('#addForm').on('submit', function (e) {
        e.preventDefault();
        var menuId = $('#menuId').val();
        var roomId = $('#roomId').val();
        var amount = $('#amount').val();
        var isActive = $('#isActive').val() === "1" ? true : false;
        var note = $('#note').val();
        var object = {
            menuId: menuId, roomId: roomId, amount: amount
            , isActive: isActive, note: note
        }

        $.ajax({
            type: 'POST',
            data: JSON.stringify(object),
            url: '../menu',
            success: function (result) {
                var obj = JSON.parse(result);
                if (obj.error) {
                    alert(obj.errorMsg);
                } else {
                    var html = " <tr id= :menuId>" +
                        "<td>:menuId</td>" +
                        "<td>:roomId</td>" +
                        "<td>:amount</td>" +
                        "<td>:isActive</td>" +
                        "<td>:note</td>" +
                        "<td>:createDate</td>" +
                        '<td><button onclick="updateClick(this)" data-toggle="modal" data-target="#editModal" class="btn btn-primary">' +
                        '<i class="fas fa-pencil-alt"></i> Edit</button>' +
                        '<button class="btn btn-danger" onclick="deleteClick(this)">' +
                        '<i class="fas fa-trash-alt"></i> Delete</button></td></tr>';
                    console.log(obj);
                    html = html.replace(':menuId', obj.menuId);
                    html = html.replace(':menuId', obj.menuId);
                    html = html.replace(':roomId', obj.roomId);
                    html = html.replace(':amount', obj.amount);
                    html = html.replace(':isActive', obj.isActive ? "Yes" : "No");
                    html = html.replace(':note', obj.note);
                    html = html.replace(':createDate', obj.createDate);
                    table.row.add($(html)).draw();
                    $('#closeModal').click();
                }
            }
        });

    });

    $('#editForm').on('submit', function (e) {
        e.preventDefault();
        var menuId = $('#menuIdE').val();
        var roomId = $('#roomIdE').val();
        var amount = $('#amountE').val();
        var isActive = $('#isActiveE').val() === "1" ? true : false;
        var note = $('#noteE').val();
        var object = {
            menuId: menuId, roomId: roomId, amount: amount
            , isActive: isActive, note: note
        }
        $.ajax({
            type: 'PATCH',
            data: JSON.stringify(object),
            url: '../menu?id=' + menuId,
            success: function (result) {
                var obj = JSON.parse(result);
                if (obj.error) {
                    alert(obj.errorMsg);
                } else {
                    alert("sussess");
                    var node = document.getElementById(menuId);
                    var nodeList = node.getElementsByTagName("td");
                    nodeList[1].innerText = obj.roomId;
                    nodeList[2].innerText = obj.amount;
                    nodeList[3].innerText = obj.isActive ? "Yes" : "No" ;
                    nodeList[4].innerText = obj.note;
                    $('#closeUpdateModal').click();
                }
            }
        });
    });

});

function deleteClick(e) {
    var id = e.parentNode.parentNode.getAttributeNode('id').value;
    if (confirm("Are you sure to delete Menu: " + id)) {

        console.log("delete menu " + id);
        $.ajax({
            type: 'DELETE',
            url: '../menu?id=' + id,
            success: function (result) {
                var obj = JSON.parse(result);
                if (obj.error) {
                    alert(obj.errorMsg);
                } else {
                    table.row('#' + id).remove().draw();
                }
            }

        });
    }
}


function updateClick(e) {
    var parentTr = e.parentNode.parentNode;
    var nodeList = parentTr.getElementsByTagName("td");
    console.log(nodeList);
    $('#menuIdE').val(nodeList[0].innerText.toString().trim());
    $('#roomIdE').val(nodeList[1].innerText.toString().trim());
    var number = Number.parseInt(nodeList[2].innerText.toString().trim());
    console.log(number);
    $('#amountE').val(number);
    $('#isActiveE').val(nodeList[3].innerText.toString().trim() === "Yes" ? 1 :0);
    $('#noteE').val(nodeList[4].innerText.toString().trim());
    //$('#editModal').modal();
}