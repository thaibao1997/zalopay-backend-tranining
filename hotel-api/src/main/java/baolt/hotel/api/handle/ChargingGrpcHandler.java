/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.handle;

import baolt.hotel.api.business.BUSResult;
import baolt.hotel.api.business.ChargingBUS;
import baolt.hotel.api.dto.ChargeStatus;
import baolt.hotel.api.dto.Charging;
import baolt.hotel.api.dto.Checkout;
import baolt.hotel.api.grpc.charging.ChargingServicesGrpc;
import baolt.hotel.api.grpc.charging.GetPaymentStatusParams;
import baolt.hotel.api.grpc.checkout.CheckoutMsg;
import static baolt.hotel.api.handle.CheckoutGrpcHandler.convertCheckoutToCheckoutMsg;
import static baolt.hotel.api.handle.HandlerHelper.getRuntimeExceptionWithMessage;
import io.grpc.stub.StreamObserver;

/**
 *
 * @author baolt
 */
public class ChargingGrpcHandler extends ChargingServicesGrpc.ChargingServicesImplBase {

    private ChargingBUS chargingBUS = new ChargingBUS();

    @Override
    public void getPaymentStatus(GetPaymentStatusParams request, StreamObserver<CheckoutMsg> responseObserver) {
        String chargeId = request.getChargeId();
        BUSResult res = chargingBUS.getAssociatedCheckout(chargeId);
        if (res.err) {
            responseObserver
                    .onError(getRuntimeExceptionWithMessage(res.errMsg));
        } else {
            CheckoutMsg resp = convertCheckoutToCheckoutMsg((Checkout) res.returnObj);
            responseObserver.onNext(resp);
            responseObserver.onCompleted();
        }
    }

}
