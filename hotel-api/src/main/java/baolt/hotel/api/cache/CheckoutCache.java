/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.cache;

import static baolt.hotel.api.cache.CacheConnect.MAX_TRY_LOCK_TIME;
import static baolt.hotel.api.cache.CacheConnect.UNLOCK_AFTER_TIME;
import baolt.hotel.api.dto.Checkout;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.redisson.api.LocalCachedMapOptions;
import org.redisson.api.RLocalCachedMap;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RedissonClient;

/**
 *
 * @author baolt
 */
public class CheckoutCache {

    public static final String KEY = "HOTEL_API_CHECKOUTS";
    public static final String LOCK_NAME = "CHECKOUT_LOCK";

    private final RLocalCachedMap<KeyObject, Checkout> cachedCheckouts;
    private final RReadWriteLock rwlock;

    private static CheckoutCache instance = new CheckoutCache();

    public static CheckoutCache getInstance() {
        return instance;
    }

    private CheckoutCache() {
        RedissonClient client = CacheConnect.getRedissonClient();

        cachedCheckouts = client.getLocalCachedMap(KEY, CacheConnect.getLocalCachedMapOption());
        rwlock = client.getReadWriteLock(LOCK_NAME);
        flushCaches();
    }

    public void flushCaches() {
        if (cachedCheckouts.isExists()) {
            cachedCheckouts.clear();
            cachedCheckouts.expire(0, TimeUnit.SECONDS);
        }
    }

    private void unlockReadLock() {
        if (rwlock.readLock().isHeldByCurrentThread()) {
            rwlock.readLock().unlock();
        }
    }

    private void unlockWriteLock() {
        if (rwlock.writeLock().isHeldByCurrentThread()) {
            rwlock.writeLock().unlock();
        }
    }

    public boolean isEmpty() {
        return cachedCheckouts.isEmpty();
    }

    public boolean isExist() {
        return cachedCheckouts.isExists();
    }

    public boolean addCheckout(Checkout checkout) {
        if (checkout == null) {
            return false;
        }
        try {
            KeyObject key = new KeyObject(checkout.getBookingId(), checkout.getRoomId());
            if (rwlock.writeLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                return cachedCheckouts.fastPut(key, checkout);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(CheckoutCache.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            unlockWriteLock();
        }
        return false;
    }

    public boolean updateCheckout(Checkout checkout) {
        if (checkout == null) {
            return false;
        }
        try {
            KeyObject key = new KeyObject(checkout.getBookingId(), checkout.getRoomId());
            if (rwlock.writeLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                return cachedCheckouts.fastReplace(key, checkout);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(CheckoutCache.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            unlockWriteLock();
        }
        return false;
    }

    public Checkout findCheckoutByID(String bookingId, String roomId) {
        try {
            KeyObject key = new KeyObject(bookingId, roomId);
            if (rwlock.readLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                return cachedCheckouts.get(key);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            unlockReadLock();
        }
        return null;
    }

    public boolean deleteCheckout(String bookingId, String roomId) {
        try {
            KeyObject key = new KeyObject(bookingId, roomId);
            if (rwlock.writeLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                return cachedCheckouts.fastRemove(key) > 0;
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(CheckoutCache.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            unlockWriteLock();
        }
        return false;
    }

    public Collection<Checkout> toCollection() {
        try {
            if (rwlock.readLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                return cachedCheckouts.values();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            unlockReadLock();
        }
        return null;
    }

    public void fromList(ArrayList<Checkout> checkouts) {
        for (Checkout checkout : checkouts) {
            addCheckout(checkout);
        }
    }

}

class KeyObject implements Serializable {

    String bookingId;
    String checkoutId;

    public KeyObject(String bookingId, String checkoutId) {
        this.bookingId = bookingId;
        this.checkoutId = checkoutId;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 73 * hash + Objects.hashCode(this.bookingId);
        hash = 73 * hash + Objects.hashCode(this.checkoutId);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final KeyObject other = (KeyObject) obj;
        if (!Objects.equals(this.bookingId, other.bookingId)) {
            return false;
        }
        if (!Objects.equals(this.checkoutId, other.checkoutId)) {
            return false;
        }
        return true;
    }

}
