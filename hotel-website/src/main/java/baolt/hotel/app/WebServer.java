/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.app;

import baolt.hotel.app.filter.AuthenticationFilter;
import baolt.hotel.app.filter.LogFilter;
import baolt.hotel.app.filter.ResponseFilter;
import baolt.hotel.app.servlet.*;
import java.util.EnumSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.DispatcherType;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerCollection;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;

/**
 *
 * @author baolt
 */
public class WebServer {

    public static void main(String[] args) {
        try {
            //public files
            String pwdPath = System.getProperty("user.dir") + "/public/";

            //
            //HandlerCollection handlerCollection = new HandlerCollection();
            ///Servlet classes
            ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
            context.setResourceBase(pwdPath);
            
            

            context.setContextPath("/*");
//            context.addServlet(servlet, "/");
            context.addServlet(RoomServlet.class, "/room/*");
            context.addServlet(CustomerServlet.class, "/customer/*");
            context.addServlet(MenuPriceServlet.class, "/menu/*");
            context.addServlet(BookingServlet.class, "/booking/*");
            context.addServlet(CheckoutServlet.class, "/checkout/*");
            context.addServlet(AuthenticationServlet.class, "/login/*");
            context.addServlet(ChargingServlet.class, "/charging/*");

            ///static files
            String defName = "default";
            ServletHolder holderDef = new ServletHolder(defName, DefaultServlet.class);
            holderDef.setInitParameter("dirAllowed", "false");
            context.addServlet(holderDef, "/");

            //filter
            context.addFilter(LogFilter.class, "/*", EnumSet.of(DispatcherType.REQUEST));
            context.addFilter(ResponseFilter.class, "/*", null);
            context.addFilter(AuthenticationFilter.class, "/*", EnumSet.of(DispatcherType.REQUEST));

            Server server = new Server(8080);
            server.setHandler(context);

            server.start();
        } catch (Exception ex) {
            Logger.getLogger(WebServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
