/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.dto;

/**
 *
 * @author baolt
 */
public enum BookingStatus {
    ACTIVE, DATRA;

    public static BookingStatus fromInt(int num) {
        switch (num) {
            case 1:
                return ACTIVE;
            case 2:
                return DATRA;
        }
        return null;
    }

    public String toString() {
        switch (this) {
            case ACTIVE:
                return "Active";
            case DATRA:
                return "Da tra";
        }
        return "";
    }
    
    public int toInt(){
        switch (this) {
            case ACTIVE:
                return 1;
            case DATRA:
                return 2;
        }
        return 0;
    }

}
