/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.dataacess;

import baolt.hotel.api.cache.BookingCache;
import baolt.hotel.api.database.BookingDB;
import baolt.hotel.api.dto.Booking;
import baolt.hotel.api.dto.BookingStatus;
import baolt.hotel.api.kafka.KafkaConnect;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class BookingDAO {

    BookingDB db;
    BookingCache cache;
    KafkaConnect kafka;

    public BookingDAO() {
        db = new BookingDB();
        cache = BookingCache.getInstance();
        kafka = new KafkaConnect();
    }

    public Collection<Booking> getAllBookings() throws SQLException {
        if (cache.isExist()) {
            return cache.toCollection();
        }
        ArrayList<Booking> arr = db.getAllBookings();
        cache.fromList(arr);
        return arr;
    }

    public ArrayList<Booking> getAllActiveBookingOfRoom(String roomId) throws SQLException {
        if (cache.isExist()) {
            return cache.getActiveBookingOfRoom(roomId);
        }
        ArrayList<Booking> bookings = db.getAllActiveBookingOfRoom(roomId);
        cache.fromList(bookings);
        return bookings;
    }

    public Booking getBooking(String id) throws SQLException {
        Booking booking = cache.findBookingByID(id);
        if (booking != null) {
            return booking;
        }
        booking = db.getBooking(id);
        if (!cache.addBooking(booking)) {
            return null;
        }
        return booking;
    }

    public boolean addBooking(Booking booking) throws SQLException {
        boolean res = cache.addBooking(booking);
        if (res) {
            Thread thread = new Thread(() -> {
                try {
                    db.addBooking(booking);
                } catch (SQLException ex) {
                    Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            thread.start();
            //ban log vao kafka
            kafka.writeLog(getBookingLogString(booking));
        }
        return res;
    }

    public boolean deleteBooking(String id) throws SQLException {
        boolean res = cache.deleteBooking(id);
        if (res) {
            Thread thread = new Thread(() -> {
                try {
                    db.deleteBooking(id);
                } catch (SQLException ex) {
                    Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            thread.start();
        }
        return res;

    }

    public boolean updateBooking(Booking booking) throws SQLException {
        boolean res = cache.updateBooking(booking);
        if (res) {
            Thread thread = new Thread(() -> {
                try {
                    db.updateBooking(booking);
                } catch (SQLException ex) {
                    Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            thread.start();
        }
        return res;

    }

    public boolean updateBookingStatus(String id, BookingStatus bookingStatus) throws SQLException {
        Booking booking = getBooking(id);
        if (booking != null) {
            booking.setBookingStatus(bookingStatus);
            boolean res = cache.updateBooking(booking);
            if (res) {
                Thread thread = new Thread(() -> {
                    try {
                        db.updateBookingStatus(id, bookingStatus);
                    } catch (SQLException ex) {
                        Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
                    }
                });
                thread.start();
                return res;
            }

        }
        return false;
    }

    public static String getBookingLogString(Booking booking) {
        JSONObject json = new JSONObject();
        json.put("Operation", "Booking");
        json.put("Info", booking.toJsonObject());
        return json.toString();
    }

}
