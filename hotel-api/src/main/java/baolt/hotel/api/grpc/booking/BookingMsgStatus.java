// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Booking.proto

package baolt.hotel.api.grpc.booking;

/**
 * Protobuf enum {@code BookingMsgStatus}
 */
public enum BookingMsgStatus
    implements com.google.protobuf.ProtocolMessageEnum {
  /**
   * <code>ACTIVE = 0;</code>
   */
  ACTIVE(0),
  /**
   * <code>DATRA = 1;</code>
   */
  DATRA(1),
  UNRECOGNIZED(-1),
  ;

  /**
   * <code>ACTIVE = 0;</code>
   */
  public static final int ACTIVE_VALUE = 0;
  /**
   * <code>DATRA = 1;</code>
   */
  public static final int DATRA_VALUE = 1;


  public final int getNumber() {
    if (this == UNRECOGNIZED) {
      throw new java.lang.IllegalArgumentException(
          "Can't get the number of an unknown enum value.");
    }
    return value;
  }

  /**
   * @deprecated Use {@link #forNumber(int)} instead.
   */
  @java.lang.Deprecated
  public static BookingMsgStatus valueOf(int value) {
    return forNumber(value);
  }

  public static BookingMsgStatus forNumber(int value) {
    switch (value) {
      case 0: return ACTIVE;
      case 1: return DATRA;
      default: return null;
    }
  }

  public static com.google.protobuf.Internal.EnumLiteMap<BookingMsgStatus>
      internalGetValueMap() {
    return internalValueMap;
  }
  private static final com.google.protobuf.Internal.EnumLiteMap<
      BookingMsgStatus> internalValueMap =
        new com.google.protobuf.Internal.EnumLiteMap<BookingMsgStatus>() {
          public BookingMsgStatus findValueByNumber(int number) {
            return BookingMsgStatus.forNumber(number);
          }
        };

  public final com.google.protobuf.Descriptors.EnumValueDescriptor
      getValueDescriptor() {
    return getDescriptor().getValues().get(ordinal());
  }
  public final com.google.protobuf.Descriptors.EnumDescriptor
      getDescriptorForType() {
    return getDescriptor();
  }
  public static final com.google.protobuf.Descriptors.EnumDescriptor
      getDescriptor() {
    return baolt.hotel.api.grpc.booking.BookingProto.getDescriptor().getEnumTypes().get(0);
  }

  private static final BookingMsgStatus[] VALUES = values();

  public static BookingMsgStatus valueOf(
      com.google.protobuf.Descriptors.EnumValueDescriptor desc) {
    if (desc.getType() != getDescriptor()) {
      throw new java.lang.IllegalArgumentException(
        "EnumValueDescriptor is not for this type.");
    }
    if (desc.getIndex() == -1) {
      return UNRECOGNIZED;
    }
    return VALUES[desc.getIndex()];
  }

  private final int value;

  private BookingMsgStatus(int value) {
    this.value = value;
  }

  // @@protoc_insertion_point(enum_scope:BookingMsgStatus)
}

