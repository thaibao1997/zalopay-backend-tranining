/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.zalopay;

import baolt.hotel.api.hmacutil.HMACUtil;
import baolt.hotel.api.util.HTTPUtils;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class ZalopayUtils {

    private static final String CREATE_ORDER_URL = "https://sandbox.zalopay.com.vn/v001/tpe/createorder";
    private static final String GET_ORDER_STATUS_URL = "https://sandbox.zalopay.com.vn/v001/tpe/getstatusbyapptransid";

    private static final int APP_ID = 3;
    private static final String KEY_1 = "YhEahsbviSfzwSCDXiE3kCLDkLL2GDha";
    private static final String KEY_2 = "GhJUizqyV3uS37R6DZ3mGR5UnIwa26vt";
    private static final String HMAC_ALGORITHRM =  HMACUtil.HMACSHA256;
    

    public static String getCreateOrderHmac(String bookingId, String chargingId, long amount, long time) {
        String hmacInput = APP_ID + "|" + chargingId + "|" + "hotel_api" + "|" + amount
                + "|" + time + "|" + "{}" + "|" + "[]";
        String mac = HMACUtil.HMacHexStringEncode(HMAC_ALGORITHRM, KEY_1, hmacInput);
        return mac;
    }
    
    public static String getGetStatucHmac(String apptransid){
        String hmacInput = APP_ID + "|" + apptransid + "|" + KEY_1;
        String mac = HMACUtil.HMacHexStringEncode(HMAC_ALGORITHRM, KEY_1, hmacInput);
        return mac;
    }

    public static JSONObject createZalopayOrder(String bookingId, String chargingId, long amount, long time) {
        JSONObject obj = new JSONObject();
        obj.put("appid", APP_ID);
        obj.put("appuser", "hotel_api");
        obj.put("apptime", time);
        obj.put("amount", amount);
        obj.put("apptransid", chargingId);
        obj.put("embeddata", "{}");
        obj.put("item", "[]");
        obj.put("description", "thanh toán booking " + bookingId);
        String mac = getCreateOrderHmac(bookingId, chargingId, amount, time);
        obj.put("mac", mac);
        return obj;
    }

    public static JSONObject getZalopayOrderStatus(String apptransid) {
        try {
            String para = createGetStatusParameter(apptransid);
            String res = HTTPUtils.sendGetRequest(GET_ORDER_STATUS_URL+para);
            //System.out.println(res);
            return new JSONObject(res);
        } catch (IOException ex) {
            Logger.getLogger(ZalopayUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    private static String createGetStatusParameter(String apptransid){
        String mac = getGetStatucHmac(apptransid);
        return "?appid=" + APP_ID + "&apptransid=" + apptransid
                + "&mac="+mac;
    }
    
    //for long orders 
    private static String createZalopayOrderParameter(String bookingId, String chargingId, long amount, long time) throws UnsupportedEncodingException {
        return "?appid=" + APP_ID + "&apptransid=" + chargingId
                + "&appuser=hotel_api&apptime=" + time + "&description=" + URLEncoder.encode("thanh Toan Booking", "UTF-8")
                + bookingId + "&embeddata=" + URLEncoder.encode("{}", "UTF-8")
                + "&item=" + URLEncoder.encode("[]", "UTF-8")
                + "&amount=" + amount
                + "&mac=" + getCreateOrderHmac(bookingId, chargingId, amount, time);
    }

    //for long orders 
    public static JSONObject sendZalopayOrderRequest(String bookingId, String chargingId, long amount, long time) {
        try {
            String para = createZalopayOrderParameter(bookingId, chargingId, amount, time);
            String res = HTTPUtils.sendPostRequest(CREATE_ORDER_URL + para, null);
            System.out.println(res);
            return new JSONObject(res);
        } catch (IOException ex) {
            Logger.getLogger(ZalopayUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
