/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.database;

import baolt.hotel.api.dto.Booking;
import baolt.hotel.api.dto.BookingStatus;
import static baolt.hotel.api.util.APIHelper.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author baolt
 */
public class BookingDB{

    private static final String TABLE_NAME = "Booking";
    private static final String ID_COL = "bookingID";
    private static final String ROOM_ID_COL = "roomID";
    private static final String CUSTOMER_ID_COL = "customerID";
    private static final String BOOKING_DATE_COL = "bookingDate";
    private static final String FROM_DATE_COL = "fromDate";
    private static final String TO_DATE_COL = "toDate";
    private static final String BOOKING_STATUS_COL = "bookingStatus";

    private DataSource dataSource = null;
        

    public BookingDB() {
        dataSource = DataSource.getInstance();
    }

    private Booking resultSetToBooking(ResultSet rs) throws SQLException {
        Booking booking = new Booking();
        booking.setBookingId(rs.getString(ID_COL));
        booking.setRoomId(rs.getString(ROOM_ID_COL));
        booking.setCustomerId(rs.getString(CUSTOMER_ID_COL));
        Timestamp ts = rs.getTimestamp(BOOKING_DATE_COL);
        booking.setBookingDate(convertTimestampToDate(ts));
        ts = rs.getTimestamp(FROM_DATE_COL);
        booking.setFromDate(convertTimestampToDate(ts));
        ts = rs.getTimestamp(TO_DATE_COL);
        booking.setToDate(convertTimestampToDate(ts));
        int bsInt = rs.getInt(BOOKING_STATUS_COL);
        booking.setBookingStatus(BookingStatus.fromInt(bsInt));
        return booking;

    }

    public ArrayList<Booking> getAllBookings() throws SQLException {
        ArrayList<Booking> bookings = new ArrayList<>();
        try (Connection conn = dataSource.getConnection()){
            String query = "Select * from " + TABLE_NAME;
            PreparedStatement st = conn.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Booking booking = resultSetToBooking(rs);
                bookings.add(booking);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BookingDB.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } 
        return bookings;
    }

    public ArrayList<Booking> getAllActiveBookingOfRoom(String roomId) throws SQLException {
        ArrayList<Booking> bookings = new ArrayList<>();
        try (Connection conn = dataSource.getConnection()){
            String query = "Select * from " + TABLE_NAME + " Where bookingStatus = 1 and roomID Like ?";
            PreparedStatement st = conn.prepareStatement(query);
            st.setString(1, roomId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Booking booking = resultSetToBooking(rs);
                bookings.add(booking);
            }
        } catch (SQLException ex) {
            Logger.getLogger(BookingDB.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
        return bookings;
    }

    public Booking getBooking(String id) throws SQLException {
        Booking booking = null;
        try (Connection conn = dataSource.getConnection()){
            String query = "Select * from " + TABLE_NAME + " Where bookingId Like ?";
            PreparedStatement st = conn.prepareStatement(query);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                booking = resultSetToBooking(rs);
                return booking;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(BookingDB.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }

    public boolean addBooking(Booking booking) throws SQLException {
        try(Connection conn = dataSource.getConnection()) {
            String query = "Insert into " + TABLE_NAME + " values (?,?,?,?,?,?,?)";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, booking.getBookingId());
            pst.setString(2, booking.getCustomerId());
            pst.setString(3, booking.getRoomId());

            Date date = booking.getBookingDate();
            pst.setTimestamp(4, convertDateToTimestamp(date));

            date = booking.getFromDate();
            pst.setTimestamp(5, convertDateToTimestamp(date));

            date = booking.getToDate();
            pst.setTimestamp(6, convertDateToTimestamp(date));

            pst.setInt(7, booking.getBookingStatus().toInt());
            return pst.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(BookingDB.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }

    }

    public boolean deleteBooking(String id) throws SQLException {
        try(Connection conn = dataSource.getConnection()) {
            String query = "delete from " + TABLE_NAME + " where bookingId like ?";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, id);
            return pst.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(BookingDB.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }

    }

    public boolean updateBooking(Booking booking) throws SQLException {
        try (Connection conn = dataSource.getConnection()){
            String query = "Update " + TABLE_NAME + " set customerId = ? "
                    + ",roomId = ?,bookingDate = ? ,fromDate = ?"
                    + ",toDate = ?,bookingStatus = ?"
                    + " where bookingId like ?";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, booking.getCustomerId());
            pst.setString(2, booking.getRoomId());

            Date date = booking.getBookingDate();
            pst.setTimestamp(3, convertDateToTimestamp(date));

            date = booking.getFromDate();
            pst.setTimestamp(4, convertDateToTimestamp(date));

            date = booking.getToDate();
            pst.setTimestamp(5, convertDateToTimestamp(date));

            pst.setInt(6, booking.getBookingStatus().toInt());
            pst.setString(7, booking.getBookingId());

            return pst.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(BookingDB.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }

    }

    public boolean updateBookingStatus(String id, BookingStatus bookingStatus) throws SQLException {
        try (Connection conn = dataSource.getConnection()){
            String query = "Update " + TABLE_NAME + " set bookingStatus = ?"
                    + " where bookingID like ?";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setInt(1, bookingStatus.toInt());
            pst.setString(2, id);

            return pst.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(BookingDB.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }

}
