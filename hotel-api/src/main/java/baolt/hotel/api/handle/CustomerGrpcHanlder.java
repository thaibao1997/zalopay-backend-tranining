/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.handle;

import baolt.hotel.api.business.BUSResult;
import baolt.hotel.api.business.CustomerBUS;
import baolt.hotel.api.dto.Customer;
import baolt.hotel.api.grpc.customer.CustomerDeleteParams;
import baolt.hotel.api.grpc.customer.CustomerMsg;
import baolt.hotel.api.grpc.customer.CustomerGetAllParams;
import baolt.hotel.api.grpc.customer.CustomerServicesGrpc;
import static baolt.hotel.api.handle.HandlerHelper.getRuntimeExceptionWithMessage;
import io.grpc.stub.StreamObserver;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author baolt
 */
public class CustomerGrpcHanlder extends CustomerServicesGrpc.CustomerServicesImplBase {

    private CustomerBUS customerBUS = new CustomerBUS();

    @Override
    public void delete(CustomerDeleteParams request, StreamObserver<CustomerMsg> responseObserver) {
        BUSResult res = customerBUS.deleteCustomer(request.getId());
        handleBUSResult(res, responseObserver);
    }

    @Override
    public void update(CustomerMsg request, StreamObserver<CustomerMsg> responseObserver) {
        Customer customer = convertCustomerMsgToCustomer(request);
        BUSResult res = customerBUS.updateCustomer(customer);
        handleBUSResult(res, responseObserver);
    }

    @Override
    public void add(CustomerMsg request, StreamObserver<CustomerMsg> responseObserver) {
        Customer customer = convertCustomerMsgToCustomer(request);
        BUSResult res = customerBUS.addCustomer(customer);
        handleBUSResult(res, responseObserver);
    }
    
    

    @Override
    public void getAll(CustomerGetAllParams request, StreamObserver<CustomerMsg> responseObserver) {
        Collection<Customer> customers = customerBUS.getAllCustomers();
        if (customers == null) {
            responseObserver
                    .onError(getRuntimeExceptionWithMessage("Database Error"));
        } else {
            for (Customer customer : customers) {
                CustomerMsg customerE = convertCustomerToCustomerMsg(customer);
                responseObserver.onNext(customerE);
            }
            responseObserver.onCompleted();

        }
    }

    private static void handleBUSResult(BUSResult res, StreamObserver<CustomerMsg> responseObserver) {
        if (res.err) {
            responseObserver
                    .onError(getRuntimeExceptionWithMessage(res.errMsg));
        } else {
            CustomerMsg resp = convertCustomerToCustomerMsg((Customer) res.returnObj);
            responseObserver.onNext(resp);
            responseObserver.onCompleted();
        }
    }

    public static Customer convertCustomerMsgToCustomer(CustomerMsg customerE) {
        Customer customer = new Customer();
        customer.setCustomerId(customerE.getCustomerId());
        customer.setFullName(customerE.getFullName());
        customer.setAddress(customerE.getAddress());
        customer.setPhoneNumber(customerE.getPhoneNumber());
        customer.setDayOfBirth(new Date(customerE.getDayOfBirth()));
        customer.setCreateDate(new Date(customerE.getCreateDate()));
        return customer;
    }

    public static CustomerMsg convertCustomerToCustomerMsg(Customer customer) {
        CustomerMsg customerE = CustomerMsg.newBuilder()
                .setCustomerId(customer.getCustomerId())
                .setFullName(customer.getFullName())
                .setAddress(customer.getAddress())
                .setPhoneNumber(customer.getPhoneNumber())
                .setDayOfBirth(customer.getDayOfBirth().getTime())
                .setCreateDate(customer.getCreateDate().getTime())
                .build();
        return customerE;
    }
}
