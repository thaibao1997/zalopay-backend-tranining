/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.handle;

import baolt.hotel.api.business.BUSResult;
import baolt.hotel.api.business.CheckoutBUS;
import baolt.hotel.api.dto.Checkout;
import baolt.hotel.api.grpc.checkout.CheckoutDeleteParams;
import baolt.hotel.api.grpc.checkout.CheckoutMsg;
import baolt.hotel.api.grpc.checkout.CheckoutGetAllParams;
import baolt.hotel.api.grpc.checkout.CheckoutServicesGrpc;
import static baolt.hotel.api.handle.HandlerHelper.getRuntimeExceptionWithMessage;
import io.grpc.stub.StreamObserver;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author baolt
 */
public class CheckoutGrpcHandler extends CheckoutServicesGrpc.CheckoutServicesImplBase {

    CheckoutBUS checkoutBUS = new CheckoutBUS();

    @Override
    public void delete(CheckoutDeleteParams request, StreamObserver<CheckoutMsg> responseObserver) {
        String roomId = request.getRoomId();
        String bookingId = request.getBookingId();
        BUSResult res = checkoutBUS.deleteCheckout(bookingId, roomId);
        handleBUSResult(res, responseObserver);
    }

    @Override
    public void update(CheckoutMsg request, StreamObserver<CheckoutMsg> responseObserver) {
        Checkout checkout = convertCheckoutMsgToCheckout(request);
        BUSResult res = checkoutBUS.updateCheckout(checkout);
        handleBUSResult(res, responseObserver);
    }

    @Override
    public void add(CheckoutMsg request, StreamObserver<CheckoutMsg> responseObserver) {
        Checkout checkout = convertCheckoutMsgToCheckout(request);
        BUSResult res = checkoutBUS.addCheckout(checkout);
        handleBUSResult(res, responseObserver);
    }

    @Override
    public void getAll(CheckoutGetAllParams request, StreamObserver<CheckoutMsg> responseObserver) {
        Collection<Checkout> checkouts = checkoutBUS.getAllCheckout();
        if (checkouts == null) {
            responseObserver
                    .onError(getRuntimeExceptionWithMessage("Database Error"));
        } else {
            for (Checkout checkout : checkouts) {
                CheckoutMsg checkoutE = convertCheckoutToCheckoutMsg(checkout);
                responseObserver.onNext(checkoutE);
            }
            responseObserver.onCompleted();

        }
    }

    public static void handleBUSResult(BUSResult res, StreamObserver<CheckoutMsg> responseObserver) {
        if (res.err) {
            responseObserver
                    .onError(getRuntimeExceptionWithMessage(res.errMsg));
        } else {
            CheckoutMsg resp = convertCheckoutToCheckoutMsg((Checkout) res.returnObj);
            responseObserver.onNext(resp);
            responseObserver.onCompleted();
        }
    }

    public static Checkout convertCheckoutMsgToCheckout(CheckoutMsg checkoutE) {
        Checkout checkout = new Checkout();
        checkout.setRoomId(checkoutE.getRoomId());
        checkout.setBookingId(checkoutE.getBookingId());
        checkout.setTotalAmount(checkoutE.getTotalAmount());
        Date date = new Date(checkoutE.getCheckoutDate());
        checkout.setCheckOutDate(date);
        return checkout;
    }

    public static CheckoutMsg convertCheckoutToCheckoutMsg(Checkout checkout) {
        return CheckoutMsg.newBuilder()
                .setBookingId(checkout.getBookingId())
                .setRoomId(checkout.getRoomId())
                .setTotalAmount(checkout.getTotalAmount())
                .setCheckoutDate(checkout.getCheckOutDate().getTime())
                .build();
    }

}
