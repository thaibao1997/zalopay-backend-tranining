/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.app.view;

import static baolt.hotel.app.view.ViewRenderer.VIEWS_FOLDER;
import hapax.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class BookingView {

    private static final String templateName = "booking.xtm";
    private static final String jsFile = "/js/booking.js";

    public static String getBookingView(JSONArray bookings, JSONArray customers, JSONArray rooms,String user) throws TemplateException {
        String content = getContentSection(bookings, customers, rooms);
        String footer = getFooterSection();
        return ViewRenderer.getMyPage("Bookings", content, footer,user);
    }

    public static String getFooterSection() {
        return ViewRenderer.getJSLink(jsFile);
    }

    public static String getContentSection(JSONArray bookings, JSONArray customers, JSONArray rooms) {
        try {
            TemplateLoader templateLoader = TemplateResourceLoader.create(VIEWS_FOLDER, true);
            Template template = templateLoader.getTemplate(templateName);
            TemplateDictionary templeDictionary = new TemplateDictionary();

            for (int i = 0; i < bookings.length(); i++) {
                TemplateDataDictionary temp = templeDictionary.addSection("booking");
                JSONObject jsonObject = bookings.getJSONObject(i);
                temp.setVariable("bookingId", jsonObject.getString("bookingId"));
                temp.setVariable("roomId", jsonObject.getString("roomId"));
                temp.setVariable("customerId", jsonObject.getString("customerId"));
                temp.setVariable("bookingDate", jsonObject.getString("bookingDate"));
                temp.setVariable("fromDate", jsonObject.getString("fromDate"));
                temp.setVariable("toDate", jsonObject.getString("toDate"));
                int status= jsonObject.getInt("bookingStatus");
                temp.setVariable("bookingStatus", status == 2 ? "Da Tra" : "Active" );
                if(status == 2){
                    temp.setVariable("enableTinhTien","disabled");
                }
            }
            
            for (int i = 0; i < customers.length(); i++) {
                TemplateDataDictionary temp = templeDictionary.addSection("customer");
                JSONObject jsonObject = customers.getJSONObject(i);

                String id = jsonObject.getString("customerId");
                temp.setVariable("customerId", id);
                String name = jsonObject.getString("fullName");
                temp.setVariable("customerName", id + " - " + name);

            }

            for (int i = 0; i < rooms.length(); i++) {
                TemplateDataDictionary temp = templeDictionary.addSection("room");
                JSONObject jsonObject = rooms.getJSONObject(i);

                String roomId = jsonObject.getString("roomId");
                temp.setVariable("roomId", roomId);
                String roomName = jsonObject.getString("roomName");
                temp.setVariable("roomName", roomId + " - " + roomName);

            }

            return template.renderToString(templeDictionary);
        } catch (TemplateException ex) {
            Logger.getLogger(RoomView.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
