/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.business;

import baolt.hotel.api.dataacess.*;
import baolt.hotel.api.dto.Booking;
import baolt.hotel.api.dto.BookingStatus;
import baolt.hotel.api.dto.ChargeStatus;
import baolt.hotel.api.dto.Charging;
import baolt.hotel.api.dto.Checkout;
import baolt.hotel.api.dto.Customer;
import baolt.hotel.api.dto.Menu;
import baolt.hotel.api.dto.Room;
import baolt.hotel.api.hmacutil.HMACUtil;
import baolt.hotel.api.idgenerate.IDGenHelper;
import baolt.hotel.api.idgenerate.IDGenerator;
import baolt.hotel.api.util.APIHelper;
import baolt.hotel.api.zalopay.PaymentCheckingThread;
import baolt.hotel.api.zalopay.ZalopayUtils;
import baolt.hotel.api.zookeeper.ZookeeperClient;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class BookingBUS {

    public static final String FK_ERR_MSG = "Room or Customer not exist";
    public static final String FD_TD_ERR_MSG = "To Date cannot be before from date";
    public static final String PAST_BOOKING_ERR_MSG = "Cannot not book in the past";
    public static final String NOT_ACTIVE_ROOM_ERR_MSG = "This Room Is Not Active";
    public static final String CHECKOUTED_BOOKING_ERR_MSG = "This Room Has Already Checkouted";

    private CheckoutBUS checkoutBUS;
    private MenuBUS menuBUS;
    private BookingDAO bookingDAO;
    private RoomDAO roomDAO;
    private CustomerDAO customerDAO;

    private IDGenerator idGen;

    private ZookeeperClient zookeeperClient;

    public BookingBUS() {
        bookingDAO = new BookingDAO();
        roomDAO = new RoomDAO();
        customerDAO = new CustomerDAO();
        checkoutBUS = new CheckoutBUS();
        menuBUS = new MenuBUS();
        idGen = IDGenerator.getInstance();

        zookeeperClient = ZookeeperClient.getInstance();
    }

    public Collection<Booking> getAllBookings() {
        try {
            return bookingDAO.getAllBookings();
        } catch (SQLException ex) {
            return null;
        }
    }

    public BUSResult getBookingByID(String id) {
        if (id.isEmpty()) {
            return BUSHelper.NULL_ID_ERR;
        }
        try {
            Booking booking = bookingDAO.getBooking(id);
            if (booking == null) {
                return BUSHelper.ID_NOT_FOUND_ERR;
            }
            return new BUSResult(booking);
        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, FK_ERR_MSG);
        }
    }

    public BUSResult checkBUS(Booking booking) throws SQLException {
        Room room = roomDAO.getRoom(booking.getRoomId());
        if (room == null) {
            return new BUSResult(FK_ERR_MSG);
        }

        Customer customer = customerDAO.getCustomer(booking.getCustomerId());
        if (customer == null) {
            return new BUSResult(FK_ERR_MSG);
        }

        long ifromDate = booking.getFromDate().getTime();
        long itoDate = booking.getToDate().getTime();
        long nowTime = (new Date()).getTime();

        if (nowTime > ifromDate) {
            return new BUSResult(PAST_BOOKING_ERR_MSG);
        }

        if (ifromDate > itoDate) {
            return new BUSResult(FD_TD_ERR_MSG);
        }
        String roomId = booking.getRoomId();
        if (room != null && !room.isIsActive()) {
            return new BUSResult(NOT_ACTIVE_ROOM_ERR_MSG);
        }

        ArrayList<Booking> list = bookingDAO.getAllActiveBookingOfRoom(roomId);
        if (list != null) {
            boolean err = true;
            for (Booking abooking : list) {
                long afromDate = abooking.getFromDate().getTime();
                long atoDate = abooking.getToDate().getTime();

                if (!(afromDate < ifromDate && atoDate < itoDate && ifromDate > atoDate)
                        && !(afromDate > ifromDate && atoDate > itoDate && ifromDate < atoDate)) {
                    return new BUSResult("This Room Is Already Booked. BookingID: " + abooking.getBookingId());
                }
            }
        }

        return new BUSResult(null, false, null);
    }

    public BUSResult addBooking(Booking booking) {

        try {

            BUSResult res = checkBUS(booking);

            if (res.err) {
                return res;
            }
            String bookingId = "";
            Booking booking_ = null;
            do {
                bookingId = idGen.getBookingID();
                booking_ = bookingDAO.getBooking(bookingId);
            } while (booking_ != null);
            booking.setBookingId(bookingId);

            if (bookingDAO.addBooking(booking)) {
                return new BUSResult(booking);
            }
        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, FK_ERR_MSG);
        }
        return BUSHelper.DB_ERR;
    }

    public BUSResult deleteBooking(String id) {
        try {
            Booking booking = bookingDAO.getBooking(id);
            if (booking != null) {

                return bookingDAO.deleteBooking(id)
                        ? new BUSResult(booking) : BUSHelper.DB_ERR;
            }
            return BUSHelper.ID_NOT_FOUND_ERR;
        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, FK_ERR_MSG);
        }
    }

    public BUSResult updateBooking(Booking booking) {
        try {
            BUSResult res = checkBUS(booking);
            if (res.err) {
                return res;
            }
            String bookingId = booking.getBookingId();

            Booking booking_ = bookingDAO.getBooking(bookingId);
            if (booking_ == null) {
                return BUSHelper.ID_NOT_FOUND_ERR;
            }

            booking.setBookingDate(booking_.getBookingDate());
            if (bookingDAO.updateBooking(booking)) {
                return new BUSResult(booking);
            }

        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, FK_ERR_MSG);

        }
        return BUSHelper.DB_ERR;

    }

    public BUSResult getAssociatedCheckout(String bookingId) {
        try {
            Booking booking = bookingDAO.getBooking(bookingId);
            if (booking == null) {
                return BUSHelper.ID_NOT_FOUND_ERR;
            }
            if(booking.getBookingStatus() == BookingStatus.ACTIVE){
                return new BUSResult("Booking is not checkouted yet");
            }
            String roomId = booking.getRoomId();
            return checkoutBUS.getCheckoutByID(bookingId, roomId);
        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, FK_ERR_MSG);
        }
    }

    public BUSResult checkout(String id) {
        BUSResult bookingRes = getBookingByID(id);
        if (bookingRes.err) {
            return BUSHelper.ID_NOT_FOUND_ERR;
        }
        Booking booking = (Booking) bookingRes.returnObj;
        if (booking.getBookingStatus() == BookingStatus.DATRA) {
            return new BUSResult(CHECKOUTED_BOOKING_ERR_MSG);
        }

        String roomId = booking.getRoomId();

        BUSResult menuRes = menuBUS.getActiveMenuOfRoom(roomId);
        if (menuRes.err) {
            return new BUSResult("This room doesn't have price. please add a menu for this room");
        }

        Menu menu = (Menu) menuRes.returnObj;
        //long amount = caculateTotalAmount(booking,menu);
        long amount = 100000;

        String node = zookeeperClient.createNode(id);
        if (node != null) {
            Date now = new Date();
            String chargingId = IDGenHelper.getChargingIdFromBookingId(id, now);
            PaymentCheckingThread paymentThread
                    = new PaymentCheckingThread(id, roomId, chargingId, amount, now);
            paymentThread.start();
            //create qr code
            JSONObject jsonObj = ZalopayUtils.createZalopayOrder(id, chargingId, amount, now.getTime());
            //jsonObj.put("expireTime", PaymentCheckingThread.EXPIRE_PAYMENT_TIME);
            return new BUSResult(jsonObj);
        } else {
            return new BUSResult("Another Payment is still progressing");
        }
    }

    private long caculateTotalAmount(Booking booking, Menu menu) {
        Date fromDate = booking.getFromDate();
        Date toDate = booking.getToDate();
        long days = APIHelper.getDateDistance(fromDate, toDate) + 1;
        return days * menu.getAmount();
    }
}
