/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.app.model;

import baolt.hotel.api.grpc.booking.BookingCheckoutMsg;
import baolt.hotel.api.grpc.booking.BookingCheckoutParams;
import baolt.hotel.api.grpc.booking.BookingDeleteParams;
import baolt.hotel.api.grpc.booking.BookingGetAllParams;
import baolt.hotel.api.grpc.booking.BookingMsg;
import baolt.hotel.api.grpc.booking.BookingMsgStatus;
import baolt.hotel.api.grpc.booking.BookingServicesGrpc;
import static baolt.hotel.app.model.ModelHelper.createErrorJSONObject;
import baolt.hotel.app.utils.ServerHelper;
import io.grpc.ManagedChannel;
import java.util.Date;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class BookingModel {

    private final ManagedChannel channel;
    private final BookingServicesGrpc.BookingServicesBlockingStub blockingStub;

    public BookingModel() {
        this.channel = ModelHelper.getChannel();
        this.blockingStub = BookingServicesGrpc.newBlockingStub(channel);
    }

    public JSONArray getAllBooking() {
        JSONArray arr = new JSONArray();
        if (channel.isShutdown() || channel.isTerminated()) {
            System.out.println("Server died");
            return arr;
        }
        BookingGetAllParams params = BookingGetAllParams.newBuilder().build();
        try {
            Iterator<BookingMsg> reply = blockingStub.getAll(params);
            while (reply.hasNext()) {
                BookingMsg booking = reply.next();
                JSONObject json = convertBookingMsgToJSON(booking);
                arr.put(json);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return arr;
    }

    public JSONObject addBooking(JSONObject json) {
        if (channel.isShutdown() || channel.isTerminated()) {
            return createErrorJSONObject("API Server died");
        }
        try {
            BookingMsg request = convertJSONObjectToBookingMsg(json);
            BookingMsg response = blockingStub.add(request);
            return convertBookingMsgToJSON(response);
        } catch (Exception ex) {
            return createErrorJSONObject(ex.getMessage());
        }
    }

    public JSONObject updateBooking(JSONObject json) {
        if (channel.isShutdown() || channel.isTerminated()) {
            return createErrorJSONObject("API Server died");
        }
        try {
            BookingMsg request = convertJSONObjectToBookingMsg(json);
            BookingMsg response = blockingStub.update(request);
            return convertBookingMsgToJSON(response);
        } catch (Exception ex) {
            return createErrorJSONObject(ex.getMessage());
        }
    }

    public JSONObject deleteBooking(String bookingId) {
        if (channel.isShutdown() || channel.isTerminated()) {
            return createErrorJSONObject("API Server died");
        }
        try {
            BookingDeleteParams dltp = BookingDeleteParams.newBuilder().setId(bookingId)
                    .build();
            BookingMsg response = blockingStub.delete(dltp);
            return convertBookingMsgToJSON(response);
        } catch (Exception ex) {
            return createErrorJSONObject(ex.getMessage());
        }
    }

    public JSONObject checkoutBooking(String bookingId) {
        if (channel.isShutdown() || channel.isTerminated()) {
            return createErrorJSONObject("API Server died");
        }
        try {
            BookingCheckoutParams params = BookingCheckoutParams.newBuilder().setId(bookingId)
                    .build();
            BookingCheckoutMsg response = blockingStub.checkout(params);
            return convertBookingCheckoutMsgToJSON(response);
        } catch (Exception ex) {
            return createErrorJSONObject(ex.getMessage());
        }
    }

    public static JSONObject convertBookingCheckoutMsgToJSON(BookingCheckoutMsg checkout) {
        JSONObject json = new JSONObject();
        json.put("qrStr", checkout.getQrStr());
        json.put("apptransid", checkout.getApptransid());
        json.put("expiretime", checkout.getExpireTime());
        return json;
    }

    public static JSONObject convertBookingMsgToJSON(BookingMsg booking) {
        JSONObject json = new JSONObject();

        json.put("bookingId", booking.getBookingId());
        json.put("roomId", booking.getRoomId());
        json.put("customerId", booking.getCustomerId());
        json.put("bookingStatus", booking.getBookingStatus().getNumber() + 1);
        Date date = new Date(booking.getBookingDate());
        json.put("bookingDate", ServerHelper.getDateStr(date));
        date = new Date(booking.getToDate());
        json.put("toDate", ServerHelper.getDateStr(date));
        date = new Date(booking.getFromDate());
        json.put("fromDate", ServerHelper.getDateStr(date));
        return json;
    }

    public static BookingMsg convertJSONObjectToBookingMsg(JSONObject json) {
        Date fromDate = ServerHelper.getDateFromStr(json.getString("fromDate"));
        Date toDate = ServerHelper.getDateFromStr(json.getString("toDate"));
        int status = BookingMsgStatus.ACTIVE.getNumber();
        if (json.has("bookingStatus")) {
            status = json.getInt("bookingStatus") - 1;
        }
        String bookingId;
        if (json.has("bookingId")) {
            bookingId = json.getString("bookingId");
        } else {
            bookingId = "";
        }

        return BookingMsg.newBuilder()
                .setBookingId(bookingId)
                .setCustomerId(json.getString("customerId"))
                .setRoomId(json.getString("roomId"))
                .setBookingStatus(BookingMsgStatus.forNumber(status))
                .setBookingDate(new Date().getTime())
                .setFromDate(fromDate.getTime())
                .setToDate(toDate.getTime())
                .build();
    }

//    @Override
//    protected void finalize() throws Throwable {
//        channel.shutdown();
//    }
}
