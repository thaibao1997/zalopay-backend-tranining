/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api;

import baolt.hotel.api.dataacess.DAOUtils;
import baolt.hotel.api.handle.*;
import baolt.hotel.api.idgenerate.IDGenerator;
import baolt.hotel.api.kafka.KafkaLogConsumer;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author baolt
 */
public class ApiServer {

    public static void main(String[] args) {

        Server server;
        int port = 7070;
        RoomGrpcHandler roomHanlder = new RoomGrpcHandler();
        CustomerGrpcHanlder customerHandler = new CustomerGrpcHanlder();
        MenuGrpcHandler menuHandler = new MenuGrpcHandler();
        CheckoutGrpcHandler checkoutHandler = new CheckoutGrpcHandler();
        BookingGrpcHandler bookingHandler = new BookingGrpcHandler();
        ChargingGrpcHandler chargingHandler = new ChargingGrpcHandler();

        server = ServerBuilder.forPort(port)
                .addService(roomHanlder)
                .addService(customerHandler)
                .addService(menuHandler)
                .addService(checkoutHandler)
                .addService(bookingHandler)
                .addService(menuHandler)
                .addService(chargingHandler)
                .build();
        try {
            
            
            //start generate id for booking
            IDGenerator idGen = IDGenerator.getInstance();
            idGen.startGenerateID();
            
            //fill all caches on start up
            DAOUtils.fillAllCaches();
            
            //start log cosumser
            KafkaLogConsumer logConsumer = new KafkaLogConsumer();
            logConsumer.startConsume();

            //start server
            server.start();
            System.out.println("Server started at: " + port);

            server.awaitTermination();

        } catch (IOException ex) {
            Logger.getLogger(ApiServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InterruptedException ex) {
            Logger.getLogger(ApiServer.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
