/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.cache;

import static baolt.hotel.api.cache.CacheConnect.MAX_TRY_LOCK_TIME;
import static baolt.hotel.api.cache.CacheConnect.UNLOCK_AFTER_TIME;
import baolt.hotel.api.dto.Menu;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.redisson.api.RLocalCachedMap;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RedissonClient;

/**
 *
 * @author baolt
 */
public class MenuCache {

    public static final String KEY = "HOTEL_API_MENUS";
    public static final String LOCK_NAME = "MENU_LOCK";

    private final RLocalCachedMap<String, Menu> cachedMenus;
    private final RReadWriteLock rwlock;

    private static MenuCache instance = new MenuCache();

    public static MenuCache getInstance() {
        return instance;
    }

    private MenuCache() {
        RedissonClient client = CacheConnect.getRedissonClient();
        cachedMenus = client.getLocalCachedMap(KEY, CacheConnect.getLocalCachedMapOption());
        rwlock = client.getReadWriteLock(LOCK_NAME);
        flushCaches();
    }

    public void flushCaches() {
        if (cachedMenus.isExists()) {
            cachedMenus.clear();
            cachedMenus.expire(0, TimeUnit.SECONDS);
        }
    }

    private void unlockReadLock() {
        if (rwlock.readLock().isHeldByCurrentThread()) {
            rwlock.readLock().unlock();
        }
    }

    private void unlockWriteLock() {
        if (rwlock.writeLock().isHeldByCurrentThread()) {
            rwlock.writeLock().unlock();
        }
    }

    public boolean isEmpty() {
        return cachedMenus.isEmpty();
    }

    public boolean isExist() {
        return cachedMenus.isExists();
    }

    public boolean addMenu(Menu menu) {
        if (menu == null) {
            return false;
        }
        try {
            if (rwlock.writeLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                return cachedMenus.fastPut(menu.getMenuId(), menu);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(MenuCache.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            unlockWriteLock();
        }
        return false;
    }

    public boolean updateMenu(Menu menu) {
        if (menu == null) {
            return false;
        }
        try {
            if (rwlock.writeLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                return cachedMenus.fastReplace(menu.getMenuId(), menu);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(MenuCache.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            unlockWriteLock();
        }
        return false;
    }

    public Menu findMenuByID(String id) {
        try {
            if (rwlock.readLock().tryLock(5, 2, TimeUnit.SECONDS)) {
                return cachedMenus.get(id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            unlockReadLock();
        }
        return null;
    }

    public boolean deleteMenu(String id) {
        try {
            if (rwlock.writeLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                Menu menu = cachedMenus.get(id);
                return cachedMenus.fastRemove(id) > 0;
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(MenuCache.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            unlockWriteLock();
        }
        return false;
    }

    public Collection<Menu> toCollection() {
        try {
            if (rwlock.readLock().tryLock(5, 2, TimeUnit.SECONDS)) {
                return cachedMenus.values();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            unlockReadLock();
        }
        return null;
    }

    public void fromList(ArrayList<Menu> menus) {
        for (Menu menu : menus) {
            addMenu(menu);
        }
    }

    public void deactiveAllOfRoom(String roomId) {
        try {
            if (rwlock.readLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {

                Collection<Menu> menus = cachedMenus.values();
                for (Menu menu : menus) {
                    if (menu.getRoomId().equals(roomId)) {
                        menu.setIsActive(false);
                        cachedMenus.fastReplace(menu.getMenuId(), menu);
                    }
                }

            }
        } catch (InterruptedException ex) {
            Logger.getLogger(MenuCache.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            unlockReadLock();
        }
    }

    public Menu getActiveMenuOfRoom(String roomId) {
        try {
            if (rwlock.readLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {

                Collection<Menu> menus = cachedMenus.values();
                for (Menu menu : menus) {
                    if (menu.getRoomId().equals(roomId) && menu.isIsActive()) {
                        return menu;
                    }
                }

            }
        } catch (InterruptedException ex) {
            Logger.getLogger(MenuCache.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            unlockReadLock();
        }
        return null;
    }

}
