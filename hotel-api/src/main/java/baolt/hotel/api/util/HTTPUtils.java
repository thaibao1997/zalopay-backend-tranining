/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class HTTPUtils {

    public static String getResponseString(HttpResponse response) throws IOException {
        BufferedReader rd = new BufferedReader(new InputStreamReader(
                response.getEntity().getContent()));
        StringBuilder content = new StringBuilder();
        String line = "";
        while ((line = rd.readLine()) != null) {
            content.append(line);
        }
        return content.toString();
    }

    public static String sendPostRequest(String url, JSONObject jsonObject) throws IOException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpPost postReq = new HttpPost(url);

        if (jsonObject != null) {
            StringEntity xmlEntity = new StringEntity(jsonObject.toString(), "UTF-8");
            postReq.setEntity(xmlEntity);
        }

        HttpResponse response = client.execute(postReq);

        StatusLine status = response.getStatusLine();

        int statusCode = status.getStatusCode();

        if (statusCode == 200) {
            return getResponseString(response);
        } else {
            return "{\"error\" : true,\"errorMsg\":\"" + status.getReasonPhrase() + "\"}";
        }
    }

    public static String sendGetRequest(String url) throws IOException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet getReq = new HttpGet(url);

        HttpResponse response = client.execute(getReq);

        StatusLine status = response.getStatusLine();

        int statusCode = status.getStatusCode();

        if (statusCode == 200) {
            return getResponseString(response);
        } else {
            return "{\"error\" : true,\"errorMsg\":\"" + status.getReasonPhrase() + "\"}";
        }
    }
}
