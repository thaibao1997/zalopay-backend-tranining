/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.dto;

import baolt.hotel.api.util.APIHelper;
import static baolt.hotel.api.util.APIHelper.getDateFromStr;
import java.io.Serializable;
import java.util.Date;
import java.time.LocalDateTime;
import java.util.Objects;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class Room implements JSONable, Serializable {

    private String roomId;
    private String roomName;
    private RoomType roomType;
    private Date createDate;
    private boolean isActive = true;
    private String description = "";

    public Room() {
        createDate = new Date();
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId.toUpperCase();
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public boolean isIsActive() {
        return isActive;
    }

    public void setIsActive(boolean isActive) {
        this.isActive = isActive;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "Room{" + "roomId=" + roomId + ", roomName=" + roomName + ", roomType=" + roomType + ", createDate=" + createDate + ", isActive=" + isActive + ", description=" + description + '}';
    }

    @Override
    public JSONObject toJsonObject() {
        JSONObject jsonO = new JSONObject(this);
        jsonO.put("createDate", APIHelper.getDateStr(createDate));
        jsonO.put("roomType", this.roomType.toInt());
        return jsonO;
    }

    @Override
    public void fromJsonString(String jsonStr) throws JSONException, IllegalArgumentException {
        JSONObject jsonObject = new JSONObject(jsonStr);
        String id = jsonObject.getString("roomId");
        String name = jsonObject.getString("roomName");
        int type = jsonObject.getInt("roomType");
        boolean isActive = jsonObject.getBoolean("isActive");

        if (jsonObject.has("createDate")) {
            String createStr = jsonObject.getString("createDate");
            Date dateTime = getDateFromStr(createStr);
            this.setCreateDate(dateTime);
        } else if (this.createDate == null) {
            this.setCreateDate(new Date());

        }

        this.setRoomId(id);
        this.setRoomName(name);
        this.setIsActive(isActive);
        if (type < 1 || type > 4) {
            throw new IllegalArgumentException("Room Type Illegal");
        }
        this.setRoomType(RoomType.fromInt(type));
        if (jsonObject.has("description")) {
            String descr = jsonObject.getString("description");
            this.setDescription(descr);
        } else {
            this.setDescription(null);
        }
        this.setIsActive(isActive);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + Objects.hashCode(this.roomId);
        hash = 67 * hash + Objects.hashCode(this.roomName);
        hash = 67 * hash + Objects.hashCode(this.roomType);
        hash = 67 * hash + Objects.hashCode(this.createDate);
        hash = 67 * hash + (this.isActive ? 1 : 0);
        hash = 67 * hash + Objects.hashCode(this.description);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Room other = (Room) obj;
        if (this.isActive != other.isActive) {
            return false;
        }
        if (!Objects.equals(this.roomId, other.roomId)) {
            return false;
        }
        if (!Objects.equals(this.roomName, other.roomName)) {
            return false;
        }
        if (!Objects.equals(this.description, other.description)) {
            return false;
        }
        if (this.roomType != other.roomType) {
            return false;
        }
        if ( Math.abs(this.createDate.getTime() - other.createDate.getTime()) > 1000) {
            return false;
        }

        return true;
    }
    
    

}
