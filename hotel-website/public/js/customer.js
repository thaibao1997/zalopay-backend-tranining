
function formatInputDate(datestr) {
    datestr = datestr.split("-");
    return datestr[2] + "/" + datestr[1] + "/" + datestr[0];
}

function formatResponseDate(datestr) {
    datestr = datestr.split(" ");
    datestr = datestr[0].split("/");
    return datestr[2] + "-" + datestr[1] + "-" + datestr[0];
}

var table;
$(document).ready(function () {

    table= $('#contentTable').DataTable();

    $('#addForm').on('submit', function (e) {
        e.preventDefault();
        var customerId = $('#customerId').val();
        var fullName = $('#fullName').val();
        var dayOfBirth = formatInputDate($('#dayOfBirth').val());
        var address = $('#address').val();
        var phoneNumber = $('#phoneNumber').val();
        var object = {
            customerId: customerId, fullName: fullName, dayOfBirth: dayOfBirth
            , address: address, phoneNumber: phoneNumber
        }

        $.ajax({
            type: 'POST',
            data: JSON.stringify(object),
            url: '../customer',
            success: function (result) {
                var obj = JSON.parse(result);
                if (obj.error) {
                    alert(obj.errorMsg);
                } else {
                    var html = " <tr id= :customerId>" +
                        "<td>:customerId</td>" +
                        "<td>:fullName</td>" +
                        "<td>:dayOfBirth</td>" +
                        "<td>:address</td>" +
                        "<td>:phoneNumber</td>" +
                        "<td>:createDate</td>" +
                        '<td><button onclick="updateClick(this)" data-toggle="modal" data-target="#editModal" class="btn btn-primary">' +
                        '<i class="fas fa-pencil-alt"></i> Edit</button>' +
                        '<button class="btn btn-danger" onclick="deleteClick(this)">' +
                        '<i class="fas fa-trash-alt"></i> Delete</button></td></tr>';
                    console.log(obj);
                    html = html.replace(':customerId', obj.customerId);
                    html = html.replace(':customerId', obj.customerId);
                    html = html.replace(':fullName', obj.fullName);
                    html = html.replace(':dayOfBirth', obj.dayOfBirth);
                    html = html.replace(':address', obj.address);
                    html = html.replace(':phoneNumber', obj.phoneNumber);
                    html = html.replace(':createDate', obj.createDate);
                    table.row.add($(html)).draw();
                    $('#closeModal').click();
                }
            }
        });

    });

    $('#editForm').on('submit', function (e) {
        e.preventDefault();
        var customerId = $('#customerIdE').val();
        var fullName = $('#fullNameE').val();
        var dayOfBirth = formatInputDate($('#dayOfBirthE').val());
        var address = $('#addressE').val();
        var phoneNumber = $('#phoneNumberE').val();
        var object = {
            customerId: customerId, fullName: fullName, dayOfBirth: dayOfBirth
            , address: address, phoneNumber: phoneNumber
        }
        $.ajax({
            type: 'PATCH',
            data: JSON.stringify(object),
            url: '../customer?id=' + customerId,
            success: function (result) {
                var obj = JSON.parse(result);
                if (obj.error) {
                    alert(obj.errorMsg);
                } else {
                    alert("sussess");
                    var node = document.getElementById(customerId);
                    var nodeList = node.getElementsByTagName("td");
                    nodeList[1].innerText = fullName;
                    nodeList[2].innerText = obj.dayOfBirth;
                    nodeList[3].innerText = obj.address;
                    nodeList[4].innerText = obj.phoneNumber;
                    $('#closeUpdateModal').click();
                }
            }
        });
    });

});

function deleteClick(e) {
    var id = e.parentNode.parentNode.getAttributeNode('id').value;
    if (confirm("Are you sure to delete customer: " + id)) {

        console.log("delete room " + id);
        $.ajax({
            type: 'DELETE',
            url: '../customer?id=' + id,
            success: function (result) {
                var obj = JSON.parse(result);
                if (obj.error) {
                    alert(obj.errorMsg);
                } else {
                    table.row('#' + id).remove().draw();
                }
            }

        });
    }
}


function updateClick(e) {
    var parentTr = e.parentNode.parentNode;
    var nodeList = parentTr.getElementsByTagName("td");
    console.log(nodeList);
    $('#customerIdE').val(nodeList[0].innerText.toString().trim());
    $("#fullNameE").val(nodeList[1].innerText.toString().trim());
    var date = formatResponseDate(nodeList[2].innerText.toString().trim());
    $('#dayOfBirthE').val(date);
    $('#addressE').val(nodeList[3].innerText.toString().trim());
    $('#phoneNumberE').val(nodeList[4].innerText.toString().trim());
}