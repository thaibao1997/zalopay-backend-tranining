/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.business;

import baolt.hotel.api.database.DataSource;
import baolt.hotel.api.dto.ChargeStatus;
import baolt.hotel.api.dto.Charging;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import testUtil.TestUtil;

/**
 *
 * @author baolt
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ChargingBUSTest {

    ChargingBUS instance;
    ArrayList<Charging> testChargings;

    public ChargingBUSTest() {
        instance = new ChargingBUS();
        testChargings = new ArrayList<>();

        Charging charging = new Charging();
        charging.setChargeID("181202000000001");
        charging.setBookingID("B01");
        charging.setChargeStatus(ChargeStatus.SUCCESS);
        charging.setTotalAmount(50000);
        charging.setTpeReturnCode(1);
        charging.setTpeTransID(123456);

        testChargings.add(charging);

        charging = new Charging();
        charging.setChargeID("181202000000002");
        charging.setBookingID("B02");
        charging.setChargeStatus(ChargeStatus.FAILED);
        charging.setTotalAmount(20000);
        charging.setTpeReturnCode(-117);
        charging.setTpeTransID(123457);

        testChargings.add(charging);
    }

    @BeforeClass
    public static void setUpClass() {
        TestUtil.flushAllCaches();

        DataSource.switchToTestDatabase();
        DataSource dataSource = DataSource.getInstance();
        dataSource.dropAllData();

        try (Connection connection = dataSource.getConnection()) {
            Statement st = connection.createStatement();
            String query;

            st = connection.createStatement();
            query = "Insert into Room values ('R01','Room 01',1,NOW(),true,'Phong 001')";
            st.execute(query);

            st = connection.createStatement();
            query = "Insert into Customer values ('C01','Nguyen Van A','VN',NOW(),'123456',NOW())";
            st.execute(query);

            st = connection.createStatement();
            query = "Insert into Booking values ('B01','C01','R01',NOW(),NOW(),NOW(),2)";
            st.execute(query);

            st = connection.createStatement();
            query = "Insert into Booking values ('B02','C01','R01',NOW(),NOW(),NOW(),2)";
            st.execute(query);

        } catch (SQLException ex) {
            Logger.getLogger(RoomBUSTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @AfterClass
    public static void tearDownClass() {
        TestUtil.flushAllCaches();
        DataSource.swicthToMainDatabase();
    }

    /**
     * Test of addCharging method, of class ChargingBUS.
     */
    @Test
    public void A_testAddCharging() {
        System.out.println("addCharging");
        Charging charging = testChargings.get(0);
        BUSResult expResult = new BUSResult(charging);
        BUSResult result = instance.addCharging(charging);
        assertEquals(expResult, result);

        charging = testChargings.get(1);
        expResult = new BUSResult(charging);
        result = instance.addCharging(charging);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAllChargings method, of class ChargingBUS.
     */
    @Test
    public void B_testGetAllChargings() {
        System.out.println("getAllChargings");

        Collection<Charging> result = instance.getAllChargings();
        assertEquals(result.size(), testChargings.size());
    }

//
    /**
     * Test of getChargingByID method, of class ChargingBUS.
     */
    @Test
    public void D_testGetChargingByID() {
        System.out.println("getChargingByID");
        String id = testChargings.get(0).getChargeID();
        BUSResult expResult = new BUSResult(testChargings.get(0));
        BUSResult result = instance.getChargingByID(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateCharging method, of class ChargingBUS.
     */
    @Test
    public void E_testUpdateCharging() {
        System.out.println("updateCharging");
        Charging charging = testChargings.get(0);
        charging.setTotalAmount(1000000);
        charging.setChargeStatus(ChargeStatus.EXCEPTION);
        charging.setTpeReturnCode(-1);
        BUSResult expResult = new BUSResult(charging);
        BUSResult result = instance.updateCharging(charging);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteCharging method, of class ChargingBUS.
     */
    @Test
    public void testDeleteCharging() {
        System.out.println("deleteCharging");
        Charging charging = testChargings.get(1);
        String id = charging.getChargeID();
        BUSResult expResult = new BUSResult(charging);
        BUSResult result = instance.deleteCharging(id);
        assertEquals(expResult, result);
    }

}
