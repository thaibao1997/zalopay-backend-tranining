/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.app.model;

import baolt.hotel.api.grpc.checkout.CheckoutDeleteParams;
import baolt.hotel.api.grpc.checkout.CheckoutMsg;
import baolt.hotel.api.grpc.checkout.CheckoutGetAllParams;
import baolt.hotel.api.grpc.checkout.CheckoutServicesGrpc;
import static baolt.hotel.app.model.ModelConstansts.*;
import static baolt.hotel.app.model.ModelHelper.createErrorJSONObject;
import baolt.hotel.app.utils.ServerHelper;
import static baolt.hotel.app.utils.ServerHelper.*;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class CheckoutModel {

    private final ManagedChannel channel;
    private final CheckoutServicesGrpc.CheckoutServicesBlockingStub blockingStub;

    public CheckoutModel() {
        this.channel = ModelHelper.getChannel();
        this.blockingStub = CheckoutServicesGrpc.newBlockingStub(channel);
    }

    public JSONArray getAllCheckout() {
        JSONArray arr = new JSONArray();
        if (channel.isShutdown() || channel.isTerminated()) {
            System.out.println("Server died");
            return arr;
        }
        CheckoutGetAllParams params = CheckoutGetAllParams.newBuilder().build();
        try {
            Iterator<CheckoutMsg> reply = blockingStub.getAll(params);
            while (reply.hasNext()) {
                CheckoutMsg checkout = reply.next();
                JSONObject json = convertCheckoutMsgToJSON(checkout);
                arr.put(json);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return arr;
    }

    public JSONObject addCheckout(JSONObject json) {
        if (channel.isShutdown() || channel.isTerminated()) {
            return createErrorJSONObject("API Server died");
        }
        try {
            CheckoutMsg request = convertJSONObjectToCheckoutMsg(json);
            CheckoutMsg response = blockingStub.add(request);
            return convertCheckoutMsgToJSON(response);
        } catch (Exception ex) {
            return createErrorJSONObject(ex.getMessage());
        }
    }

    public JSONObject updateCheckout(JSONObject json) {
        if (channel.isShutdown() || channel.isTerminated()) {
            return createErrorJSONObject("API Server died");
        }
        try {
            CheckoutMsg request = convertJSONObjectToCheckoutMsg(json);
            CheckoutMsg response = blockingStub.update(request);
            return convertCheckoutMsgToJSON(response);
        } catch (Exception ex) {
            return createErrorJSONObject(ex.getMessage());
        }
    }

    public JSONObject deleteCheckout(String bookingId,String roomId) {
        if (channel.isShutdown() || channel.isTerminated()) {
            return createErrorJSONObject("API Server died");
        }
        try {
            CheckoutDeleteParams dltp = CheckoutDeleteParams.newBuilder().setBookingId(bookingId)
                                        .setRoomId(roomId).build();
            CheckoutMsg response = blockingStub.delete(dltp);
            return convertCheckoutMsgToJSON(response);
        } catch (Exception ex) {
            return createErrorJSONObject(ex.getMessage());
        }
    }

    public static JSONObject convertCheckoutMsgToJSON(CheckoutMsg checkout) {
        JSONObject json = new JSONObject();
        json.put("bookingId", checkout.getBookingId());
        json.put("roomId", checkout.getRoomId());
        json.put("totalAmount", checkout.getTotalAmount());
        Date date = new Date(checkout.getCheckoutDate());
        json.put("checkOutDate", ServerHelper.getDateStr(date));
        return json;
    }

    public static CheckoutMsg convertJSONObjectToCheckoutMsg(JSONObject json) {
        return CheckoutMsg.newBuilder()
                .setBookingId(json.getString("bookingId"))
                .setRoomId(json.getString("roomId"))
                .setTotalAmount(json.getLong("totalAmount"))
                .setCheckoutDate(new Date().getTime())
                .build();
    }

}
