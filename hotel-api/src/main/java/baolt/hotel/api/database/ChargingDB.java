/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.database;

import baolt.hotel.api.dto.ChargeStatus;
import baolt.hotel.api.dto.Charging;
import static baolt.hotel.api.util.APIHelper.convertDateToTimestamp;
import static baolt.hotel.api.util.APIHelper.convertTimestampToDate;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author baolt
 */
public class ChargingDB {

    private static final String TABLE_NAME = "Charging";
    private static final String ID_COL = "chargeID";
    private static final String BOOKING_ID_COL = "bookingID";
    private static final String TOTAL_AMOUNT_COL = "totalAmount";
    private static final String CHARGE_DATE_COL = "chargeDate";
    private static final String TPE_TRANSID_COL = "tpeTransID";
    private static final String TPE_RETURN_CODE_COL = "tpeReturnCode";
    private static final String CHARGE_STATUS_COL = "chargeStatus";

    private DataSource dataSource = null;

    public ChargingDB() {
        dataSource = DataSource.getInstance();
    }

    private Charging resultSetToCharging(ResultSet rs) throws SQLException {
        Charging charging = new Charging();
        charging.setChargeID(rs.getString(ID_COL));
        charging.setBookingID(rs.getString(BOOKING_ID_COL));
        charging.setTotalAmount(rs.getLong(TOTAL_AMOUNT_COL));
        Timestamp ts = rs.getTimestamp(CHARGE_DATE_COL);
        charging.setChargeDate(convertTimestampToDate(ts));
        charging.setTpeTransID(rs.getLong(TPE_TRANSID_COL));
        charging.setTpeReturnCode(rs.getInt(TPE_RETURN_CODE_COL));
        charging.setChargeStatus(ChargeStatus.fromInt(rs.getInt(CHARGE_STATUS_COL)));
        return charging;
    }

    public ArrayList<Charging> getAllChargings() throws SQLException {
        ArrayList<Charging> chargings = new ArrayList<>();
        try (Connection conn = dataSource.getConnection()) {
            String query = "Select * from " + TABLE_NAME;
            PreparedStatement st = conn.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Charging charging = resultSetToCharging(rs);
                chargings.add(charging);
            }
        } catch (SQLException ex) {
            Logger.getLogger(ChargingDB.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
        return chargings;
    }

    public Charging getCharging(String id) throws SQLException {
        Charging charging = null;
        try (Connection conn = dataSource.getConnection()) {
            String query = "Select * from " + TABLE_NAME + " Where chargeID Like ?";
            PreparedStatement st = conn.prepareStatement(query);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                charging = resultSetToCharging(rs);
                return charging;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ChargingDB.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }

    public boolean addCharging(Charging charging) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            String query = "Insert into " + TABLE_NAME + " values (?,?,?,?,?,?,?)";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, charging.getChargeID());
            pst.setString(2, charging.getBookingID());
            pst.setLong(3, charging.getTotalAmount());
            Date date = charging.getChargeDate();
            pst.setTimestamp(4, convertDateToTimestamp(date));
            pst.setLong(5, charging.getTpeTransID());
            pst.setLong(6, charging.getTpeReturnCode());
            pst.setLong(7, charging.getChargeStatus().toInt());
            return pst.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(ChargingDB.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }

    }

    public boolean deleteCharging(String id) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            String query = "delete from " + TABLE_NAME + " where chargeID like ?";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, id);
            return pst.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(ChargingDB.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }

    }

    public boolean updateCharging(Charging charging) throws SQLException {
        try (Connection conn = dataSource.getConnection()) {
            String query = "Update " + TABLE_NAME + " set bookingID = ? "
                    + ",totalAmount = ?,chargeDate = ? ,tpeTransID = ?"
                    + ",tpeReturnCode = ?,chargeStatus = ?"
                    + " where chargeID like ?";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, charging.getBookingID());
            pst.setLong(2, charging.getTotalAmount());
            Date date = charging.getChargeDate();
            pst.setTimestamp(3, convertDateToTimestamp(date));
            pst.setLong(4, charging.getTpeTransID());
            pst.setLong(5, charging.getTpeReturnCode());
            pst.setLong(6, charging.getChargeStatus().toInt());
            pst.setString(7, charging.getChargeID());
            return pst.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(ChargingDB.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }

    }

}
