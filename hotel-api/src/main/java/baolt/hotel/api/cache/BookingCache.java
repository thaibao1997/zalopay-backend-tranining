/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.cache;

import static baolt.hotel.api.cache.CacheConnect.MAX_TRY_LOCK_TIME;
import static baolt.hotel.api.cache.CacheConnect.UNLOCK_AFTER_TIME;
import baolt.hotel.api.dto.Booking;
import baolt.hotel.api.dto.BookingStatus;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.redisson.api.RLocalCachedMap;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RedissonClient;

/**
 *
 * @author baolt
 */
public class BookingCache {

    public static final String KEY = "HOTEL_API_BOOKINGS";
    public static final String LOCK_NAME = "BOOKING_LOCK";

    private final RLocalCachedMap<String, Booking> cachedBookings;
    private final RReadWriteLock rwlock;

    private static BookingCache instance = new BookingCache();

    public static BookingCache getInstance() {
        return instance;
    }

    private BookingCache() {
        RedissonClient client = CacheConnect.getRedissonClient();
        cachedBookings = client.getLocalCachedMap(KEY, CacheConnect.getLocalCachedMapOption());
        rwlock = client.getReadWriteLock(LOCK_NAME);
        flushCaches();
    }

    public void flushCaches() {
        if (cachedBookings.isExists()) {
            cachedBookings.clear();
            cachedBookings.expire(0, TimeUnit.SECONDS);
        }
    }

    private void unlockReadLock() {
        if (rwlock.readLock().isHeldByCurrentThread()) {
            rwlock.readLock().unlock();
        }
    }

    private void unlockWriteLock() {
        if (rwlock.writeLock().isHeldByCurrentThread()) {
            rwlock.writeLock().unlock();
        }
    }

    public boolean isEmpty() {
        return cachedBookings.isEmpty();
    }

    public boolean isExist() {
        return cachedBookings.isExists();
    }

    public boolean addBooking(Booking booking) {
        if (booking == null) {
            return false;
        }
        try {
            if (rwlock.writeLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                return cachedBookings.fastPut(booking.getBookingId(), booking);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(BookingCache.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            unlockWriteLock();
        }
        return false;
    }

    public boolean updateBooking(Booking booking) {
        if (booking == null) {
            return false;
        }
        try {
            if (rwlock.writeLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                return cachedBookings.fastReplace(booking.getBookingId(), booking);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(BookingCache.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            unlockWriteLock();
        }
        return false;
    }

    public Booking findBookingByID(String id) {
        try {
            if (rwlock.readLock().tryLock(5, 2, TimeUnit.SECONDS)) {
                return cachedBookings.get(id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            unlockReadLock();
        }
        return null;
    }

    public boolean deleteBooking(String id) {
        try {
            if (rwlock.writeLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                Booking booking = cachedBookings.get(id);
                return cachedBookings.fastRemove(id) > 0;
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(BookingCache.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            unlockWriteLock();
        }
        return false;
    }

    public Collection<Booking> toCollection() {
        try {
            if (rwlock.readLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                return cachedBookings.values();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            unlockReadLock();
        }
        return null;
    }

    public void fromList(ArrayList<Booking> bookings) {
        if (bookings == null) {
            return;
        }
        for (Booking booking : bookings) {
            addBooking(booking);
        }
    }

    public ArrayList<Booking> getActiveBookingOfRoom(String roomId) {
        try {
            if (rwlock.readLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                ArrayList<Booking> res = new ArrayList<Booking>();
                Collection<Booking> bookingVals = cachedBookings.values();
                for (Booking booking : bookingVals) {

                    if (booking.getRoomId().equals(roomId)
                            && booking.getBookingStatus() == BookingStatus.ACTIVE) {
                        res.add(booking);
                    }
                }
                return res;
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(MenuCache.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            unlockReadLock();
        }
        return null;
    }

}
