/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.business;

import java.sql.SQLException;

/**
 *
 * @author baolt
 */
public class BUSHelper {

    public static final BUSResult DB_ERR = new BUSResult("Database Error");
    public static final BUSResult ID_NOT_FOUND_ERR = new BUSResult("ID cannot be found");
    public static final BUSResult NULL_ID_ERR = new BUSResult("ID cannot be null");
    public static final BUSResult DUPLICATE_ID_ERR = new BUSResult("ID already exist in database");

    public static BUSResult getResultFromException(SQLException ex, String duplicateMsg) {
        if (ex.getErrorCode() == 1062) {
            return BUSHelper.DUPLICATE_ID_ERR;
        }
        if (ex.getErrorCode() == 1452) {
            return new BUSResult(duplicateMsg);
        }
        return new BUSResult(ex.getMessage()); 
    }
}
