package baolt.hotel.api.grpc.room;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.13.0-SNAPSHOT)",
    comments = "Source: Room.proto")
public final class RoomServicesGrpc {

  private RoomServicesGrpc() {}

  public static final String SERVICE_NAME = "RoomServices";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getGetAllMethod()} instead. 
  public static final io.grpc.MethodDescriptor<baolt.hotel.api.grpc.room.RoomGetAllParams,
      baolt.hotel.api.grpc.room.RoomMsg> METHOD_GET_ALL = getGetAllMethodHelper();

  private static volatile io.grpc.MethodDescriptor<baolt.hotel.api.grpc.room.RoomGetAllParams,
      baolt.hotel.api.grpc.room.RoomMsg> getGetAllMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.room.RoomGetAllParams,
      baolt.hotel.api.grpc.room.RoomMsg> getGetAllMethod() {
    return getGetAllMethodHelper();
  }

  private static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.room.RoomGetAllParams,
      baolt.hotel.api.grpc.room.RoomMsg> getGetAllMethodHelper() {
    io.grpc.MethodDescriptor<baolt.hotel.api.grpc.room.RoomGetAllParams, baolt.hotel.api.grpc.room.RoomMsg> getGetAllMethod;
    if ((getGetAllMethod = RoomServicesGrpc.getGetAllMethod) == null) {
      synchronized (RoomServicesGrpc.class) {
        if ((getGetAllMethod = RoomServicesGrpc.getGetAllMethod) == null) {
          RoomServicesGrpc.getGetAllMethod = getGetAllMethod = 
              io.grpc.MethodDescriptor.<baolt.hotel.api.grpc.room.RoomGetAllParams, baolt.hotel.api.grpc.room.RoomMsg>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(
                  "RoomServices", "getAll"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.room.RoomGetAllParams.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.room.RoomMsg.getDefaultInstance()))
                  .setSchemaDescriptor(new RoomServicesMethodDescriptorSupplier("getAll"))
                  .build();
          }
        }
     }
     return getGetAllMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getAddMethod()} instead. 
  public static final io.grpc.MethodDescriptor<baolt.hotel.api.grpc.room.RoomMsg,
      baolt.hotel.api.grpc.room.RoomMsg> METHOD_ADD = getAddMethodHelper();

  private static volatile io.grpc.MethodDescriptor<baolt.hotel.api.grpc.room.RoomMsg,
      baolt.hotel.api.grpc.room.RoomMsg> getAddMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.room.RoomMsg,
      baolt.hotel.api.grpc.room.RoomMsg> getAddMethod() {
    return getAddMethodHelper();
  }

  private static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.room.RoomMsg,
      baolt.hotel.api.grpc.room.RoomMsg> getAddMethodHelper() {
    io.grpc.MethodDescriptor<baolt.hotel.api.grpc.room.RoomMsg, baolt.hotel.api.grpc.room.RoomMsg> getAddMethod;
    if ((getAddMethod = RoomServicesGrpc.getAddMethod) == null) {
      synchronized (RoomServicesGrpc.class) {
        if ((getAddMethod = RoomServicesGrpc.getAddMethod) == null) {
          RoomServicesGrpc.getAddMethod = getAddMethod = 
              io.grpc.MethodDescriptor.<baolt.hotel.api.grpc.room.RoomMsg, baolt.hotel.api.grpc.room.RoomMsg>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "RoomServices", "add"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.room.RoomMsg.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.room.RoomMsg.getDefaultInstance()))
                  .setSchemaDescriptor(new RoomServicesMethodDescriptorSupplier("add"))
                  .build();
          }
        }
     }
     return getAddMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getUpdateMethod()} instead. 
  public static final io.grpc.MethodDescriptor<baolt.hotel.api.grpc.room.RoomMsg,
      baolt.hotel.api.grpc.room.RoomMsg> METHOD_UPDATE = getUpdateMethodHelper();

  private static volatile io.grpc.MethodDescriptor<baolt.hotel.api.grpc.room.RoomMsg,
      baolt.hotel.api.grpc.room.RoomMsg> getUpdateMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.room.RoomMsg,
      baolt.hotel.api.grpc.room.RoomMsg> getUpdateMethod() {
    return getUpdateMethodHelper();
  }

  private static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.room.RoomMsg,
      baolt.hotel.api.grpc.room.RoomMsg> getUpdateMethodHelper() {
    io.grpc.MethodDescriptor<baolt.hotel.api.grpc.room.RoomMsg, baolt.hotel.api.grpc.room.RoomMsg> getUpdateMethod;
    if ((getUpdateMethod = RoomServicesGrpc.getUpdateMethod) == null) {
      synchronized (RoomServicesGrpc.class) {
        if ((getUpdateMethod = RoomServicesGrpc.getUpdateMethod) == null) {
          RoomServicesGrpc.getUpdateMethod = getUpdateMethod = 
              io.grpc.MethodDescriptor.<baolt.hotel.api.grpc.room.RoomMsg, baolt.hotel.api.grpc.room.RoomMsg>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "RoomServices", "update"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.room.RoomMsg.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.room.RoomMsg.getDefaultInstance()))
                  .setSchemaDescriptor(new RoomServicesMethodDescriptorSupplier("update"))
                  .build();
          }
        }
     }
     return getUpdateMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getDeleteMethod()} instead. 
  public static final io.grpc.MethodDescriptor<baolt.hotel.api.grpc.room.RoomDeleteParams,
      baolt.hotel.api.grpc.room.RoomMsg> METHOD_DELETE = getDeleteMethodHelper();

  private static volatile io.grpc.MethodDescriptor<baolt.hotel.api.grpc.room.RoomDeleteParams,
      baolt.hotel.api.grpc.room.RoomMsg> getDeleteMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.room.RoomDeleteParams,
      baolt.hotel.api.grpc.room.RoomMsg> getDeleteMethod() {
    return getDeleteMethodHelper();
  }

  private static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.room.RoomDeleteParams,
      baolt.hotel.api.grpc.room.RoomMsg> getDeleteMethodHelper() {
    io.grpc.MethodDescriptor<baolt.hotel.api.grpc.room.RoomDeleteParams, baolt.hotel.api.grpc.room.RoomMsg> getDeleteMethod;
    if ((getDeleteMethod = RoomServicesGrpc.getDeleteMethod) == null) {
      synchronized (RoomServicesGrpc.class) {
        if ((getDeleteMethod = RoomServicesGrpc.getDeleteMethod) == null) {
          RoomServicesGrpc.getDeleteMethod = getDeleteMethod = 
              io.grpc.MethodDescriptor.<baolt.hotel.api.grpc.room.RoomDeleteParams, baolt.hotel.api.grpc.room.RoomMsg>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "RoomServices", "delete"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.room.RoomDeleteParams.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.room.RoomMsg.getDefaultInstance()))
                  .setSchemaDescriptor(new RoomServicesMethodDescriptorSupplier("delete"))
                  .build();
          }
        }
     }
     return getDeleteMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static RoomServicesStub newStub(io.grpc.Channel channel) {
    return new RoomServicesStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static RoomServicesBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new RoomServicesBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static RoomServicesFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new RoomServicesFutureStub(channel);
  }

  /**
   */
  public static abstract class RoomServicesImplBase implements io.grpc.BindableService {

    /**
     */
    public void getAll(baolt.hotel.api.grpc.room.RoomGetAllParams request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.room.RoomMsg> responseObserver) {
      asyncUnimplementedUnaryCall(getGetAllMethodHelper(), responseObserver);
    }

    /**
     */
    public void add(baolt.hotel.api.grpc.room.RoomMsg request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.room.RoomMsg> responseObserver) {
      asyncUnimplementedUnaryCall(getAddMethodHelper(), responseObserver);
    }

    /**
     */
    public void update(baolt.hotel.api.grpc.room.RoomMsg request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.room.RoomMsg> responseObserver) {
      asyncUnimplementedUnaryCall(getUpdateMethodHelper(), responseObserver);
    }

    /**
     */
    public void delete(baolt.hotel.api.grpc.room.RoomDeleteParams request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.room.RoomMsg> responseObserver) {
      asyncUnimplementedUnaryCall(getDeleteMethodHelper(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetAllMethodHelper(),
            asyncServerStreamingCall(
              new MethodHandlers<
                baolt.hotel.api.grpc.room.RoomGetAllParams,
                baolt.hotel.api.grpc.room.RoomMsg>(
                  this, METHODID_GET_ALL)))
          .addMethod(
            getAddMethodHelper(),
            asyncUnaryCall(
              new MethodHandlers<
                baolt.hotel.api.grpc.room.RoomMsg,
                baolt.hotel.api.grpc.room.RoomMsg>(
                  this, METHODID_ADD)))
          .addMethod(
            getUpdateMethodHelper(),
            asyncUnaryCall(
              new MethodHandlers<
                baolt.hotel.api.grpc.room.RoomMsg,
                baolt.hotel.api.grpc.room.RoomMsg>(
                  this, METHODID_UPDATE)))
          .addMethod(
            getDeleteMethodHelper(),
            asyncUnaryCall(
              new MethodHandlers<
                baolt.hotel.api.grpc.room.RoomDeleteParams,
                baolt.hotel.api.grpc.room.RoomMsg>(
                  this, METHODID_DELETE)))
          .build();
    }
  }

  /**
   */
  public static final class RoomServicesStub extends io.grpc.stub.AbstractStub<RoomServicesStub> {
    private RoomServicesStub(io.grpc.Channel channel) {
      super(channel);
    }

    private RoomServicesStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected RoomServicesStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new RoomServicesStub(channel, callOptions);
    }

    /**
     */
    public void getAll(baolt.hotel.api.grpc.room.RoomGetAllParams request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.room.RoomMsg> responseObserver) {
      asyncServerStreamingCall(
          getChannel().newCall(getGetAllMethodHelper(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void add(baolt.hotel.api.grpc.room.RoomMsg request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.room.RoomMsg> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getAddMethodHelper(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void update(baolt.hotel.api.grpc.room.RoomMsg request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.room.RoomMsg> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getUpdateMethodHelper(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void delete(baolt.hotel.api.grpc.room.RoomDeleteParams request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.room.RoomMsg> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getDeleteMethodHelper(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class RoomServicesBlockingStub extends io.grpc.stub.AbstractStub<RoomServicesBlockingStub> {
    private RoomServicesBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private RoomServicesBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected RoomServicesBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new RoomServicesBlockingStub(channel, callOptions);
    }

    /**
     */
    public java.util.Iterator<baolt.hotel.api.grpc.room.RoomMsg> getAll(
        baolt.hotel.api.grpc.room.RoomGetAllParams request) {
      return blockingServerStreamingCall(
          getChannel(), getGetAllMethodHelper(), getCallOptions(), request);
    }

    /**
     */
    public baolt.hotel.api.grpc.room.RoomMsg add(baolt.hotel.api.grpc.room.RoomMsg request) {
      return blockingUnaryCall(
          getChannel(), getAddMethodHelper(), getCallOptions(), request);
    }

    /**
     */
    public baolt.hotel.api.grpc.room.RoomMsg update(baolt.hotel.api.grpc.room.RoomMsg request) {
      return blockingUnaryCall(
          getChannel(), getUpdateMethodHelper(), getCallOptions(), request);
    }

    /**
     */
    public baolt.hotel.api.grpc.room.RoomMsg delete(baolt.hotel.api.grpc.room.RoomDeleteParams request) {
      return blockingUnaryCall(
          getChannel(), getDeleteMethodHelper(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class RoomServicesFutureStub extends io.grpc.stub.AbstractStub<RoomServicesFutureStub> {
    private RoomServicesFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private RoomServicesFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected RoomServicesFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new RoomServicesFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<baolt.hotel.api.grpc.room.RoomMsg> add(
        baolt.hotel.api.grpc.room.RoomMsg request) {
      return futureUnaryCall(
          getChannel().newCall(getAddMethodHelper(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<baolt.hotel.api.grpc.room.RoomMsg> update(
        baolt.hotel.api.grpc.room.RoomMsg request) {
      return futureUnaryCall(
          getChannel().newCall(getUpdateMethodHelper(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<baolt.hotel.api.grpc.room.RoomMsg> delete(
        baolt.hotel.api.grpc.room.RoomDeleteParams request) {
      return futureUnaryCall(
          getChannel().newCall(getDeleteMethodHelper(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_ALL = 0;
  private static final int METHODID_ADD = 1;
  private static final int METHODID_UPDATE = 2;
  private static final int METHODID_DELETE = 3;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final RoomServicesImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(RoomServicesImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_ALL:
          serviceImpl.getAll((baolt.hotel.api.grpc.room.RoomGetAllParams) request,
              (io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.room.RoomMsg>) responseObserver);
          break;
        case METHODID_ADD:
          serviceImpl.add((baolt.hotel.api.grpc.room.RoomMsg) request,
              (io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.room.RoomMsg>) responseObserver);
          break;
        case METHODID_UPDATE:
          serviceImpl.update((baolt.hotel.api.grpc.room.RoomMsg) request,
              (io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.room.RoomMsg>) responseObserver);
          break;
        case METHODID_DELETE:
          serviceImpl.delete((baolt.hotel.api.grpc.room.RoomDeleteParams) request,
              (io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.room.RoomMsg>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class RoomServicesBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    RoomServicesBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return baolt.hotel.api.grpc.room.RoomProto.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("RoomServices");
    }
  }

  private static final class RoomServicesFileDescriptorSupplier
      extends RoomServicesBaseDescriptorSupplier {
    RoomServicesFileDescriptorSupplier() {}
  }

  private static final class RoomServicesMethodDescriptorSupplier
      extends RoomServicesBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    RoomServicesMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (RoomServicesGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new RoomServicesFileDescriptorSupplier())
              .addMethod(getGetAllMethodHelper())
              .addMethod(getAddMethodHelper())
              .addMethod(getUpdateMethodHelper())
              .addMethod(getDeleteMethodHelper())
              .build();
        }
      }
    }
    return result;
  }
}
