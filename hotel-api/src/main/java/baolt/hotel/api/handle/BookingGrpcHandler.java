/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.handle;

import baolt.hotel.api.business.BUSResult;
import baolt.hotel.api.business.BookingBUS;
import baolt.hotel.api.dto.Booking;
import baolt.hotel.api.dto.BookingStatus;
import baolt.hotel.api.dto.Checkout;
import baolt.hotel.api.grpc.booking.BookingCheckoutMsg;
import baolt.hotel.api.grpc.booking.BookingCheckoutParams;
import baolt.hotel.api.grpc.booking.BookingDeleteParams;
import baolt.hotel.api.grpc.booking.BookingMsg;
import baolt.hotel.api.grpc.booking.BookingMsgStatus;
import baolt.hotel.api.grpc.booking.BookingGetAllParams;
import baolt.hotel.api.grpc.booking.BookingServicesGrpc;
import baolt.hotel.api.grpc.checkout.CheckoutMsg;
import static baolt.hotel.api.handle.CheckoutGrpcHandler.convertCheckoutToCheckoutMsg;
import static baolt.hotel.api.handle.HandlerHelper.getRuntimeExceptionWithMessage;
import baolt.hotel.api.zalopay.PaymentCheckingThread;
import io.grpc.stub.StreamObserver;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class BookingGrpcHandler extends BookingServicesGrpc.BookingServicesImplBase {

    BookingBUS bookingBUS = new BookingBUS();


    @Override
    public void checkout(BookingCheckoutParams request, StreamObserver<BookingCheckoutMsg> responseObserver) {
        BUSResult res = bookingBUS.checkout(request.getId());
        if (res.err) {
            responseObserver
                    .onError(getRuntimeExceptionWithMessage(res.errMsg));
        } else {
            JSONObject json = (JSONObject)res.returnObj;
            String apptransid = json.getString("apptransid");
            
            
            BookingCheckoutMsg resp = BookingCheckoutMsg.newBuilder()
                                        .setQrStr(json.toString())
                                        .setApptransid(apptransid)
                                        .setExpireTime(PaymentCheckingThread.EXPIRE_PAYMENT_TIME)
                                        .build();
            responseObserver.onNext(resp);
            responseObserver.onCompleted();
        }
    }

    @Override
    public void delete(BookingDeleteParams request, StreamObserver<BookingMsg> responseObserver) {
        BUSResult res = bookingBUS.deleteBooking(request.getId());
        handleBUSResult(res, responseObserver);
    }

    @Override
    public void update(BookingMsg request, StreamObserver<BookingMsg> responseObserver) {
        Booking booking = convertBookingMsgToBooking(request);
        BUSResult res = bookingBUS.updateBooking(booking);
        handleBUSResult(res, responseObserver);
    }

    @Override
    public void add(BookingMsg request, StreamObserver<BookingMsg> responseObserver) {
        Booking booking = convertBookingMsgToBooking(request);
        BUSResult res = bookingBUS.addBooking(booking);
        handleBUSResult(res, responseObserver);
    }

    @Override
    public void getAll(BookingGetAllParams request, StreamObserver<BookingMsg> responseObserver) {
        Collection<Booking> bookings = bookingBUS.getAllBookings();
        if (bookings == null) {
            responseObserver
                    .onError(getRuntimeExceptionWithMessage("Database Error"));
        } else {
            for (Booking booking : bookings) {
                BookingMsg bookingE = convertBookingToBookingMsg(booking);
                responseObserver.onNext(bookingE);
            }
            responseObserver.onCompleted();

        }
    }

    public static void handleBUSResult(BUSResult res, StreamObserver<BookingMsg> responseObserver) {
        if (res.err) {
            responseObserver
                    .onError(getRuntimeExceptionWithMessage(res.errMsg));
        } else {
            BookingMsg resp = convertBookingToBookingMsg((Booking) res.returnObj);
            responseObserver.onNext(resp);
            responseObserver.onCompleted();
        }
    }

    public static Booking convertBookingMsgToBooking(BookingMsg bookingE) {
        Booking booking = new Booking();
        booking.setBookingId(bookingE.getBookingId());
        booking.setCustomerId(bookingE.getCustomerId());
        booking.setRoomId(bookingE.getRoomId());
        booking.setFromDate(new Date(bookingE.getFromDate()));
        booking.setToDate(new Date(bookingE.getToDate()));
        booking.setBookingDate(new Date(bookingE.getBookingDate()));
        BookingMsgStatus status = bookingE.getBookingStatus();
        booking.setBookingStatus(BookingStatus.fromInt(status.getNumber() + 1));
        return booking;
    }

    public static BookingMsg convertBookingToBookingMsg(Booking booking) {
        int bookingSt = booking.getBookingStatus().toInt() - 1;
        return BookingMsg.newBuilder().setBookingId(booking.getBookingId())
                .setCustomerId(booking.getCustomerId())
                .setRoomId(booking.getRoomId())
                .setFromDate(booking.getFromDate().getTime())
                .setToDate(booking.getToDate().getTime())
                .setBookingStatus(BookingMsgStatus.forNumber(bookingSt))
                .setBookingDate(booking.getBookingDate().getTime())
                .build();
    }
}
