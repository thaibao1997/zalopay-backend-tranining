/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.idgenerate;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author baolt
 */
public class IDGenHelper {

    private static final String BOOKINGID_DATE_FORMAT = "yyyyMMdd";
    private static final String CHARGINGID_DATE_FORMAT = "yyMMdd";
    private static final SimpleDateFormat BOOKINGID_DATE_FORMATTER = new SimpleDateFormat(BOOKINGID_DATE_FORMAT);
    private static final SimpleDateFormat CHARGING_DATE_FORMATTER = new SimpleDateFormat(CHARGINGID_DATE_FORMAT);

    public static String longToStringFixedLength(long num, int lenght) {
        int numLenght = (int) Math.floor(Math.log10(num)) + 1;
        if (numLenght < lenght) {
            String res = "";
            for (int i = 0; i < lenght - numLenght; i++) {
                res += "0";
            }
            return res + String.valueOf(num);

        }
        return String.valueOf(num);
    }

    public static String getBookingIDDateString() {
        return BOOKINGID_DATE_FORMATTER.format(new Date());
    }

    public static String getChargingIdFromBookingId(String bookingId, Date now) {
        return CHARGING_DATE_FORMATTER.format(now) + "_" + bookingId + "_" + now.getTime();
    }

}
