/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.app.view;

import static baolt.hotel.app.view.ViewRenderer.VIEWS_FOLDER;
import hapax.Template;
import hapax.TemplateDataDictionary;
import hapax.TemplateDictionary;
import hapax.TemplateException;
import hapax.TemplateLoader;
import hapax.TemplateResourceLoader;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class CheckoutView {

    private static final String templateName = "checkout.xtm";
    private static final String jsFile = "/js/checkout.js";

    public static String getMenuView(JSONArray checkouts,String user) throws TemplateException {
        String content = getContentSection(checkouts);
        String footer = getFooterSection();
        return ViewRenderer.getMyPage("Checkouts", content, footer,user);
    }

    public static String getFooterSection() {
        return ViewRenderer.getJSLink(jsFile);
    }

    public static String getContentSection(JSONArray checkouts) {
        try {
            TemplateLoader templateLoader = TemplateResourceLoader.create(VIEWS_FOLDER, true);
            Template template = templateLoader.getTemplate(templateName);
            TemplateDictionary templeDictionary = new TemplateDictionary();
            for (int i = 0; i < checkouts.length(); i++) {
                TemplateDataDictionary temp = templeDictionary.addSection("checkout");
                JSONObject jsonObject = checkouts.getJSONObject(i);
                temp.setVariable("bookingId", jsonObject.getString("bookingId"));
                temp.setVariable("roomId", jsonObject.getString("roomId"));
                temp.setVariable("totalAmount", String.valueOf(jsonObject.getInt("totalAmount")));
                temp.setVariable("checkOutDate", jsonObject.getString("checkOutDate"));
            }

            return template.renderToString(templeDictionary);
        } catch (TemplateException ex) {
            Logger.getLogger(RoomView.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
