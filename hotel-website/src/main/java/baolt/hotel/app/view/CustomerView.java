/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.app.view;

import static baolt.hotel.app.view.ViewRenderer.VIEWS_FOLDER;
import hapax.Template;
import hapax.TemplateDataDictionary;
import hapax.TemplateDictionary;
import hapax.TemplateException;
import hapax.TemplateLoader;
import hapax.TemplateResourceLoader;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class CustomerView {

    private static final String templateName = "customer.xtm";
    private static final String jsFile = "/js/customer.js";

    public static String getCustomerView(JSONArray rooms,String user) throws TemplateException {
        String content = getContentSection(rooms);
        String footer = getFooterSection();
        return ViewRenderer.getMyPage("Customers", content, footer,user);
    }

    public static String getFooterSection() {
        return ViewRenderer.getJSLink(jsFile);
    }

    public static String getContentSection(JSONArray objs) {
        try {
            TemplateLoader templateLoader = TemplateResourceLoader.create(VIEWS_FOLDER, true);
            Template template = templateLoader.getTemplate(templateName);
            TemplateDictionary templeDictionary = new TemplateDictionary();

            for (int i = 0; i < objs.length(); i++) {
                TemplateDataDictionary temp = templeDictionary.addSection("customer");
                JSONObject jsonObject = objs.getJSONObject(i);
                temp.setVariable("customerId", jsonObject.getString("customerId"));
                temp.setVariable("fullName", jsonObject.getString("fullName"));
                temp.setVariable("dayOfBirth", jsonObject.getString("dayOfBirth"));
                temp.setVariable("address", jsonObject.getString("address"));
                temp.setVariable("createDate", jsonObject.getString("createDate"));
                temp.setVariable("phoneNumber", jsonObject.getString("phoneNumber"));

            }
            return template.renderToString(templeDictionary);
        } catch (TemplateException ex) {
            Logger.getLogger(RoomView.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
