/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.dataacess;

import baolt.hotel.api.cache.CustomerCache;
import baolt.hotel.api.database.CustomerDB;
import baolt.hotel.api.dto.Customer;
import baolt.hotel.api.dto.Menu;
import static baolt.hotel.api.util.APIHelper.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author baolt
 */
public class CustomerDAO {

    CustomerCache cache = null;
    CustomerDB db = null;

    public CustomerDAO() {
        cache = CustomerCache.getInstance();
        db = new CustomerDB();
    }

    public Collection<Customer> getAllCustomers() throws SQLException {
        if (cache.isExist()) {
            return cache.toCollection();
        }
        ArrayList<Customer> cus= db.getAllCustomers();
        cache.fromList(cus);
        return cus;
    }

    public Customer getCustomer(String id) throws SQLException {
        Customer customer = cache.findCustomerByID(id);
        if (customer != null) {
            return customer;
        }
        customer = db.getCustomer(id);
        if(!cache.addCustomer(customer)){
            return null;
        }
        return customer;
    }

    public boolean addCustomer(Customer customer)  {
        boolean res = cache.addCustomer(customer);
        if(res){
            Thread thread = new Thread(()->{
                try {
                    db.addCustomer(customer);
                } catch (SQLException ex) {
                    Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            thread.start();
        }
        return res;
    }

    public boolean deleteCustomer(String id)  {
        boolean res = cache.deleteCustomer(id);
        if(res){
            Thread thread = new Thread(()->{
                try {
                    db.deleteCustomer(id);
                } catch (SQLException ex) {
                    Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            thread.start();
        }
        return res;

    }

    public boolean updateCustomer(Customer customer) {
        boolean res = cache.updateCustomer(customer);
        if(res){
            Thread thread = new Thread(()->{
                try {
                    db.updateCustomer(customer);
            } catch (SQLException ex) {
                    Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            thread.start();
        }
        return res;

    }
}
