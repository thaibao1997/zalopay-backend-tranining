/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.app.view;

import static baolt.hotel.app.utils.ServerHelper.getProjectDir;
import static baolt.hotel.app.view.ViewRenderer.VIEWS_FOLDER;
import hapax.Template;
import hapax.TemplateDataDictionary;
import hapax.TemplateDictionary;
import hapax.TemplateException;
import hapax.TemplateLoader;
import hapax.TemplateResourceLoader;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class RoomView {

    private static final String templateName = "room.xtm";
    private static final String jsFile = "/js/room.js";

    public static String getRoomView(JSONArray rooms,String user) throws TemplateException {
        String content = getContentSection(rooms);
        String footer = getFooterSection();
        return ViewRenderer.getMyPage("Rooms", content, footer,user);
    }

    public static String getFooterSection() {
        return ViewRenderer.getJSLink(jsFile);
    }

    public static String getContentSection(JSONArray objs) {
        try {
            TemplateLoader templateLoader = TemplateResourceLoader.create(VIEWS_FOLDER, true);
            Template template = templateLoader.getTemplate(templateName);
            TemplateDictionary templeDictionary = new TemplateDictionary();
            for (int i = 0; i < objs.length(); i++) {
                TemplateDataDictionary temp = templeDictionary.addSection("room");
                JSONObject jsonObject = objs.getJSONObject(i);
                temp.setVariable("roomId", jsonObject.getString("roomId"));
                temp.setVariable("roomName", jsonObject.getString("roomName"));
                int roomType = jsonObject.getInt("roomType");
                temp.setVariable("roomType", getRoomType(roomType));
                String dateStr = jsonObject.getString("createDate");
                temp.setVariable("createDate", dateStr);
                temp.setVariable("isActive", jsonObject.getBoolean("isActive") ? "Yes" : "No");
                temp.setVariable("description", jsonObject.getString("description"));
            }
            return template.renderToString(templeDictionary);
        } catch (TemplateException ex) {
            Logger.getLogger(RoomView.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static String getRoomType(int type) {
        switch (type) {
            case 1:
                return "Single";
            case 2:
                return "Double";
            case 3:
                return "Three";
            default:
                return "Tap The";
        }
    }

}
