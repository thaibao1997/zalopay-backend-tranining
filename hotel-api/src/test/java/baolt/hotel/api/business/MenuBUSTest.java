/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.business;

import baolt.hotel.api.database.DataSource;
import baolt.hotel.api.dto.Menu;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import testUtil.TestUtil;
import static testUtil.TestUtil.flushAllCaches;

/**
 *
 * @author baolt
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MenuBUSTest {

    private ArrayList<Menu> testMenus;
    private MenuBUS instance;

    public MenuBUSTest() {
        instance = new MenuBUS();
        testMenus = new ArrayList<>();

        Menu menu = new Menu();
        menu.setMenuId("M01");
        menu.setRoomId("R01");
        menu.setIsActive(true);
        menu.setAmount(10000);
        menu.setCreateDate(new Date());
        menu.setNote("Phong 01");
        testMenus.add(menu);

        menu = new Menu();
        menu.setMenuId("M02");
        menu.setRoomId("R02");
        menu.setIsActive(true);
        menu.setAmount(10000);
        menu.setCreateDate(new Date());
        testMenus.add(menu);

    }

    @BeforeClass
    public static void setUpClass() {
        TestUtil.flushAllCaches();

        DataSource.switchToTestDatabase();
        DataSource dataSource = DataSource.getInstance();
        dataSource.dropAllData();

        try (Connection connection = dataSource.getConnection()) {
            Statement st = connection.createStatement();
            String query;

            st = connection.createStatement();
            query = "Insert into Room values ('R01','Room 01',1,NOW(),true,'Phong 001')";
            st.execute(query);

            st = connection.createStatement();
            query = "Insert into Room values ('R02','Room 02',2,NOW(),false,'Phong 002')";
            st.execute(query);

            flushAllCaches();
        } catch (SQLException ex) {
            Logger.getLogger(RoomBUSTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @AfterClass
    public static void tearDownClass() {
        flushAllCaches();

        DataSource.swicthToMainDatabase();
    }

    /**
     * Test of addMenu method, of class MenuBUS.
     */
    @Test
    public void AA_testAddMenu() {
        System.out.println("addMenu");
        BUSResult expResult = new BUSResult(testMenus.get(0));
        BUSResult result = instance.addMenu(testMenus.get(0));

        BUSResult expResult1 = new BUSResult(testMenus.get(1));
        BUSResult result1 = instance.addMenu(testMenus.get(1));

        assertEquals(expResult, result);
        assertEquals(expResult1, result1);
    }

    @Test
    public void AB_testAddMenu_FAIL_ROOM_ID_NOT_FOUND() {
        System.out.println("addMenu");

        Menu menu = new Menu();
        menu.setMenuId("M03");
        menu.setRoomId("RZZZ");//
        menu.setIsActive(true);
        menu.setAmount(10000);
        menu.setCreateDate(new Date());
        menu.setNote("Phong 01");

        BUSResult expResult = new BUSResult(MenuBUS.ROOM_NOT_FOUND_MSG);
        BUSResult result = instance.addMenu(menu);

        assertEquals(expResult, result);
    }

    /**
     * Test of getAllMenus method, of class MenuBUS.
     */
    @Test
    public void B_testGetAllMenus() {
        System.out.println("getAllMenus");
        Collection<Menu> result = instance.getAllMenus();
        assertEquals(testMenus.size(), result.size());
    }

    /**
     * Test of getMenuByID method, of class MenuBUS.
     */
    @Test
    public void CA_testGetMenuByID() {
        System.out.println("getMenuByID");
        Menu menu = testMenus.get(0);
        BUSResult expResult = new BUSResult(menu);
        BUSResult result = instance.getMenuByID(menu.getMenuId());
        assertEquals(expResult, result);
    }

    @Test
    public void CB_testGetMenuByID_FAIL_ID_NOT_FOUND() {
        System.out.println("getMenuByID");
        String id = "ZZZZs";
        BUSResult expResult = BUSHelper.ID_NOT_FOUND_ERR;
        BUSResult result = instance.getMenuByID(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateMenu method, of class MenuBUS.
     */
    @Test
    public void DA_testUpdateMenu() {
        System.out.println("updateMenu");
        Menu menu = testMenus.get(0);
        menu.setAmount(20000);
        menu.setNote("ABC");

        BUSResult expResult = new BUSResult(menu);
        BUSResult result = instance.updateMenu(menu);
        assertEquals(expResult, result);
    }

    @Test
    public void DB_testUpdateMenu_FAIL_ROOMID_NOT_FOUND() {
        System.out.println("updateMenu");
        Menu menu = testMenus.get(0);
        menu.setAmount(20000);
        menu.setRoomId("ZZLZ");

        BUSResult expResult = new BUSResult(MenuBUS.ROOM_NOT_FOUND_MSG);
        BUSResult result = instance.updateMenu(menu);
        assertEquals(expResult, result);

        menu.setRoomId("R01");
    }

    @Test
    public void DC_testUpdateMenu_FAIL_ID_NOT_FOUND() {
        System.out.println("getMenuByID");

        Menu menu = new Menu();
        menu.setMenuId("M03");
        menu.setRoomId("R01");//
        menu.setIsActive(true);
        menu.setAmount(10000);
        menu.setCreateDate(new Date());
        menu.setNote("Phong 01");

        BUSResult expResult = BUSHelper.ID_NOT_FOUND_ERR;
        BUSResult result = instance.updateMenu(menu);
        assertEquals(expResult, result);
    }

    /**
     * Test of getActiveMenuOfRoom method, of class MenuBUS.
     */
    @Test
    public void E_testGetActiveMenuOfRoom() {
        System.out.println("getActiveMenuOfRoom");
        Menu menu = testMenus.get(1);
        BUSResult expResult = new BUSResult(menu);
        BUSResult result = instance.getActiveMenuOfRoom(menu.getRoomId());
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteMenu method, of class MenuBUS.
     */
    @Test
    public void F_testDeleteMenu() {
        System.out.println("deleteMenu");
        Menu menu = testMenus.get(1);
        BUSResult expResult = new BUSResult(menu);
        BUSResult result = instance.deleteMenu(menu.getMenuId());
        assertEquals(expResult, result);
    }

}
