/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.database;

import baolt.hotel.api.dataacess.RoomDAO;
import baolt.hotel.api.dto.Room;
import baolt.hotel.api.dto.RoomType;
import static baolt.hotel.api.util.APIHelper.convertDateToTimestamp;
import static baolt.hotel.api.util.APIHelper.convertTimestampToDate;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author baolt
 */
public class RoomDB {

    private static final String TABLE_NAME = "Room";
    private static final String ID_COL = "roomId";
    private static final String ROOM_NAME_COL = "roomName";
    private static final String ROOM_TYPE_COL = "roomType";
    private static final String ISACTIVE_COL = "isActive";
    private static final String DESCRIPTION_COL = "description";
    private static final String CREATE_DATE_COL = "createDate";

    private DataSource dataSource = null;
    
    public RoomDB() {
        dataSource = DataSource.getInstance();
    }

    private Room resultSetToRoom(ResultSet rs) throws SQLException {
        Room room = new Room();
        room.setRoomId(rs.getString(ID_COL));
        room.setRoomName(rs.getString(ROOM_NAME_COL));
        room.setRoomType(RoomType.fromInt(rs.getInt(ROOM_TYPE_COL)));
        room.setIsActive(rs.getBoolean(ISACTIVE_COL));
        String descr = rs.getString(DESCRIPTION_COL);
        room.setDescription(descr == null ? "" : descr);
        Timestamp ts = rs.getTimestamp(CREATE_DATE_COL);
        room.setCreateDate(convertTimestampToDate(ts));
        return room;
    }

    public ArrayList<Room> getAllRooms() throws SQLException {
        ArrayList<Room> rooms = new ArrayList<>();
        try (Connection conn = dataSource.getConnection()){
            String query = "Select * from " + TABLE_NAME;
            PreparedStatement st = conn.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Room room = resultSetToRoom(rs);
                rooms.add(room);
            }
        } catch (SQLException ex) {
            Logger.getLogger(RoomDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } 
        return rooms;
    }

    public Room getRoom(String roomID) throws SQLException {
        Room room = null;
        try (Connection conn = dataSource.getConnection()){
            String query = "Select * from " + TABLE_NAME + " Where roomID Like ?";
            PreparedStatement st = conn.prepareStatement(query);
            st.setString(1, roomID);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                room = resultSetToRoom(rs);
                return room;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(RoomDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } 
    }

    public boolean addRom(Room room) throws SQLException {
        try (Connection conn = dataSource.getConnection()){
            String query = "Insert into " + TABLE_NAME + " values (?,?,?,?,?,?)";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, room.getRoomId());
            pst.setString(2, room.getRoomName());
            pst.setInt(3, room.getRoomType().toInt());

            Date createDate = room.getCreateDate();
            pst.setTimestamp(4, convertDateToTimestamp(createDate));

            pst.setBoolean(5, room.isIsActive());
            pst.setString(6, room.getDescription());
            return pst.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(RoomDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }

    public boolean deleteRoom(String id) throws SQLException {
        try(Connection conn = dataSource.getConnection()) {
            String query = "delete from " + TABLE_NAME + " where roomID like ?";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, id);
            return pst.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(RoomDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } 
    }

    public boolean updateRoom(Room room) throws SQLException {
        try (Connection conn = dataSource.getConnection()){
            String query = "update " + TABLE_NAME + " set roomName = ?,roomType = ? "
                    + ",createDate = ?,isActive = ? ,description = ? "
                    + "where roomId like ?";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, room.getRoomName());
            pst.setInt(2, room.getRoomType().toInt());
            Date createDate = room.getCreateDate();
            pst.setTimestamp(3, convertDateToTimestamp(createDate));
            pst.setBoolean(4, room.isIsActive());
            pst.setString(5, room.getDescription());
            pst.setString(6, room.getRoomId());
            return pst.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(RoomDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }

}
