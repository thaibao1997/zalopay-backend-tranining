/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.business;

import baolt.hotel.api.dataacess.MenuDAO;
import baolt.hotel.api.dataacess.RoomDAO;
import baolt.hotel.api.dto.Menu;
import baolt.hotel.api.dto.Room;
import java.sql.SQLException;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author baolt
 */
public class MenuBUS {

    public static final String ROOM_NOT_FOUND_MSG = "Room not found";

    private MenuDAO menuDAO;
    private RoomDAO roomDAO;

    public MenuBUS() {
        menuDAO = new MenuDAO();
        roomDAO = new RoomDAO();

    }

    public Collection<Menu> getAllMenus() {
        try {
            return menuDAO.getAllMenus();
        } catch (SQLException ex) {
            return null;
        }
    }

    public BUSResult CheckBUS(Menu menu) throws SQLException {
        Room room = roomDAO.getRoom(menu.getRoomId());
        if (room == null) {
            return new BUSResult(ROOM_NOT_FOUND_MSG);
        }
        return new BUSResult(null, false, null);
    }

    public BUSResult getMenuByID(String id) {
        if (id.isEmpty()) {
            return BUSHelper.NULL_ID_ERR;
        }
        try {
            Menu menu = menuDAO.getMenu(id);
            if (menu != null) {
                return new BUSResult(menu);
            } else {
                return BUSHelper.ID_NOT_FOUND_ERR;
            }
        } catch (SQLException ex) {
            Logger.getLogger(MenuBUS.class.getName()).log(Level.SEVERE, null, ex);
            return BUSHelper.getResultFromException(ex, ROOM_NOT_FOUND_MSG);
        }
    }

    public BUSResult addMenu(Menu menu) {
        try {
            BUSResult check = CheckBUS(menu);
            if (check.err) {
                return check;
            }

            Menu menu_ = menuDAO.getMenu(menu.getMenuId());
            if (menu_ != null) {
                return BUSHelper.DUPLICATE_ID_ERR;
            }

            if (menu.isIsActive()) {
                menuDAO.deactiveAllOfRoom(menu.getRoomId());
            }
            if (menuDAO.addMenu(menu)) {
                return new BUSResult(menu);
            }
        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, ROOM_NOT_FOUND_MSG);
        }
        return BUSHelper.DB_ERR;
    }

    public BUSResult deleteMenu(String id) {

        try {
            Menu menu = menuDAO.getMenu(id);
            if (menu == null) {
                return BUSHelper.ID_NOT_FOUND_ERR;
            }
            if (menuDAO.deleteMenu(id)) {
                return new BUSResult(menu);
            }
        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, ROOM_NOT_FOUND_MSG);
        }
        return BUSHelper.DB_ERR;

    }

    public BUSResult updateMenu(Menu menu) {
        try {
            BUSResult check = CheckBUS(menu);
            if (check.err) {
                return check;
            }
            
            Menu menu_ = menuDAO.getMenu(menu.getMenuId());
            if (menu_ == null) {
                return BUSHelper.ID_NOT_FOUND_ERR;
            }

            menu.setCreateDate(menu_.getCreateDate());
            if (menuDAO.updateMenu(menu)) {
                return new BUSResult(menu);
            }
        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, ROOM_NOT_FOUND_MSG);
        }
        return BUSHelper.DB_ERR;
    }

    public BUSResult getActiveMenuOfRoom(String roomId) {
        try {
            Menu menu = menuDAO.getActiveMenuOfRoom(roomId);
            if (menu == null) {
                return new BUSResult("Cannot find menu");
            } else {
                return new BUSResult(menu);
            }
        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, ROOM_NOT_FOUND_MSG);
        }
    }

}
