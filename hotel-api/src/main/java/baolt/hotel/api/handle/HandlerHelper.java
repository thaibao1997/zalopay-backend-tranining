/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.handle;

import io.grpc.Status;

/**
 *
 * @author baolt
 */
public class HandlerHelper {
    public static RuntimeException getRuntimeExceptionWithMessage(String msg){
        return Status.ABORTED.withDescription(msg).asRuntimeException();
    }
}
