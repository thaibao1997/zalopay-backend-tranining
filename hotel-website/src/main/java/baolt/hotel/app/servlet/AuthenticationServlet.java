/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.app.servlet;

import baolt.hotel.app.model.AuthenticationModel;
import baolt.hotel.app.utils.ServerHelper;
import baolt.hotel.app.view.AuthenticationView;
import java.io.IOException;
import java.util.Enumeration;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class AuthenticationServlet extends RestHttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String request = ServerHelper.getRequestStr(req);

        JSONObject jsonObj = new JSONObject(request);
        String username = jsonObj.getString("username");
        String code = jsonObj.getString("code");
        boolean isAuthen = AuthenticationModel.checkAuthentication(username, code);
        if (isAuthen) {
            AuthenticationModel.setLogin(req, username);
            resp.getWriter().print("{\"authen\" : true}");
        } else {
            resp.getWriter().print("{\"authen\" : false}");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            resp.getWriter().print(AuthenticationView.getLoginView());
        } catch (Exception e) {
            resp.sendError(500);
        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            AuthenticationModel.logout(req);
            resp.getWriter().println( "{\"authen\" : false}");
        } catch (Exception e) {
            resp.sendError(500);
        }
    }

}
