/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.app.servlet;

import baolt.hotel.app.model.BookingModel;
import baolt.hotel.app.model.CustomerModel;
import baolt.hotel.app.model.MenuModel;
import baolt.hotel.app.model.RoomModel;
import static baolt.hotel.app.utils.ServerHelper.getRequestStr;
import static baolt.hotel.app.utils.ServerHelper.getUserName;
import baolt.hotel.app.view.BookingView;
import baolt.hotel.app.view.MenuView;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import hapax.TemplateException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.glxn.qrgen.core.image.ImageType;
import net.glxn.qrgen.javase.QRCode;
import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class BookingServlet extends RestHttpServlet {

    BookingModel bookingModel = new BookingModel();
    RoomModel roomModel = new RoomModel();
    CustomerModel customerModel = new CustomerModel();

    @Override
    protected void doPatch(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String id = req.getParameter("id");
            if (id == null || id.isEmpty()) {
                resp.sendError(400);
                return;
            }
            String reqBody = getRequestStr(req);
            JSONObject jsonObject = new JSONObject(reqBody);
            jsonObject.put("bookingId", id);
            System.out.println("Client update booking: " + jsonObject.toString());
            String res = bookingModel.updateBooking(jsonObject).toString();
            resp.getWriter().print(res);
        } catch (Exception e) {
            resp.sendError(500);

        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        if (id == null || id.isEmpty()) {
            resp.sendError(400);
            return;
        }
        try {
            String res = bookingModel.deleteBooking(id).toString();
            resp.getWriter().print(res);
        } catch (Exception e) {
            resp.sendError(500);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //CHECKOUT BOOKING
        String url = req.getRequestURL().toString();
        boolean isCheckout = url.endsWith("checkout");
        if (isCheckout) {
            String id = req.getParameter("id");
            if (id == null) {
                resp.sendError(HttpStatus.BAD_REQUEST_400);
            } else {
                JSONObject result = bookingModel.checkoutBooking(id);
                System.out.println(result);
                if (result.has("error")) {
                    resp.getWriter().print(result.toString());
                } else {
                    String chargeId = result.getString("apptransid");
                    int expireTime = result.getInt("expiretime");
                    String qrStr = result.getString("qrStr");
                    ByteArrayOutputStream stream = QRCode.from(qrStr)
                            .to(ImageType.PNG).withSize(300, 300).stream();
                    String qrBase64 = Base64.encode(stream.toByteArray());
                    resp.getWriter().print(createCheckoutResponseJSON(chargeId,qrBase64,expireTime));
                }
            }
            return;
        }
        //Create new booking
        String reqBody = getRequestStr(req);
        System.out.println("Client add new booking: " + reqBody);
        JSONObject jsonObject = new JSONObject(reqBody);
        try {
            String result = bookingModel.addBooking(jsonObject).toString();
            System.out.println("Result: " + result);
            resp.getWriter().print(result);
        } catch (Exception e) {
            e.printStackTrace();
            resp.sendError(500);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            JSONArray bookings = bookingModel.getAllBooking();
            JSONArray rooms = roomModel.getAllRoom();
            JSONArray customers = customerModel.getAllCustomer();

            resp.getWriter().print(BookingView.getBookingView(bookings, customers, rooms, getUserName(req)));
        } catch (TemplateException ex) {
            resp.sendError(500);
            Logger.getLogger(RoomServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
            e.printStackTrace();
            resp.sendError(500);
        }
    }
    
    public static String createCheckoutResponseJSON(String chargeId,String qrBase64,int expireTime){
        return "{\"chargeId\":\"" + chargeId 
                            + "\",\"QRCode\":\"" + qrBase64 + "\","
                            + "\"expireTime\":"+expireTime+"}";
    
    }

}
