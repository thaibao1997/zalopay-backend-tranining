/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.util;

import baolt.hotel.api.business.BUSResult;
import java.io.BufferedReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONObject;
import baolt.hotel.api.dto.JSONable;
import baolt.hotel.api.dto.Room;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.json.JSONArray;

/**
 *
 * @author baolt
 */
public class APIHelper {

    public static final String JSON_CONTENT_TYPE = "application/json";

    public static final String DUPLICATE_KEY_JSON = "{\"error\": true,\"errorMsg\" : \"Primary Key already exist\"}";
    public static final String DB_ERROR_JSON = "{\"error\": true,\"errorMsg\" : \"Database error\"}";
    public static final String KEY_NOT_FOUND_JSON = "{\"error\": true,\"errorMsg\" : \"Primary key not found\"}";
    public static final String DELETE_OKE_JSON = "{\"error\": false,\"message\" : \"Delete Successed\"}";

    private static final SimpleDateFormat dateformatter = new SimpleDateFormat("dd/MM/yyyy");
    private static final SimpleDateFormat datetimeformatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

    public static void sendJsonObject(HttpServletResponse resp, JSONable obj) throws IOException {
        JSONObject jsonObject = obj.toJsonObject();
        resp.setStatus(HttpStatus.OK_200);
        resp.setContentType(JSON_CONTENT_TYPE);
        resp.getWriter().print(jsonObject.toString());
    }

    public static boolean sendBUSResult(HttpServletResponse resp, BUSResult rs) throws IOException {
        if (rs.err && rs.errMsg != null) {
            String errMsg = "{\"error\": true,\"errorMsg\" : \"" + rs.errMsg + "\"}";
            sendJsonString(resp, errMsg);
            return true;
        } else if (rs.returnObj != null && rs.returnObj instanceof JSONable) {
            sendJsonObject(resp, (JSONable) rs.returnObj);
            return true;
        }
        return false;

    }

    public static void sendJsonString(HttpServletResponse resp, String json) throws IOException {
        resp.setStatus(HttpStatus.OK_200);
        resp.setContentType(JSON_CONTENT_TYPE);
        resp.getWriter().print(json);
    }

    public static String getRequestStr(HttpServletRequest req) {
        StringBuffer jb = new StringBuffer();
        String line = null;
        try {
            BufferedReader reader = req.getReader();
            while ((line = reader.readLine()) != null) {
                jb.append(line);
            }
        } catch (Exception e) {
            /*report an error*/
        }
        return jb.toString();
    }

    public static String getDateStr(Date date) {
        return date != null ? datetimeformatter.format(date) : datetimeformatter.format(new Date());
    }

    public static String getShortDateStr(Date date) {
        return date != null ? dateformatter.format(date) : dateformatter.format(new Date());
    }

    public static Date getDateFromStr(String str) {
        try {
            return datetimeformatter.parse(str);
        } catch (ParseException ex) {
            try {
                return dateformatter.parse(str);
            } catch (ParseException ex_) {
                return new Date();
            }
        }
    }

    public static Date convertSqlDateToUtilDate(java.sql.Date sDate) {
        return new Date(sDate.getTime());
    }

    public static java.sql.Date convertUtilDateToSqlDate(Date uDate) {
        return uDate != null ? new java.sql.Date(uDate.getTime())
                : new java.sql.Date((new Date()).getTime());
    }

    public static Timestamp convertDateToTimestamp(Date uDate) {
        return new Timestamp(uDate.getTime());
    }

    public static Date convertTimestampToDate(Timestamp ts) {
        return new Date(ts.getTime());
    }

    public static long getDateDistance(Date date1, Date date2) {
        LocalDate ld1 = new java.sql.Date(date1.getTime()).toLocalDate();
        LocalDate ld2 = new java.sql.Date(date2.getTime()).toLocalDate();
        return Math.abs(ld1.toEpochDay() - ld2.toEpochDay());
    }

}
