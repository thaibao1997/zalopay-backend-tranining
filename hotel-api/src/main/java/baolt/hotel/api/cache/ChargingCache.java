/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.cache;

import static baolt.hotel.api.cache.CacheConnect.MAX_TRY_LOCK_TIME;
import static baolt.hotel.api.cache.CacheConnect.UNLOCK_AFTER_TIME;
import baolt.hotel.api.dto.Charging;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.redisson.api.RLocalCachedMap;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RedissonClient;

/**
 *
 * @author baolt
 */
public class ChargingCache {

    public static final String KEY = "HOTEL_API_CHARGINGS";
    public static final String LOCK_NAME = "CHARGING_LOCK";

    private final RLocalCachedMap<String, Charging> cachedChargings;
    private final RReadWriteLock rwlock;

    private static ChargingCache instance = new ChargingCache();

    public static ChargingCache getInstance() {
        return instance;
    }

    private ChargingCache() {
        RedissonClient client = CacheConnect.getRedissonClient();
        cachedChargings = client.getLocalCachedMap(KEY, CacheConnect.getLocalCachedMapOption());
        rwlock = client.getReadWriteLock(LOCK_NAME);
        flushCaches();
    }

    public void flushCaches() {
        if (cachedChargings.isExists()) {
            cachedChargings.clear();
            cachedChargings.expire(0, TimeUnit.SECONDS);
        }
    }

    private void unlockReadLock() {
        if (rwlock.readLock().isHeldByCurrentThread()) {
            rwlock.readLock().unlock();
        }
    }

    private void unlockWriteLock() {
        if (rwlock.writeLock().isHeldByCurrentThread()) {
            rwlock.writeLock().unlock();
        }
    }

    public boolean isEmpty() {
        return cachedChargings.isEmpty();
    }

    public boolean isExist() {
        return cachedChargings.isExists();
    }

    public boolean addCharging(Charging charging) {
        if (charging == null) {
            return false;
        }
        try {
            if (rwlock.writeLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                return cachedChargings.fastPut(charging.getChargeID(), charging);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(ChargingCache.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            unlockWriteLock();
        }
        return false;
    }

    public boolean updateCharging(Charging charging) {
        if (charging == null) {
            return false;
        }
        try {
            if (rwlock.writeLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                return cachedChargings.fastReplace(charging.getChargeID(), charging);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(ChargingCache.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            unlockWriteLock();
        }
        return false;
    }

    public Charging findChargingByID(String id) {
        try {
            if (rwlock.readLock().tryLock(5, 2, TimeUnit.SECONDS)) {
                return cachedChargings.get(id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            unlockReadLock();
        }
        return null;
    }

    public boolean deleteCharging(String id) {
        try {
            if (rwlock.writeLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                Charging charging = cachedChargings.get(id);
                return cachedChargings.fastRemove(id) > 0;
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(ChargingCache.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            unlockWriteLock();
        }
        return false;
    }

    public Collection<Charging> toCollection() {
        try {
            if (rwlock.readLock().tryLock(5, 2, TimeUnit.SECONDS)) {
                return cachedChargings.values();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            unlockReadLock();
        }
        return null;
    }

    public void fromList(ArrayList<Charging> chargings) {
        for (Charging charging : chargings) {
            addCharging(charging);
        }
    }

    
}
