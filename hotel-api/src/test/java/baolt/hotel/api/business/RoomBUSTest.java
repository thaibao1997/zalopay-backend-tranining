/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.business;

import baolt.hotel.api.cache.RoomCache;
import baolt.hotel.api.database.DataSource;
import baolt.hotel.api.dto.Room;
import baolt.hotel.api.dto.RoomType;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import testUtil.TestUtil;
import static testUtil.TestUtil.flushAllCaches;

/**
 *
 * @author baolt
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RoomBUSTest {

    private ArrayList<Room> testRooms;
    private RoomBUS instance;

    public RoomBUSTest() {

        instance = new RoomBUS();
        testRooms = new ArrayList<>();

        Room room = new Room();
        room.setRoomId("R01");
        room.setRoomName("Room 01");
        room.setRoomType(RoomType.SINGLE);
        room.setIsActive(true);
        testRooms.add(room);

        room = new Room();
        room.setRoomId("R02");
        room.setRoomName("Room 02");
        room.setRoomType(RoomType.DOUBLE);
        room.setIsActive(false);
        room.setDescription("Phong 002 Tang 1");
        testRooms.add(room);

    }

    @BeforeClass
    public static void setUp() {
        TestUtil.flushAllCaches();

        DataSource.switchToTestDatabase();
        DataSource dataSource = DataSource.getInstance();
        dataSource.dropAllData();

    }

    @AfterClass
    public static void tearDown() {
        flushAllCaches();

        DataSource.swicthToMainDatabase();
    }

    /**
     * Test of addRoom method, of class RoomBUS.
     */
    @Test
    public void AA_testAddRoom() {
        System.out.println("addRoom");

        BUSResult expResult1 = new BUSResult(testRooms.get(0));
        BUSResult expResult2 = new BUSResult(testRooms.get(1));

        BUSResult result1 = instance.addRoom(testRooms.get(0));
        BUSResult result2 = instance.addRoom(testRooms.get(1));

        assertFalse(result1.err);
        assertFalse(result2.err);

        assertEquals(testRooms.get(0), result1.returnObj);
        assertEquals(testRooms.get(1), result2.returnObj);

    }

    @Test
    public void AB_testAddRoom_Fail_Duplicate_ID() {
        BUSResult expResult = BUSHelper.DUPLICATE_ID_ERR;
        BUSResult result = instance.addRoom(testRooms.get(0));

        assertTrue(result.err);
        assertEquals(expResult.errMsg, result.errMsg);

    }

    /**
     * Test of getAllRooms method, of class RoomBUS.
     */
    @Test
    public void B_testGetAllRooms() {
        System.out.println("getAllRooms");
        Collection<Room> result = instance.getAllRooms();
        assertEquals(testRooms.size(), result.size());
//        assertEquals(testRooms.get(0), result.get(0));
//        assertEquals(testRooms.get(1), result.get(1));

    }

    /**
     * Test of getRoomByID method, of class RoomBUS.
     */
    @Test
    public void CA_testGetRoomByID() {
        System.out.println("getRoomByID");
        BUSResult expResult = new BUSResult(testRooms.get(0));
        BUSResult result = instance.getRoomByID(testRooms.get(0).getRoomId());
        assertFalse(result.err);
        assertEquals(expResult.returnObj, result.returnObj);

        expResult = new BUSResult(testRooms.get(1));
        result = instance.getRoomByID(testRooms.get(1).getRoomId());
        assertFalse(result.err);
        assertEquals(expResult.returnObj, result.returnObj);

    }

    @Test
    public void CB_testGetRoomByID_Fail_ID_NOT_FOUND() {
        System.out.println("getRoomByID");
        String id = "R00zz";
        BUSResult expResult = BUSHelper.ID_NOT_FOUND_ERR;
        BUSResult result = instance.getRoomByID(id);
        assertTrue(result.err);
        assertEquals(expResult.returnObj, result.returnObj);

    }

    /**
     * Test of updateRoom method, of class RoomBUS.
     */
    @Test
    public void DA_testUpdateRoom() {
        System.out.println("updateRoom");
        Room room = testRooms.get(0);

        room.setRoomName("Phong OO1");
        room.setDescription("Phong OO1");
        room.setIsActive(false);
        room.setRoomType(RoomType.TAPTHE);

        BUSResult expResult = new BUSResult(room);
        BUSResult result = instance.updateRoom(room);

        assertFalse(result.err);
        assertEquals(expResult.returnObj, result.returnObj);
    }

    @Test
    public void DB_testUpdateRoom_ID_NOT_FOUND() {
        System.out.println("updateRoom - Fail");
        Room room = testRooms.get(0);

        room.setRoomName("Phong OO1");
        room.setDescription("Phong OO1");
        room.setIsActive(false);
        room.setRoomType(RoomType.TAPTHE);

        room.setRoomId("IDZZZZ");

        BUSResult expResult = BUSHelper.ID_NOT_FOUND_ERR;
        BUSResult result = instance.updateRoom(room);

        assertEquals(expResult, result);
    }

    /**
     * Test of deleteRoom method, of class RoomBUS.
     */
    @Test

    public void EA_testDeleteRoom() {
        System.out.println("deleteRoom");
        Room room = testRooms.get(1);
        String id = room.getRoomId();
        BUSResult expResult = new BUSResult(room);
        BUSResult result = instance.deleteRoom(id);

        assertFalse(result.err);
        assertEquals(expResult.returnObj, result.returnObj);
    }

    @Test
    public void EB_EA_testDeleteRoom_Fail_ID_NOT_FOUND() {
        System.out.println("getRoomByID");
        String id = "R00zz";
        BUSResult expResult = BUSHelper.ID_NOT_FOUND_ERR;
        BUSResult result = instance.deleteRoom(id);
        assertTrue(result.err);
        assertEquals(expResult.returnObj, result.returnObj);

    }

}
