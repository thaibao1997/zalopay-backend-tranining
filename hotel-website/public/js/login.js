$(document).ready(function () {

    $('#login_form').on('submit', function (e) {
        e.preventDefault();
        var paramObj = {};
        $.each($('#login_form').serializeArray(), function (_, kv) {
            paramObj[kv.name] = kv.value;
        });

        $.ajax({
            url : '../login',
            method : 'POST',
            data : JSON.stringify(paramObj),
            success: function (result) {
                console.log(result);
                var obj = JSON.parse(result);
                if(obj.authen){
                    window.location.href ="/room";
                }else{
                    $("#login_notify").html("Invalid User Or Passcode");
                }
            }
        })

    })
});