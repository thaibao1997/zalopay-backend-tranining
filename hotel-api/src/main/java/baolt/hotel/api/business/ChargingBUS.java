/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.business;

import baolt.hotel.api.dataacess.BookingDAO;
import baolt.hotel.api.dataacess.ChargingDAO;
import baolt.hotel.api.dto.Booking;
import baolt.hotel.api.dto.Charging;
import java.sql.SQLException;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author baolt
 */
public class ChargingBUS {

    public static final String BOOKING_NOT_FOUND_MSG = "Booking not found";

    private ChargingDAO chargingDAO;
    private BookingBUS bookingBUS;
    private CheckoutBUS checkoutBUS;

    public ChargingBUS() {
        chargingDAO = new ChargingDAO();
        bookingBUS = new BookingBUS();
        checkoutBUS = new CheckoutBUS();
    }

    public Collection<Charging> getAllChargings() {
        try {
            return chargingDAO.getAllChargings();
        } catch (SQLException ex) {
            return null;
        }
    }

    public BUSResult CheckBUS(Charging chargigng) throws SQLException {
        return bookingBUS.getBookingByID(chargigng.getBookingID());
    }

    public BUSResult getChargingByID(String id) {
        if (id.isEmpty()) {
            return BUSHelper.NULL_ID_ERR;
        }
        try {
            Charging charging = chargingDAO.getCharging(id);
            if (charging != null) {
                return new BUSResult(charging);
            } else {
                return BUSHelper.ID_NOT_FOUND_ERR;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ChargingBUS.class.getName()).log(Level.SEVERE, null, ex);
            return BUSHelper.getResultFromException(ex, BOOKING_NOT_FOUND_MSG);
        }
    }

    public BUSResult addCharging(Charging charging) {
        try {
            BUSResult check = CheckBUS(charging);
            if (check.err) {
                return check;
            }

            Charging charging_ = chargingDAO.getCharging(charging.getChargeID());
            if (charging_ != null) {
                return BUSHelper.DUPLICATE_ID_ERR;
            }
            if (chargingDAO.addCharging(charging)) {
                return new BUSResult(charging);
            }
        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, BOOKING_NOT_FOUND_MSG);
        }
        return BUSHelper.DB_ERR;
    }

    public BUSResult deleteCharging(String id) {

        try {
            Charging charging = chargingDAO.getCharging(id);
            if (charging == null) {
                return BUSHelper.ID_NOT_FOUND_ERR;
            }
            if (chargingDAO.deleteCharging(id)) {
                return new BUSResult(charging);
            }
        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, BOOKING_NOT_FOUND_MSG);
        }
        return BUSHelper.DB_ERR;

    }

    public BUSResult updateCharging(Charging charging) {
        try {
            BUSResult check = CheckBUS(charging);
            if (check.err) {
                return check;
            }

            Charging charging_ = chargingDAO.getCharging(charging.getChargeID());
            if (charging_ == null) {
                return BUSHelper.ID_NOT_FOUND_ERR;
            }

            charging.setChargeDate(charging_.getChargeDate());
            if (chargingDAO.updateCharging(charging)) {
                return new BUSResult(charging);
            }
        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, BOOKING_NOT_FOUND_MSG);
        }
        return BUSHelper.DB_ERR;
    }

    public BUSResult getAssociatedCheckout(String chargeId) {
        try {
            Charging charging = chargingDAO.getCharging(chargeId);
            if (charging == null) {
                return BUSHelper.ID_NOT_FOUND_ERR;
            }
            String bookingId = charging.getBookingID();
            return bookingBUS.getAssociatedCheckout(bookingId);
        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, BOOKING_NOT_FOUND_MSG);
        }
    }
}
