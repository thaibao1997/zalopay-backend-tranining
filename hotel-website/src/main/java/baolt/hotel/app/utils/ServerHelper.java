/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.app.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class ServerHelper {

    private static final SimpleDateFormat dateformatter
            = new SimpleDateFormat("dd/MM/yyyy");
    private static final SimpleDateFormat datetimeformatter
            = new SimpleDateFormat("dd/MM/yyyy HH:mm");
    private static final SimpleDateFormat defaultformatter
            = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");

    public static String getDateStr(Date date) {
        return date != null ? datetimeformatter.format(date) : datetimeformatter.format(new Date());
    }

    public static Date getDateFromStr(String str) {
        try {
            return datetimeformatter.parse(str);
        } catch (ParseException ex) {
            try {
                return dateformatter.parse(str);
            } catch (ParseException ex_) {
                return new Date();
            }
        }
    }

    public static String getRequestStr(HttpServletRequest req) {
        StringBuffer jb = new StringBuffer();
        String line = null;
        try {
            BufferedReader reader = req.getReader();
            while ((line = reader.readLine()) != null) {
                jb.append(line);
            }
        } catch (Exception e) {
            /*report an error*/
        }
        return jb.toString();
    }

    public static Date getDateFromDefaultStr(String str) {
        try {
            return defaultformatter.parse(str);
        } catch (ParseException ex) {
            return null;
        }
    }

    public static String getProjectDir() {
        File cur = new File("");
        return cur.getAbsolutePath();
    }



    public static String getUserName(HttpServletRequest req) {
        HttpSession session = req.getSession();
        return (String)session.getAttribute("user");
    }

}
