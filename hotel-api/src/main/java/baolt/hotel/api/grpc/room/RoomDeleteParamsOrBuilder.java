// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Room.proto

package baolt.hotel.api.grpc.room;

public interface RoomDeleteParamsOrBuilder extends
    // @@protoc_insertion_point(interface_extends:RoomDeleteParams)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>string id = 1;</code>
   */
  java.lang.String getId();
  /**
   * <code>string id = 1;</code>
   */
  com.google.protobuf.ByteString
      getIdBytes();
}
