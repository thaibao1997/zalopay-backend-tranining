/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.zookeeper;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooDefs;
import org.apache.zookeeper.ZooKeeper;

/**
 *
 * @author baolt
 */
public class ZookeeperClient {
    private static int PORT = 2181;
    private static String HOST = "localhost";
    
    private static ZookeeperClient instance = new ZookeeperClient();
    
    public static ZookeeperClient getInstance(){
        return instance;
    }
    
    private ZooKeeper client;
    
    private ZookeeperClient(){
        try {            
            client = new ZooKeeper(HOST, PORT,null);
        } catch (Exception ex) {
            Logger.getLogger(ZookeeperClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    synchronized public String createNode(String name){//return null => cannot create =>Lock
        try {
            return client.create("/"+name, null, ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL);
        } catch (Exception ex) {
            Logger.getLogger(ZookeeperClient.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return null;
    }
    
    synchronized public boolean deleteNode(String name){
        try {
            client.delete("/"+name, -1);
            return true;
        } catch (Exception ex) {
            Logger.getLogger(ZookeeperClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    } 
    
}
