/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.app.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author baolt
 */
public class LogFilter implements Filter {

    @Override
    public void init(FilterConfig fc) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest sr, ServletResponse sr1, FilterChain fc) throws IOException, ServletException {
        String url = ((HttpServletRequest) sr).getRequestURL().toString();
        System.out.println("Request: " + url);
//        if (sr1 != null) {
//            HttpServletResponse rsp = (HttpServletResponse) sr1;
//                    System.out.println("Respose: " + rsp.getCharacterEncoding());
//
//        }

        fc.doFilter(sr, sr1);

    }

    @Override
    public void destroy() {
    }

}
