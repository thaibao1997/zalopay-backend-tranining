/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.app.servlet;

import baolt.hotel.app.model.CheckoutModel;
import baolt.hotel.app.model.RoomModel;
import static baolt.hotel.app.utils.ServerHelper.getRequestStr;
import static baolt.hotel.app.utils.ServerHelper.getUserName;
import baolt.hotel.app.view.CheckoutView;
import baolt.hotel.app.view.RoomView;
import hapax.TemplateException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class CheckoutServlet extends RestHttpServlet {
    
    CheckoutModel checkoutModel = new CheckoutModel();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            String jsonStr = checkoutModel.getAllCheckout().toString();
            JSONArray jsonArray = new JSONArray(jsonStr);
            resp.getWriter().print(CheckoutView.getMenuView(jsonArray,getUserName(req)));
        } catch (TemplateException ex) {
            resp.sendError(500);
            Logger.getLogger(RoomServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
            resp.sendError(500);
        }

    }
    
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String reqBody = getRequestStr(req);
        System.out.println("Client add new checkout: " + reqBody);

        JSONObject jsonObject = new JSONObject(reqBody);
        try {
            String result = checkoutModel.addCheckout(jsonObject).toString();
            resp.getWriter().print(result);
        } catch (Exception e) {
            resp.sendError(500);
        }

    }

}
