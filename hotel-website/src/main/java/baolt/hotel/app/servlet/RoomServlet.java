/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.app.servlet;

import baolt.hotel.app.model.RoomModel;
import static baolt.hotel.app.utils.ServerHelper.getRequestStr;
import static baolt.hotel.app.utils.ServerHelper.getUserName;
import baolt.hotel.app.view.RoomView;
import hapax.TemplateException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class RoomServlet extends RestHttpServlet {
    
    private RoomModel roomModel = new RoomModel();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            long t1 = System.nanoTime();
            JSONArray jsonArray = roomModel.getAllRoom();
                        long t2 = System.nanoTime();
            resp.getWriter().print(RoomView.getRoomView(jsonArray,getUserName(req)));
        } catch (TemplateException ex) {
                        Logger.getLogger(RoomServlet.class.getName()).log(Level.SEVERE, null, ex);
            resp.sendError(500);
        } catch (Exception e) {
            resp.sendError(500);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String reqBody = getRequestStr(req);
        System.out.println("Client add new room: " + reqBody);

        JSONObject jsonObject = new JSONObject(reqBody);
       //jsonObject.put("createDate", getDateStr(new Date()));
        try {
            String result = roomModel.addRoom(jsonObject).toString();
            resp.getWriter().print(result);
        } catch (Exception e) {
            resp.sendError(500);
        }

    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        if (id == null || id.isEmpty()) {
            resp.sendError(400);
            return;
        }
        try {
            String res = roomModel.deleteRoom(id).toString();
            resp.getWriter().print(res);
        } catch (Exception e) {
            resp.sendError(500);
        }
    }

    @Override
    protected void doPatch(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String id = req.getParameter("id");
            if (id == null || id.isEmpty()) {
                resp.sendError(400);
                return;
            }
            String reqBody = getRequestStr(req);
            JSONObject jsonObject = new JSONObject(reqBody);
            jsonObject.put("roomId", id);
            System.out.println("Client update room: "+ jsonObject.toString());
            String res = roomModel.updateRoom(jsonObject).toString();
            resp.getWriter().print(res);
        } catch (Exception e) {
            resp.sendError(500);

        }
    }

}
