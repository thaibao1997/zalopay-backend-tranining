/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.cache;

import static baolt.hotel.api.cache.CacheConnect.MAX_TRY_LOCK_TIME;
import static baolt.hotel.api.cache.CacheConnect.UNLOCK_AFTER_TIME;
import baolt.hotel.api.dto.Customer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.redisson.api.LocalCachedMapOptions;
import org.redisson.api.LocalCachedMapOptions.SyncStrategy;
import org.redisson.api.RLocalCachedMap;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RedissonClient;

/**
 *
 * @author baolt
 */
public class CustomerCache {

    public static final String KEY = "HOTEL_API_CUSTOMERS";
    public static final String LOCK_NAME = "CUSTOMER_LOCK";

    private final RLocalCachedMap<String, Customer> cachedCustomers;
    private final RReadWriteLock rwlock;

    private static CustomerCache instance = new CustomerCache();

    public static CustomerCache getInstance() {
        return instance;
    }

    private CustomerCache() {
        RedissonClient client = CacheConnect.getRedissonClient();
        cachedCustomers = client.getLocalCachedMap(KEY, CacheConnect.getLocalCachedMapOption());
        rwlock = client.getReadWriteLock(LOCK_NAME);
        flushCaches();
    }

    public void flushCaches() {
        if (cachedCustomers.isExists()) {
            cachedCustomers.clear();
            cachedCustomers.expire(0, TimeUnit.SECONDS);
        }
    }

    private void unlockReadLock() {
        if (rwlock.readLock().isHeldByCurrentThread()) {
            rwlock.readLock().unlock();
        }
    }

    private void unlockWriteLock() {
        if (rwlock.writeLock().isHeldByCurrentThread()) {
            rwlock.writeLock().unlock();
        }
    }

    public boolean isEmpty() {
        return cachedCustomers.isEmpty();
    }

    public boolean isExist() {
        return cachedCustomers.isExists();
    }

    public boolean addCustomer(Customer customer) {
        if (customer == null) {
            return false;
        }
        try {
            if (rwlock.writeLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                return cachedCustomers.fastPut(customer.getCustomerId(), customer);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(CustomerCache.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            unlockWriteLock();
        }
        return false;
    }

    public boolean updateCustomer(Customer customer) {
        if (customer == null) {
            return false;
        }
        try {
            if (rwlock.writeLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                return cachedCustomers.fastReplace(customer.getCustomerId(), customer);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(CustomerCache.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            unlockWriteLock();
        }
        return false;
    }

    public Customer findCustomerByID(String id) {
        try {
            if (rwlock.readLock().tryLock(5, 2, TimeUnit.SECONDS)) {
                return cachedCustomers.get(id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            unlockReadLock();
        }
        return null;
    }

    public boolean deleteCustomer(String id) {
        try {
            if (rwlock.writeLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                Customer customer = cachedCustomers.get(id);
                return cachedCustomers.fastRemove(id) > 0;
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(CustomerCache.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            unlockWriteLock();
        }
        return false;
    }

    public Collection<Customer> toCollection() {
        try {
            if (rwlock.readLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                return cachedCustomers.values();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            unlockReadLock();
        }
        return null;
    }

    public void fromList(ArrayList<Customer> customers) {
        if (customers == null) {
            return;
        }
        for (Customer customer : customers) {
            addCustomer(customer);
        }
    }
}
