/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.business;

import baolt.hotel.api.dataacess.CustomerDAO;
import baolt.hotel.api.dto.Customer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author baolt
 */
public class CustomerBUS {

    private CustomerDAO customerDAO;

    public CustomerBUS() {
        customerDAO = new CustomerDAO();
    }

    public Collection<Customer> getAllCustomers() {
        try {
            return customerDAO.getAllCustomers();
        } catch (SQLException ex) {
            return null;
        }
    }

    public BUSResult getCustomerByID(String id) {
        if (id.isEmpty()) {
            return BUSHelper.NULL_ID_ERR;
        }
        try {
            Customer customer = customerDAO.getCustomer(id);
            if (customer != null) {
                return new BUSResult(customer);
            } else {
                return BUSHelper.ID_NOT_FOUND_ERR;
            }
        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, "");
        }
    }

    public BUSResult addCustomer(Customer customer) {

        try {
            Customer customer_ = customerDAO.getCustomer(customer.getCustomerId());
            if (customer_ != null) {
                return BUSHelper.DUPLICATE_ID_ERR;
            }
            if (customerDAO.addCustomer(customer)) {
                return new BUSResult(customer);
            }
        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, "");
        }
        return BUSHelper.DB_ERR;
    }

    public BUSResult deleteCustomer(String id) {

        try {
            Customer cus = customerDAO.getCustomer(id);
            if (cus == null) {
                return BUSHelper.ID_NOT_FOUND_ERR;
            }
            if (customerDAO.deleteCustomer(id)) {
                return new BUSResult(cus);
            }
        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, "");
        }
        return BUSHelper.DB_ERR;
    }

    public BUSResult updateCustomer(Customer customer) {

        try {
            Customer customer_ = customerDAO.getCustomer(customer.getCustomerId());
            if (customer_ == null) {
                return BUSHelper.ID_NOT_FOUND_ERR;
            }
            customer.setCreateDate(customer_.getCreateDate());
            if (customerDAO.updateCustomer(customer)) {
                return new BUSResult(customer);
            }
        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, "");
        }
        return BUSHelper.DB_ERR;
    }

}
