/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.dto;

/**
 *
 * @author baolt
 */
public enum ChargeStatus {
    SUCCESS, EXCEPTION, FAILED;

    public static ChargeStatus fromInt(int num) {
        switch (num) {
            case 1:
                return SUCCESS;
            case -1:
                return EXCEPTION;
            case -2:
                return FAILED;
        }
        return null;
    }


public String toString() {
        switch (this) {
            case SUCCESS:
                return "Thanh Cong";
            case EXCEPTION:
                return "Exception";
            case FAILED:
                return "Failed";
        }
        return "";
    }
    
    public int toInt(){
        switch (this) {
             case SUCCESS:
                return 1;
            case EXCEPTION:
                return -1;
            case FAILED:
                return -2;
        }
        return 0;
    }

}
