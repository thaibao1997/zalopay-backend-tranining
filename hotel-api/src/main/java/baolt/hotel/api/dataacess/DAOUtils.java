package baolt.hotel.api.dataacess;

import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author baolt
 */
public class DAOUtils {

    public static void fillAllCaches() {
        try {
            BookingDAO bookingDAO = new BookingDAO();
            ChargingDAO chargingDAO = new ChargingDAO();
            CheckoutDAO checkoutDAO = new CheckoutDAO();
            CustomerDAO customerDAO = new CustomerDAO();
            MenuDAO menuDAO = new MenuDAO();
            RoomDAO roomDAO = new RoomDAO();
            
            bookingDAO.getAllBookings();
            chargingDAO.getAllChargings();
            checkoutDAO.getAllCheckout();
            customerDAO.getAllCustomers();
            menuDAO.getAllMenus();
            roomDAO.getAllRooms();
        } catch (SQLException ex) {
            Logger.getLogger(DAOUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
