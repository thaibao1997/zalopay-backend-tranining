/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.dataacess;

import baolt.hotel.api.cache.CheckoutCache;
import baolt.hotel.api.database.CheckoutDB;
import baolt.hotel.api.dto.Checkout;
import baolt.hotel.api.kafka.KafkaConnect;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class CheckoutDAO {

    CheckoutDB db;
    CheckoutCache cache;
    KafkaConnect kafka;

    public CheckoutDAO() {
        db = new CheckoutDB();
        cache = CheckoutCache.getInstance();
        kafka = new KafkaConnect();
    }

    public Collection<Checkout> getAllCheckout() throws SQLException {
        if (cache.isExist()) {
            return cache.toCollection();
        }
        ArrayList<Checkout> arr = db.getAllCheckout();
        cache.fromList(arr);
        return arr;
    }

    public Checkout getCheckout(String bookingId, String roomId) throws SQLException {
        Checkout checkout = cache.findCheckoutByID(bookingId, roomId);
        if (checkout != null) {
            return checkout;
        }
        checkout = db.getCheckout(bookingId, roomId);
        if (!cache.addCheckout(checkout)) {
            return null;
        }
        return checkout;
    }

    public boolean deleteCheckout(String bookingId, String roomId) throws SQLException {
        boolean res = cache.deleteCheckout(bookingId, roomId);
        if (res) {
            Thread thread = new Thread(() -> {
                try {
                    db.deleteCheckout(bookingId, roomId);
                } catch (SQLException ex) {
                    Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            thread.start();
        }
        return res;

    }

    public boolean addCheckout(Checkout checkout) throws SQLException {
        boolean res = cache.addCheckout(checkout);
        if (res) {
            Thread thread = new Thread(() -> {
                try {
                    db.addCheckout(checkout);
                } catch (SQLException ex) {
                    Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            thread.start();
            kafka.writeLog(getCheckoutLogString(checkout));
        }
        return res;
    }

    public boolean updateCheckout(Checkout checkout) throws SQLException {
        boolean res = cache.updateCheckout(checkout);
        if (res) {
            Thread thread = new Thread(() -> {
                try {
                    db.updateCheckout(checkout);
                } catch (SQLException ex) {
                    Logger.getLogger(CustomerDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            thread.start();
        }
        return res;

    }

    public static String getCheckoutLogString(Checkout checkout) {
        JSONObject json = new JSONObject();
        json.put("Operation", "Checkout");
        json.put("Info", checkout.toJsonObject());
        return json.toString();

    }

}
