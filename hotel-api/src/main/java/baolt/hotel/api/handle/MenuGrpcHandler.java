/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.handle;

import baolt.hotel.api.business.BUSResult;
import baolt.hotel.api.business.MenuBUS;
import baolt.hotel.api.dto.Menu;
import baolt.hotel.api.grpc.menu.MenuDeleteParams;
import baolt.hotel.api.grpc.menu.MenuMsg;
import baolt.hotel.api.grpc.menu.MenuGetAllParams;
import baolt.hotel.api.grpc.menu.MenuServicesGrpc;
import static baolt.hotel.api.handle.HandlerHelper.getRuntimeExceptionWithMessage;
import io.grpc.stub.StreamObserver;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 *
 * @author baolt
 */
public class MenuGrpcHandler extends MenuServicesGrpc.MenuServicesImplBase {

    MenuBUS menuBUS = new MenuBUS();

    @Override
    public void delete(MenuDeleteParams request, StreamObserver<MenuMsg> responseObserver) {
        BUSResult res = menuBUS.deleteMenu(request.getId());
        handleBUSResult(res, responseObserver);
    }

    @Override
    public void update(MenuMsg request, StreamObserver<MenuMsg> responseObserver) {
        Menu menu = convertMenuMsgToMenu(request);
        BUSResult res = menuBUS.updateMenu(menu);
        handleBUSResult(res, responseObserver);

    }

    @Override
    public void add(MenuMsg request, StreamObserver<MenuMsg> responseObserver) {
        Menu menu = convertMenuMsgToMenu(request);
        BUSResult res = menuBUS.addMenu(menu);
        handleBUSResult(res, responseObserver);
    }

    @Override
    public void getAll(MenuGetAllParams request, StreamObserver<MenuMsg> responseObserver) {
        Collection<Menu> menus = menuBUS.getAllMenus();
        if (menus == null) {
            responseObserver
                    .onError(getRuntimeExceptionWithMessage("Database Error"));
        } else {
            for (Menu menu : menus) {
                MenuMsg menuE = convertMenuToMenuMsg(menu);
                responseObserver.onNext(menuE);
            }
            responseObserver.onCompleted();

        }
    }

    private static void handleBUSResult(BUSResult res, StreamObserver<MenuMsg> responseObserver) {
        if (res.err) {
            responseObserver
                    .onError(getRuntimeExceptionWithMessage(res.errMsg));
        } else {
            MenuMsg resp = convertMenuToMenuMsg((Menu) res.returnObj);
            responseObserver.onNext(resp);
            responseObserver.onCompleted();
        }
    }

    public static MenuMsg convertMenuToMenuMsg(Menu menu) {
        MenuMsg menuE = MenuMsg.newBuilder()
                .setMenuId(menu.getMenuId())
                .setRoomId(menu.getRoomId())
                .setAmount(menu.getAmount())
                .setIsActive(menu.isIsActive())
                .setCreateDate(menu.getCreateDate().getTime())
                .setNote(menu.getNote())
                .build();
        return menuE;
    }

    public static Menu convertMenuMsgToMenu(MenuMsg menuE) {
        Menu menu = new Menu();
        menu.setMenuId(menuE.getMenuId());
        menu.setRoomId(menuE.getRoomId());
        menu.setAmount(menuE.getAmount());
        menu.setCreateDate(new Date(menuE.getCreateDate()));
        menu.setIsActive(menuE.getIsActive());
        menu.setNote(menuE.getNote());
        return menu;
    }

}
