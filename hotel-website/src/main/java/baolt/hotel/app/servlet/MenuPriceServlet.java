/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.app.servlet;

import baolt.hotel.app.model.MenuModel;
import baolt.hotel.app.model.RoomModel;
import static baolt.hotel.app.utils.ServerHelper.getRequestStr;
import static baolt.hotel.app.utils.ServerHelper.getUserName;
import baolt.hotel.app.view.MenuView;
import hapax.TemplateException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class MenuPriceServlet extends RestHttpServlet {
    
    MenuModel menuModel = new MenuModel();
    RoomModel roomModel =new RoomModel();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try {
            JSONArray menus = menuModel.getAllMenu();
            JSONArray rooms = roomModel.getAllRoom();

            resp.getWriter().print(MenuView.getMenuView(menus, rooms,getUserName(req)));
        } catch (TemplateException ex) {
            resp.sendError(500);
            Logger.getLogger(RoomServlet.class.getName()).log(Level.SEVERE, null, ex);
        } catch (Exception e) {
            e.printStackTrace();
            resp.sendError(500);
        }
    }

    @Override
    protected void doPatch(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String id = req.getParameter("id");
            if (id == null || id.isEmpty()) {
                resp.sendError(400);
                return;
            }
            String reqBody = getRequestStr(req);
            JSONObject jsonObject = new JSONObject(reqBody);
            jsonObject.put("menuId", id);
            System.out.println("Client update menu: " + jsonObject.toString());
            String res = menuModel.updateMenu(jsonObject).toString();
            resp.getWriter().print(res);
        } catch (Exception e) {
            resp.sendError(500);

        }
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        if (id == null || id.isEmpty()) {
            resp.sendError(400);
            return;
        }
        try {
            String res = menuModel.deleteMenu(id).toString();
            resp.getWriter().print(res);
        } catch (Exception e) {
            resp.sendError(500);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String reqBody = getRequestStr(req);
        System.out.println("Client add new Menu: " + reqBody);
        JSONObject jsonObject = new JSONObject(reqBody);
        try {
            String result = menuModel.addMenu(jsonObject).toString();
            resp.getWriter().print(result);
        } catch (Exception e) {
            resp.sendError(500);
        }
    }

}
