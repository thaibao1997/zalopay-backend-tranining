/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.business;

import baolt.hotel.api.database.DataSource;
import baolt.hotel.api.dto.Booking;
import baolt.hotel.api.dto.BookingStatus;
import baolt.hotel.api.dto.Checkout;
import baolt.hotel.api.idgenerate.IDGenHelper;
import baolt.hotel.api.idgenerate.IDGenerator;
import baolt.hotel.api.util.APIHelper;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import testUtil.TestUtil;

/**
 *
 * @author baolt
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BookingBUSTest {

    BookingBUS instance;
    ArrayList<Booking> testBookings;
    Booking failBooking;

    public BookingBUSTest() {
        instance = new BookingBUS();
        testBookings = new ArrayList<>();

        Booking booking = new Booking();
        booking.setBookingId(IDGenHelper.getBookingIDDateString()+"000000001");
        booking.setRoomId("R01");
        booking.setCustomerId("C01");
        booking.setBookingDate(new Date());
        booking.setBookingStatus(BookingStatus.ACTIVE);
        booking.setFromDate(APIHelper.getDateFromStr("1/1/2019"));
        booking.setToDate(APIHelper.getDateFromStr("2/1/2019"));
        
        ;
        testBookings.add(booking);

        booking = new Booking();
        booking.setBookingId(IDGenHelper.getBookingIDDateString()+"000000002");
        booking.setRoomId("R01");
        booking.setCustomerId("C01");
        booking.setBookingDate(new Date());
        booking.setBookingStatus(BookingStatus.ACTIVE);
        booking.setFromDate(APIHelper.getDateFromStr("1/2/2019"));
        booking.setToDate(APIHelper.getDateFromStr("3/2/2019"));

        testBookings.add(booking);

        failBooking = new Booking();
        failBooking.setBookingId("B03");
        failBooking.setBookingDate(new Date());
        failBooking.setBookingStatus(BookingStatus.ACTIVE);
        failBooking.setFromDate(APIHelper.getDateFromStr("1/2/2019"));
        failBooking.setToDate(APIHelper.getDateFromStr("3/2/2019"));

    }

    @BeforeClass
    public static void setUpClass() {
        IDGenerator gen = IDGenerator.getInstance();
        gen.startGenerateID();
        TestUtil.flushAllCaches();

        DataSource.switchToTestDatabase();
        DataSource dataSource = DataSource.getInstance();
        dataSource.dropAllData();

        try (Connection connection = dataSource.getConnection()) {
            Statement st = connection.createStatement();
            String query;

            st = connection.createStatement();
            query = "DELETE FROM Customer WHERE True";
            st.execute(query);

            st = connection.createStatement();
            query = "Insert into Room values ('R01','Room 01',1,NOW(),true,'Phong 001')";
            st.execute(query);

            st = connection.createStatement();
            query = "Insert into Room values ('R02','Room 02',1,NOW(),false,'Phong 002')";
            st.execute(query);

            st = connection.createStatement();
            query = "Insert into Room values ('R03','Room 03',1,NOW(),true,'Phong 002')";
            st.execute(query);

            st = connection.createStatement();
            query = "Insert into Customer values ('C01','Nguyen Van A','VN',NOW(),'123456',NOW())";
            st.execute(query);

            st = connection.createStatement();
            query = "Insert into Menu values ('M01','R01',20000,true,NOW(),'123456')";
            st.execute(query);
        } catch (SQLException ex) {
            Logger.getLogger(RoomBUSTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @AfterClass
    public static void tearDownClass() {
        TestUtil.flushAllCaches();
        DataSource.swicthToMainDatabase();
    }

    /**
     * Test of addBooking method, of class BookingBUS.
     */
    @Test
    public void AA_testAddBooking() {
        System.out.println("addBooking");
        BUSResult expResult = new BUSResult(testBookings.get(0));
        BUSResult result = instance.addBooking(testBookings.get(0));

        BUSResult expResult1 = new BUSResult(testBookings.get(1));
        BUSResult result1 = instance.addBooking(testBookings.get(1));

        assertEquals(expResult, result);
        assertEquals(expResult1, result1);

    }

    @Test
    public void AB_testAddBooking_FAIL_ROOMID_NOT_FOUND() {
        System.out.println("addBooking");

        failBooking.setRoomId("ZOZZ");
        failBooking.setCustomerId("C01");
        BUSResult expResult = new BUSResult(BookingBUS.FK_ERR_MSG);
        BUSResult result = instance.addBooking(failBooking);

        assertEquals(expResult, result);
    }

    @Test
    public void AC_testAddBooking_FAIL_CUSTOMERD_NOT_FOUND() {
        System.out.println("addBooking");
        failBooking.setRoomId("R03");
        failBooking.setCustomerId("ZZZZ");
        BUSResult expResult = new BUSResult(BookingBUS.FK_ERR_MSG);
        BUSResult result = instance.addBooking(failBooking);

        assertEquals(expResult, result);
    }

    @Test
    public void AD_testAddBooking_FAIL_NOT_ACTIVE_ROOM() {
        System.out.println("addBooking");
        failBooking.setRoomId("R02");
        failBooking.setCustomerId("C01");
        BUSResult expResult = new BUSResult(BookingBUS.NOT_ACTIVE_ROOM_ERR_MSG);
        BUSResult result = instance.addBooking(failBooking);
        assertEquals(expResult, result);
    }

    @Test
    public void AE_testAddBooking_FAIL_FROMDATE_BEORE_TODATE() {
        System.out.println("addBooking");
        failBooking.setRoomId("R03");
        failBooking.setCustomerId("C01");
        failBooking.setFromDate(APIHelper.getDateFromStr("6/2/2019"));
        failBooking.setToDate(APIHelper.getDateFromStr("3/2/2019"));
        BUSResult expResult = new BUSResult(BookingBUS.FD_TD_ERR_MSG);
        BUSResult result = instance.addBooking(failBooking);
        assertEquals(expResult, result);
    }

    @Test
    public void AF_testAddBooking_FAIL_BOOKING_IN_PAST() {
        System.out.println("addBooking");
        failBooking.setRoomId("R03");
        failBooking.setCustomerId("C01");
        failBooking.setFromDate(APIHelper.getDateFromStr("6/2/2016"));
        failBooking.setToDate(APIHelper.getDateFromStr("3/2/2019"));
        BUSResult expResult = new BUSResult(BookingBUS.PAST_BOOKING_ERR_MSG);
        BUSResult result = instance.addBooking(failBooking);
        assertEquals(expResult, result);
    }

    @Test
    public void AG_testAddBooking_FAIL_BOOKED_ROOM() {
        System.out.println("addBooking");
        failBooking.setRoomId("R01");
        failBooking.setCustomerId("C01");
        failBooking.setFromDate(APIHelper.getDateFromStr("1/1/2019"));
        failBooking.setToDate(APIHelper.getDateFromStr("3/1/2019"));
        BUSResult result = instance.addBooking(failBooking);
        assertTrue(result.err);
    }

    /**
     * Test of getAllBookings method, of class BookingBUS.
     */
    @Test
    public void B_testGetAllBookings() {
        System.out.println("getAllBookings");
        ArrayList<Booking> expResult = testBookings;
        Collection<Booking> result = instance.getAllBookings();
        assertEquals(expResult.size(), result.size());

    }

    /**
     * Test of getBookingByID method, of class BookingBUS.
     */
    @Test
    public void C_testGetBookingByID() {
        System.out.println("getBookingByID");
        Booking booking = testBookings.get(0);
        BUSResult expResult = new BUSResult(booking);
        BUSResult result = instance.getBookingByID(booking.getBookingId());
        assertEquals(expResult, result);
    }

    /**
     * Test of checkout method, of class BookingBUS.
     */
    @Test
    public void DA_testCheckout() {
        System.out.println("checkout");
        String id = testBookings.get(0).getBookingId();
        BUSResult result = instance.checkout(id);
        assertFalse(result.err);
        Checkout checkout = (Checkout) result.returnObj;
        assertEquals(2 * 20000, checkout.getTotalAmount());
        assertEquals(id, checkout.getBookingId());
    }

    @Test
    public void DB_testCheckout_FAIL_CHECKOUTED_BOOKING() {
        System.out.println("checkout");
        String id = testBookings.get(0).getBookingId();
        BUSResult expected = new BUSResult(BookingBUS.CHECKOUTED_BOOKING_ERR_MSG);
        BUSResult result = instance.checkout(id);
        assertEquals(expected, result);

    }

    /**
     * Test of updateBooking method, of class BookingBUS.
     */
    @Test
    public void E_testUpdateBooking() {
        System.out.println("updateBooking");
        Booking booking = testBookings.get(0);
        booking.setToDate(APIHelper.getDateFromStr("1/2/2019"));

        BUSResult expResult = new BUSResult(booking);
        BUSResult result = instance.updateBooking(booking);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteBooking method, of class BookingBUS.
     */
    @Test
    public void F_testDeleteBooking() {
        System.out.println("deleteBooking");
        Booking booking = testBookings.get(1);
        BUSResult expResult = new BUSResult(booking);
        BUSResult result = instance.deleteBooking(booking.getBookingId());
        assertEquals(expResult, result);
    }

}
