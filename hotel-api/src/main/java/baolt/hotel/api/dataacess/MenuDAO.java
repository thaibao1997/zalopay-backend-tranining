/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.dataacess;

import baolt.hotel.api.cache.MenuCache;
import baolt.hotel.api.database.MenuDB;
import baolt.hotel.api.dto.Customer;
import baolt.hotel.api.dto.Menu;
import baolt.hotel.api.dto.Room;
import static baolt.hotel.api.util.APIHelper.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author baolt
 */
public class MenuDAO {

    private MenuDB db;
    private MenuCache cache;

    public MenuDAO() {
        db = new MenuDB();
        cache = MenuCache.getInstance();
    }

    public Collection<Menu> getAllMenus() throws SQLException {
        if (cache.isExist()) {
            return cache.toCollection();
        }
        ArrayList<Menu> cus = db.getAllMenus();
        cache.fromList(cus);
        return cus;
    }

    public Menu getMenu(String id) throws SQLException {
        Menu menu = cache.findMenuByID(id);
        if (menu != null) {
            return menu;
        }
        menu = db.getMenu(id);
        if(!cache.addMenu(menu)){
            return null;
        }
        return menu;
    }

    public boolean addMenu(Menu menu) {
        boolean res = cache.addMenu(menu);
        if (res) {
            Thread thread = new Thread(() -> {
                try {
                    db.addMenu(menu);
                } catch (SQLException ex) {
                    Logger.getLogger(MenuDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            thread.start();
        }
        return res;
    }

    public boolean deleteMenu(String id) throws SQLException {
        boolean res = cache.deleteMenu(id);
        if (res) {
            Thread thread = new Thread(() -> {
                try {
                    db.deleteMenu(id);
                } catch (SQLException ex) {
                    Logger.getLogger(MenuDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            thread.start();
        }
        return res;
    }

    public boolean updateMenu(Menu menu) throws SQLException {
        boolean res = cache.updateMenu(menu);
        if (res) {
            Thread thread = new Thread(() -> {
                try {
                    db.updateMenu(menu);
                } catch (SQLException ex) {
                    Logger.getLogger(MenuDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            thread.start();
        }
        return res;
    }

    public void deactiveAllOfRoom(String roomId) throws SQLException {
        cache.deactiveAllOfRoom(roomId);
        Thread thread = new Thread(() -> {
            try {
                db.deactiveAllOfRoom(roomId);
            } catch (SQLException ex) {
                Logger.getLogger(MenuDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        thread.start();

    }

    public Menu getActiveMenuOfRoom(String roomId) throws SQLException {
        if (cache.isExist()) {
            return cache.getActiveMenuOfRoom(roomId);
        }
        Menu menu= db.getActiveMenuOfRoom(roomId);
        cache.addMenu(menu);
        return menu;
    }
}
