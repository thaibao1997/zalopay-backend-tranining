package baolt.hotel.api.grpc.customer;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.13.0-SNAPSHOT)",
    comments = "Source: Customer.proto")
public final class CustomerServicesGrpc {

  private CustomerServicesGrpc() {}

  public static final String SERVICE_NAME = "CustomerServices";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getGetAllMethod()} instead. 
  public static final io.grpc.MethodDescriptor<baolt.hotel.api.grpc.customer.CustomerGetAllParams,
      baolt.hotel.api.grpc.customer.CustomerMsg> METHOD_GET_ALL = getGetAllMethodHelper();

  private static volatile io.grpc.MethodDescriptor<baolt.hotel.api.grpc.customer.CustomerGetAllParams,
      baolt.hotel.api.grpc.customer.CustomerMsg> getGetAllMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.customer.CustomerGetAllParams,
      baolt.hotel.api.grpc.customer.CustomerMsg> getGetAllMethod() {
    return getGetAllMethodHelper();
  }

  private static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.customer.CustomerGetAllParams,
      baolt.hotel.api.grpc.customer.CustomerMsg> getGetAllMethodHelper() {
    io.grpc.MethodDescriptor<baolt.hotel.api.grpc.customer.CustomerGetAllParams, baolt.hotel.api.grpc.customer.CustomerMsg> getGetAllMethod;
    if ((getGetAllMethod = CustomerServicesGrpc.getGetAllMethod) == null) {
      synchronized (CustomerServicesGrpc.class) {
        if ((getGetAllMethod = CustomerServicesGrpc.getGetAllMethod) == null) {
          CustomerServicesGrpc.getGetAllMethod = getGetAllMethod = 
              io.grpc.MethodDescriptor.<baolt.hotel.api.grpc.customer.CustomerGetAllParams, baolt.hotel.api.grpc.customer.CustomerMsg>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(
                  "CustomerServices", "getAll"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.customer.CustomerGetAllParams.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.customer.CustomerMsg.getDefaultInstance()))
                  .setSchemaDescriptor(new CustomerServicesMethodDescriptorSupplier("getAll"))
                  .build();
          }
        }
     }
     return getGetAllMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getAddMethod()} instead. 
  public static final io.grpc.MethodDescriptor<baolt.hotel.api.grpc.customer.CustomerMsg,
      baolt.hotel.api.grpc.customer.CustomerMsg> METHOD_ADD = getAddMethodHelper();

  private static volatile io.grpc.MethodDescriptor<baolt.hotel.api.grpc.customer.CustomerMsg,
      baolt.hotel.api.grpc.customer.CustomerMsg> getAddMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.customer.CustomerMsg,
      baolt.hotel.api.grpc.customer.CustomerMsg> getAddMethod() {
    return getAddMethodHelper();
  }

  private static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.customer.CustomerMsg,
      baolt.hotel.api.grpc.customer.CustomerMsg> getAddMethodHelper() {
    io.grpc.MethodDescriptor<baolt.hotel.api.grpc.customer.CustomerMsg, baolt.hotel.api.grpc.customer.CustomerMsg> getAddMethod;
    if ((getAddMethod = CustomerServicesGrpc.getAddMethod) == null) {
      synchronized (CustomerServicesGrpc.class) {
        if ((getAddMethod = CustomerServicesGrpc.getAddMethod) == null) {
          CustomerServicesGrpc.getAddMethod = getAddMethod = 
              io.grpc.MethodDescriptor.<baolt.hotel.api.grpc.customer.CustomerMsg, baolt.hotel.api.grpc.customer.CustomerMsg>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "CustomerServices", "add"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.customer.CustomerMsg.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.customer.CustomerMsg.getDefaultInstance()))
                  .setSchemaDescriptor(new CustomerServicesMethodDescriptorSupplier("add"))
                  .build();
          }
        }
     }
     return getAddMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getUpdateMethod()} instead. 
  public static final io.grpc.MethodDescriptor<baolt.hotel.api.grpc.customer.CustomerMsg,
      baolt.hotel.api.grpc.customer.CustomerMsg> METHOD_UPDATE = getUpdateMethodHelper();

  private static volatile io.grpc.MethodDescriptor<baolt.hotel.api.grpc.customer.CustomerMsg,
      baolt.hotel.api.grpc.customer.CustomerMsg> getUpdateMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.customer.CustomerMsg,
      baolt.hotel.api.grpc.customer.CustomerMsg> getUpdateMethod() {
    return getUpdateMethodHelper();
  }

  private static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.customer.CustomerMsg,
      baolt.hotel.api.grpc.customer.CustomerMsg> getUpdateMethodHelper() {
    io.grpc.MethodDescriptor<baolt.hotel.api.grpc.customer.CustomerMsg, baolt.hotel.api.grpc.customer.CustomerMsg> getUpdateMethod;
    if ((getUpdateMethod = CustomerServicesGrpc.getUpdateMethod) == null) {
      synchronized (CustomerServicesGrpc.class) {
        if ((getUpdateMethod = CustomerServicesGrpc.getUpdateMethod) == null) {
          CustomerServicesGrpc.getUpdateMethod = getUpdateMethod = 
              io.grpc.MethodDescriptor.<baolt.hotel.api.grpc.customer.CustomerMsg, baolt.hotel.api.grpc.customer.CustomerMsg>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "CustomerServices", "update"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.customer.CustomerMsg.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.customer.CustomerMsg.getDefaultInstance()))
                  .setSchemaDescriptor(new CustomerServicesMethodDescriptorSupplier("update"))
                  .build();
          }
        }
     }
     return getUpdateMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getDeleteMethod()} instead. 
  public static final io.grpc.MethodDescriptor<baolt.hotel.api.grpc.customer.CustomerDeleteParams,
      baolt.hotel.api.grpc.customer.CustomerMsg> METHOD_DELETE = getDeleteMethodHelper();

  private static volatile io.grpc.MethodDescriptor<baolt.hotel.api.grpc.customer.CustomerDeleteParams,
      baolt.hotel.api.grpc.customer.CustomerMsg> getDeleteMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.customer.CustomerDeleteParams,
      baolt.hotel.api.grpc.customer.CustomerMsg> getDeleteMethod() {
    return getDeleteMethodHelper();
  }

  private static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.customer.CustomerDeleteParams,
      baolt.hotel.api.grpc.customer.CustomerMsg> getDeleteMethodHelper() {
    io.grpc.MethodDescriptor<baolt.hotel.api.grpc.customer.CustomerDeleteParams, baolt.hotel.api.grpc.customer.CustomerMsg> getDeleteMethod;
    if ((getDeleteMethod = CustomerServicesGrpc.getDeleteMethod) == null) {
      synchronized (CustomerServicesGrpc.class) {
        if ((getDeleteMethod = CustomerServicesGrpc.getDeleteMethod) == null) {
          CustomerServicesGrpc.getDeleteMethod = getDeleteMethod = 
              io.grpc.MethodDescriptor.<baolt.hotel.api.grpc.customer.CustomerDeleteParams, baolt.hotel.api.grpc.customer.CustomerMsg>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "CustomerServices", "delete"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.customer.CustomerDeleteParams.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.customer.CustomerMsg.getDefaultInstance()))
                  .setSchemaDescriptor(new CustomerServicesMethodDescriptorSupplier("delete"))
                  .build();
          }
        }
     }
     return getDeleteMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static CustomerServicesStub newStub(io.grpc.Channel channel) {
    return new CustomerServicesStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static CustomerServicesBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new CustomerServicesBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static CustomerServicesFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new CustomerServicesFutureStub(channel);
  }

  /**
   */
  public static abstract class CustomerServicesImplBase implements io.grpc.BindableService {

    /**
     */
    public void getAll(baolt.hotel.api.grpc.customer.CustomerGetAllParams request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.customer.CustomerMsg> responseObserver) {
      asyncUnimplementedUnaryCall(getGetAllMethodHelper(), responseObserver);
    }

    /**
     */
    public void add(baolt.hotel.api.grpc.customer.CustomerMsg request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.customer.CustomerMsg> responseObserver) {
      asyncUnimplementedUnaryCall(getAddMethodHelper(), responseObserver);
    }

    /**
     */
    public void update(baolt.hotel.api.grpc.customer.CustomerMsg request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.customer.CustomerMsg> responseObserver) {
      asyncUnimplementedUnaryCall(getUpdateMethodHelper(), responseObserver);
    }

    /**
     */
    public void delete(baolt.hotel.api.grpc.customer.CustomerDeleteParams request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.customer.CustomerMsg> responseObserver) {
      asyncUnimplementedUnaryCall(getDeleteMethodHelper(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetAllMethodHelper(),
            asyncServerStreamingCall(
              new MethodHandlers<
                baolt.hotel.api.grpc.customer.CustomerGetAllParams,
                baolt.hotel.api.grpc.customer.CustomerMsg>(
                  this, METHODID_GET_ALL)))
          .addMethod(
            getAddMethodHelper(),
            asyncUnaryCall(
              new MethodHandlers<
                baolt.hotel.api.grpc.customer.CustomerMsg,
                baolt.hotel.api.grpc.customer.CustomerMsg>(
                  this, METHODID_ADD)))
          .addMethod(
            getUpdateMethodHelper(),
            asyncUnaryCall(
              new MethodHandlers<
                baolt.hotel.api.grpc.customer.CustomerMsg,
                baolt.hotel.api.grpc.customer.CustomerMsg>(
                  this, METHODID_UPDATE)))
          .addMethod(
            getDeleteMethodHelper(),
            asyncUnaryCall(
              new MethodHandlers<
                baolt.hotel.api.grpc.customer.CustomerDeleteParams,
                baolt.hotel.api.grpc.customer.CustomerMsg>(
                  this, METHODID_DELETE)))
          .build();
    }
  }

  /**
   */
  public static final class CustomerServicesStub extends io.grpc.stub.AbstractStub<CustomerServicesStub> {
    private CustomerServicesStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CustomerServicesStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CustomerServicesStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CustomerServicesStub(channel, callOptions);
    }

    /**
     */
    public void getAll(baolt.hotel.api.grpc.customer.CustomerGetAllParams request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.customer.CustomerMsg> responseObserver) {
      asyncServerStreamingCall(
          getChannel().newCall(getGetAllMethodHelper(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void add(baolt.hotel.api.grpc.customer.CustomerMsg request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.customer.CustomerMsg> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getAddMethodHelper(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void update(baolt.hotel.api.grpc.customer.CustomerMsg request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.customer.CustomerMsg> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getUpdateMethodHelper(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void delete(baolt.hotel.api.grpc.customer.CustomerDeleteParams request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.customer.CustomerMsg> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getDeleteMethodHelper(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class CustomerServicesBlockingStub extends io.grpc.stub.AbstractStub<CustomerServicesBlockingStub> {
    private CustomerServicesBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CustomerServicesBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CustomerServicesBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CustomerServicesBlockingStub(channel, callOptions);
    }

    /**
     */
    public java.util.Iterator<baolt.hotel.api.grpc.customer.CustomerMsg> getAll(
        baolt.hotel.api.grpc.customer.CustomerGetAllParams request) {
      return blockingServerStreamingCall(
          getChannel(), getGetAllMethodHelper(), getCallOptions(), request);
    }

    /**
     */
    public baolt.hotel.api.grpc.customer.CustomerMsg add(baolt.hotel.api.grpc.customer.CustomerMsg request) {
      return blockingUnaryCall(
          getChannel(), getAddMethodHelper(), getCallOptions(), request);
    }

    /**
     */
    public baolt.hotel.api.grpc.customer.CustomerMsg update(baolt.hotel.api.grpc.customer.CustomerMsg request) {
      return blockingUnaryCall(
          getChannel(), getUpdateMethodHelper(), getCallOptions(), request);
    }

    /**
     */
    public baolt.hotel.api.grpc.customer.CustomerMsg delete(baolt.hotel.api.grpc.customer.CustomerDeleteParams request) {
      return blockingUnaryCall(
          getChannel(), getDeleteMethodHelper(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class CustomerServicesFutureStub extends io.grpc.stub.AbstractStub<CustomerServicesFutureStub> {
    private CustomerServicesFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CustomerServicesFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CustomerServicesFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CustomerServicesFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<baolt.hotel.api.grpc.customer.CustomerMsg> add(
        baolt.hotel.api.grpc.customer.CustomerMsg request) {
      return futureUnaryCall(
          getChannel().newCall(getAddMethodHelper(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<baolt.hotel.api.grpc.customer.CustomerMsg> update(
        baolt.hotel.api.grpc.customer.CustomerMsg request) {
      return futureUnaryCall(
          getChannel().newCall(getUpdateMethodHelper(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<baolt.hotel.api.grpc.customer.CustomerMsg> delete(
        baolt.hotel.api.grpc.customer.CustomerDeleteParams request) {
      return futureUnaryCall(
          getChannel().newCall(getDeleteMethodHelper(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_ALL = 0;
  private static final int METHODID_ADD = 1;
  private static final int METHODID_UPDATE = 2;
  private static final int METHODID_DELETE = 3;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final CustomerServicesImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(CustomerServicesImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_ALL:
          serviceImpl.getAll((baolt.hotel.api.grpc.customer.CustomerGetAllParams) request,
              (io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.customer.CustomerMsg>) responseObserver);
          break;
        case METHODID_ADD:
          serviceImpl.add((baolt.hotel.api.grpc.customer.CustomerMsg) request,
              (io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.customer.CustomerMsg>) responseObserver);
          break;
        case METHODID_UPDATE:
          serviceImpl.update((baolt.hotel.api.grpc.customer.CustomerMsg) request,
              (io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.customer.CustomerMsg>) responseObserver);
          break;
        case METHODID_DELETE:
          serviceImpl.delete((baolt.hotel.api.grpc.customer.CustomerDeleteParams) request,
              (io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.customer.CustomerMsg>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class CustomerServicesBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    CustomerServicesBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return baolt.hotel.api.grpc.customer.CustomerProto.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("CustomerServices");
    }
  }

  private static final class CustomerServicesFileDescriptorSupplier
      extends CustomerServicesBaseDescriptorSupplier {
    CustomerServicesFileDescriptorSupplier() {}
  }

  private static final class CustomerServicesMethodDescriptorSupplier
      extends CustomerServicesBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    CustomerServicesMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (CustomerServicesGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new CustomerServicesFileDescriptorSupplier())
              .addMethod(getGetAllMethodHelper())
              .addMethod(getAddMethodHelper())
              .addMethod(getUpdateMethodHelper())
              .addMethod(getDeleteMethodHelper())
              .build();
        }
      }
    }
    return result;
  }
}
