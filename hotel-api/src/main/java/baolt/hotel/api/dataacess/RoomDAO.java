/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.dataacess;

import baolt.hotel.api.cache.RoomCache;
import baolt.hotel.api.database.RoomDB;
import baolt.hotel.api.dto.Menu;
import baolt.hotel.api.dto.Room;
import baolt.hotel.api.dto.RoomType;
import static baolt.hotel.api.util.APIHelper.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author baolt
 */
public class RoomDAO {


    private RoomCache cache;
    private RoomDB db;

    public RoomDAO() {
        db = new RoomDB();
        cache = RoomCache.getInstance();
    }


    public Collection<Room> getAllRooms() throws SQLException {

        if (cache.isExist()) {
            return cache.toCollection();
        }
        ArrayList<Room> rooms = db.getAllRooms();
        cache.fromList(rooms);
        return rooms;

    }

    public Room getRoom(String roomID) throws SQLException {
        Room room = cache.findRoomByID(roomID);
        if (room != null) {
            return room;
        }
        room = db.getRoom(roomID);
        if(!cache.addRoom(room)){
            return null;
        }
        return room;
    }

    public boolean addRom(Room room)  {
        boolean res = cache.addRoom(room);
        if(res){
            Thread thread = new Thread(()->{
                try {
                    db.addRom(room);
                } catch (SQLException ex) {
                    Logger.getLogger(RoomDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            thread.start();
        }
        return res;
    }

    public boolean deleteRoom(String id)  {
        boolean res = cache.deleteRoom(id);
        if(res){
            Thread thread = new Thread(()->{
                try {
                    db.deleteRoom(id);
                } catch (SQLException ex) {
                    Logger.getLogger(RoomDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            thread.start();
        }
        return res;

    }

    public boolean updateRoom(Room room)  {
        boolean res= cache.updateRoom(room);
         if(res){
            Thread thread = new Thread(()->{
                try {
                    db.updateRoom(room);
                } catch (SQLException ex) {
                    Logger.getLogger(RoomDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            thread.start();
        }
        return res;
    }

}
