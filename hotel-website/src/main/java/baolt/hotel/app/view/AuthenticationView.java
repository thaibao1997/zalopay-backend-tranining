/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.app.view;

import static baolt.hotel.app.view.ViewRenderer.VIEWS_FOLDER;
import hapax.Template;
import hapax.TemplateDictionary;
import hapax.TemplateException;
import hapax.TemplateLoader;
import hapax.TemplateResourceLoader;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;

/**
 *
 * @author baolt
 */
public class AuthenticationView {

    private static final String templateName = "login.xtm";
    private static final String jsFile = "/js/login.js";

    public static String getLoginView() throws TemplateException {
        String content = getContentSection();
        String footer = getFooterSection();
        return ViewRenderer.getMyPage("Login", content, footer,null);
    }
    
    public static String getFooterSection() {
        return ViewRenderer.getJSLink(jsFile);
    }

    public static String getContentSection() {
        try {
            TemplateLoader templateLoader = TemplateResourceLoader.create(VIEWS_FOLDER, true);
            Template template = templateLoader.getTemplate(templateName);
            TemplateDictionary templeDictionary = new TemplateDictionary();
            return template.renderToString(templeDictionary);
        } catch (TemplateException ex) {
            Logger.getLogger(AuthenticationView.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
