/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.app.model;

import baolt.hotel.api.grpc.customer.CustomerDeleteParams;
import baolt.hotel.api.grpc.customer.CustomerMsg;
import baolt.hotel.api.grpc.customer.CustomerGetAllParams;
import baolt.hotel.api.grpc.customer.CustomerServicesGrpc;
import static baolt.hotel.app.model.ModelConstansts.API_ADDRESS;
import static baolt.hotel.app.model.ModelConstansts.API_PORT;
import static baolt.hotel.app.model.ModelHelper.createErrorJSONObject;
import baolt.hotel.app.utils.ServerHelper;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import java.util.Date;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class CustomerModel {

    private final ManagedChannel channel;
    private final CustomerServicesGrpc.CustomerServicesBlockingStub blockingStub;

    public CustomerModel() {
        this.channel = ModelHelper.getChannel();

        this.blockingStub = CustomerServicesGrpc.newBlockingStub(channel);
    }

    public JSONArray getAllCustomer() {
        JSONArray arr = new JSONArray();
        if (channel.isShutdown() || channel.isTerminated()) {
            System.out.println("Server died");
            return arr;
        }
        CustomerGetAllParams params = CustomerGetAllParams.newBuilder().build();
        try {
            Iterator<CustomerMsg> reply = blockingStub.getAll(params);
            while (reply.hasNext()) {
                CustomerMsg customer = reply.next();
                JSONObject json = convertCustomerMsgToJSON(customer);
                arr.put(json);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return arr;
    }
    
    
    public JSONObject addCustomer(JSONObject json) {
        if (channel.isShutdown() || channel.isTerminated()) {
            return createErrorJSONObject("API Server died");
        }
        try {
            CustomerMsg request = convertJSONObjectToCustomerMsg(json);
            CustomerMsg response = blockingStub.add(request);
            return convertCustomerMsgToJSON(response);
        }catch (Exception ex) {
            return createErrorJSONObject(ex.getMessage());
        }
    }

    public JSONObject updateCustomer(JSONObject json) {
        if (channel.isShutdown() || channel.isTerminated()) {
            return createErrorJSONObject("API Server died");
        }
        try {
            CustomerMsg request = convertJSONObjectToCustomerMsg(json);
            CustomerMsg response = blockingStub.update(request);
            return convertCustomerMsgToJSON(response);
        }catch (Exception ex) {
            return createErrorJSONObject(ex.getMessage());
        }
    }

    public JSONObject deleteCustomer(String id) {
        if (channel.isShutdown() || channel.isTerminated()) {
            return createErrorJSONObject("API Server died");
        }
        try {
            CustomerDeleteParams dlt = CustomerDeleteParams.newBuilder().setId(id).build();
            CustomerMsg response = blockingStub.delete(dlt);
            return convertCustomerMsgToJSON(response);
        } catch (Exception ex) {
            return createErrorJSONObject(ex.getMessage());
        }

    } 
    
    public static JSONObject convertCustomerMsgToJSON(CustomerMsg customer) {
        JSONObject json = new JSONObject();
        json.put("customerId", customer.getCustomerId());
        json.put("fullName", customer.getFullName());
        json.put("address", customer.getAddress());
        json.put("phoneNumber", customer.getPhoneNumber());
        Date date = new Date(customer.getDayOfBirth());
        json.put("dayOfBirth", ServerHelper.getDateStr(date));
        date = new Date(customer.getCreateDate());
        json.put("createDate", ServerHelper.getDateStr(date));
        return json;
    }

    public static CustomerMsg convertJSONObjectToCustomerMsg(JSONObject json) {
        Date dOB = ServerHelper.getDateFromStr(json.getString("dayOfBirth"));
        CustomerMsg customer = CustomerMsg.newBuilder()
                .setCustomerId(json.getString("customerId"))
                .setFullName(json.getString("fullName"))
                .setAddress(json.getString("address"))
                .setPhoneNumber(json.getString("phoneNumber"))
                .setCreateDate(new Date().getTime())
                .setDayOfBirth(dOB.getTime())
                .build();
        return customer;
    }
}
