/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.business;

import baolt.hotel.api.database.DataSource;
import baolt.hotel.api.dto.Customer;
import baolt.hotel.api.util.APIHelper;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import testUtil.TestUtil;
import static testUtil.TestUtil.flushAllCaches;

/**
 *
 * @author baolt
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CustomerBUSTest {

    private ArrayList<Customer> testCustomers;
    private CustomerBUS instance;

    public CustomerBUSTest() {
        instance = new CustomerBUS();
        testCustomers = new ArrayList<>();

        Customer customer = new Customer();
        customer.setCustomerId("C01");
        customer.setFullName("Nguyen Van A");
        customer.setAddress("TP HCM,VN");
        customer.setCreateDate(new Date());
        customer.setDayOfBirth(APIHelper.getDateFromStr("23/07/1995"));
        customer.setPhoneNumber("12345678");
        testCustomers.add(customer);

        customer = new Customer();
        customer.setCustomerId("C02");
        customer.setFullName("Nguyen Van B");
        customer.setAddress("Ha Noi,VN");
        customer.setCreateDate(new Date());
        customer.setDayOfBirth(APIHelper.getDateFromStr("11/12/1991"));
        customer.setPhoneNumber("098776555");
        testCustomers.add(customer);
    }

    @BeforeClass
    public static void setUp() {
        TestUtil.flushAllCaches();

        DataSource.switchToTestDatabase();
        DataSource dataSource = DataSource.getInstance();
        dataSource.dropAllData();
    }

    @AfterClass
    public static void tearDown() {
        flushAllCaches();
        DataSource.swicthToMainDatabase();
    }

    /**
     * Test of addCustomer method, of class CustomerBUS.
     */
    @Test
    public void AA_testAddCustomer() {
        System.out.println("addCustomer");

        BUSResult expResult = new BUSResult(testCustomers.get(0));
        BUSResult expResult1 = new BUSResult(testCustomers.get(1));
        BUSResult result = instance.addCustomer(testCustomers.get(0));
        BUSResult result1 = instance.addCustomer(testCustomers.get(1));

        assertFalse(result.err);
        assertFalse(result1.err);
        assertEquals(expResult.returnObj, result.returnObj);
        assertEquals(expResult1.returnObj, result1.returnObj);
    }

    /**
     * Test of getAllCustomers method, of class CustomerBUS.
     */
    @Test
    public void B_testGetAllCustomers() {
        System.out.println("getAllCustomers");
        ArrayList<Customer> expResult = testCustomers;
        Collection<Customer> result = instance.getAllCustomers();
        assertEquals(expResult.size(), result.size());
    }

    /**
     * Test of getCustomerByID method, of class CustomerBUS.
     */
    @Test
    public void CA_testGetCustomerByID() {
        System.out.println("getCustomerByID");
        Customer customer = testCustomers.get(0);
        BUSResult expResult = new BUSResult(customer);
        BUSResult result = instance.getCustomerByID(customer.getCustomerId());
        assertEquals(expResult, result);
    }

    /**
     * Test of getCustomerByID method, of class CustomerBUS. Fail id not in
     * database
     */
    @Test
    public void CB_testGetCustomerByID_FAIL_ID_NOT_FOUND() {
        System.out.println("getCustomerByID - Fail");
        String id = "IDzzzz";
        BUSResult expResult = BUSHelper.ID_NOT_FOUND_ERR;
        BUSResult result = instance.getCustomerByID(id);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateCustomer method, of class CustomerBUS.
     */
    @Test
    public void DA_testUpdateCustomer() {
        System.out.println("updateCustomer");
        Customer customer = testCustomers.get(0);
        customer.setAddress("New York,US,Earth");
        customer.setPhoneNumber("555221");
        customer.setFullName("B.Obama");

        BUSResult expResult = new BUSResult(customer);
        BUSResult result = instance.updateCustomer(customer);
        assertEquals(expResult, result);
    }

    @Test
    public void DB_testUpdateCustomer_FAIL_ID_NOT_FOUND() {
        System.out.println("updateCustomer");
        Customer customer = testCustomers.get(0);
        customer.setAddress("New York,US,Earth");
        customer.setPhoneNumber("555221");
        customer.setFullName("B.Obama");
        customer.setCustomerId("IDZZZZ");

        BUSResult expResult = BUSHelper.ID_NOT_FOUND_ERR;
        BUSResult result = instance.updateCustomer(customer);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteCustomer method, of class CustomerBUS.
     */
    @Test
    public void EA_testDeleteCustomer() {
        System.out.println("deleteCustomer");
        Customer customer = testCustomers.get(1);
        BUSResult expResult = new BUSResult(customer);

        BUSResult result = instance.deleteCustomer(customer.getCustomerId());
        assertEquals(expResult, result);
    }

    @Test
    public void EB_testDeleteCustomer_FAIL_ID_NOT_FOUND() {
        System.out.println("deleteCustomer");
        String id = "IDZZZ";
        BUSResult expResult = BUSHelper.ID_NOT_FOUND_ERR;
        BUSResult result = instance.deleteCustomer(id);
        assertEquals(expResult, result);
    }

}
