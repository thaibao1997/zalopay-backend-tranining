/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.zalopay;

import baolt.hotel.api.business.BookingBUS;
import baolt.hotel.api.business.ChargingBUS;
import baolt.hotel.api.business.CheckoutBUS;
import baolt.hotel.api.dto.ChargeStatus;
import baolt.hotel.api.dto.Charging;
import baolt.hotel.api.dto.Checkout;
import baolt.hotel.api.zookeeper.ZookeeperClient;
import java.util.Date;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class PaymentCheckingThread extends Thread {
    
    public static final int EXPIRE_PAYMENT_TIME = 60; //seconds
    
    private static final int TIME_BETWEEN_CHECK_STATUS = 500; //miliseconds
    
    public static final int MAX_CHECK_STATUS_TIME
            = EXPIRE_PAYMENT_TIME*1000/TIME_BETWEEN_CHECK_STATUS;

    private ZookeeperClient zookeeperClient;

    private CheckoutBUS checkoutBUS;
    private ChargingBUS chargeBUS;

    private Checkout checkout;
    private Charging charging;

    public PaymentCheckingThread(String bookingId, String roomId, String chargingId, long amount, Date now) {
        checkoutBUS = new CheckoutBUS();
        chargeBUS = new ChargingBUS();

        checkout = new Checkout();
        checkout.setBookingId(bookingId);
        checkout.setRoomId(roomId);
        checkout.setTotalAmount(amount);
        checkout.setCheckOutDate(now);

        //create CHARGING
        charging = new Charging();
        charging.setChargeID(chargingId);
        charging.setBookingID(bookingId);
        charging.setTpeReturnCode(0);

        zookeeperClient = ZookeeperClient.getInstance();

    }

    @Override
    public void run() {
        try {
            CountDownLatch latch = new CountDownLatch(MAX_CHECK_STATUS_TIME);
            
            ScheduledExecutorService getStatusService = Executors.newSingleThreadScheduledExecutor();

            getStatusService.scheduleAtFixedRate(() -> {
                JSONObject status = ZalopayUtils.getZalopayOrderStatus(charging.getChargeID());
                int returncode = status.getInt("returncode");
                long amount = status.getLong("amount");
                long zptransid = status.getLong("zptransid");
              
                charging.setTotalAmount(amount);
                charging.setTpeReturnCode(returncode);
                charging.setTpeTransID(zptransid);

                if (returncode == 1) {
                    long countLatch = latch.getCount();
                    for (int i = 0; i < countLatch; i++) {
                        latch.countDown();
                    }
                    getStatusService.shutdown();
                } else {
                    latch.countDown();
                }
            }, 0, TIME_BETWEEN_CHECK_STATUS, TimeUnit.MILLISECONDS);
            latch.await();
            getStatusService.shutdown();

            switch (charging.getTpeReturnCode()) {
                case 1:
                    charging.setChargeStatus(ChargeStatus.SUCCESS);
                    checkoutBUS.addCheckout(checkout);
                    break;
                case 0:
                    charging.setChargeStatus(ChargeStatus.FAILED);
                    break;
                default:
                    charging.setChargeStatus(ChargeStatus.EXCEPTION);
                    break;
            }
            
            chargeBUS.addCharging(charging);
            
            //unlock;
            zookeeperClient.deleteNode(checkout.getBookingId());
        } catch (InterruptedException ex) {
            Logger.getLogger(PaymentCheckingThread.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
