/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.app.model;

import baolt.hotel.api.grpc.menu.MenuDeleteParams;
import baolt.hotel.api.grpc.menu.MenuMsg;
import baolt.hotel.api.grpc.menu.MenuGetAllParams;
import baolt.hotel.api.grpc.menu.MenuServicesGrpc;
import static baolt.hotel.app.model.ModelConstansts.*;
import static baolt.hotel.app.model.ModelHelper.createErrorJSONObject;
import baolt.hotel.app.utils.ServerHelper;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import java.util.Date;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class MenuModel {

    private final ManagedChannel channel;
    private final MenuServicesGrpc.MenuServicesBlockingStub blockingStub;

    public MenuModel() {
        this.channel = ModelHelper.getChannel();

        this.blockingStub = MenuServicesGrpc.newBlockingStub(channel);
    }
    
    
    public JSONArray getAllMenu() {
        JSONArray arr = new JSONArray();
        if (channel.isShutdown() || channel.isTerminated()) {
            System.out.println("Server died");
            return arr;
        }
        MenuGetAllParams params = MenuGetAllParams.newBuilder().build();
        try {
            Iterator<MenuMsg> reply = blockingStub.getAll(params);
            while (reply.hasNext()) {
                MenuMsg menu = reply.next();
                JSONObject json = convertMenuMsgToJSON(menu);
                arr.put(json);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return arr;
    }
    
    
    public JSONObject addMenu(JSONObject json) {
        if (channel.isShutdown() || channel.isTerminated()) {
            return createErrorJSONObject("API Server died");
        }
        try {
            MenuMsg request = convertJSONObjectToMenuMsg(json);
            MenuMsg response = blockingStub.add(request);
            return convertMenuMsgToJSON(response);
        }catch (Exception ex) {
            return createErrorJSONObject(ex.getMessage());
        }
    }

    public JSONObject updateMenu(JSONObject json) {
        if (channel.isShutdown() || channel.isTerminated()) {
            return createErrorJSONObject("API Server died");
        }
        try {
            MenuMsg request = convertJSONObjectToMenuMsg(json);
            MenuMsg response = blockingStub.update(request);
            return convertMenuMsgToJSON(response);
        }catch (Exception ex) {
            return createErrorJSONObject(ex.getMessage());
        }
    }

    public JSONObject deleteMenu(String id) {
        if (channel.isShutdown() || channel.isTerminated()) {
            return createErrorJSONObject("API Server died");
        }
        try {
            MenuDeleteParams dlt = MenuDeleteParams.newBuilder().setId(id).build();
            MenuMsg response = blockingStub.delete(dlt);
            return convertMenuMsgToJSON(response);
        } catch (Exception ex) {
            return createErrorJSONObject(ex.getMessage());
        }

    } 
    
    
    public static JSONObject convertMenuMsgToJSON(MenuMsg menu){
        JSONObject json = new JSONObject();
        json.put("menuId", menu.getMenuId());
        json.put("roomId", menu.getRoomId());
        json.put("amount", menu.getAmount());
        json.put("isActive", menu.getIsActive());
        json.put("note", menu.getNote());
        Date date = new Date(menu.getCreateDate());
        json.put("createDate", ServerHelper.getDateStr(date));
        return json;
    }
    
    public static MenuMsg convertJSONObjectToMenuMsg(JSONObject json){
       return  MenuMsg.newBuilder().setMenuId(json.getString("menuId"))
                                      .setRoomId(json.getString("roomId"))
                                      .setIsActive(json.getBoolean("isActive"))
                                      .setNote(json.getString("note"))
                                      .setAmount(json.getLong("amount"))
                                      .setCreateDate(new Date().getTime())
                                      .build();
        
    }

}
