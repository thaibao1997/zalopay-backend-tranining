/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.database;

import baolt.hotel.api.dataacess.RoomDAO;
import baolt.hotel.api.database.DataSource;
import baolt.hotel.api.dto.Menu;
import baolt.hotel.api.dto.Room;
import static baolt.hotel.api.util.APIHelper.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author baolt
 */
public class MenuDB {

    public static final String TABLE_NAME = "Menu";
    private static final String ID_COL = "menuID";
    private static final String ROOM_ID_COL = "roomID";
    private static final String NOTE_COL = "note";
    private static final String ISACTIVE_COL = "isActive";
    private static final String AMOUNT_COL = "amount";
    private static final String CREATE_DATE_COL = "createDate";
    

    private DataSource dataSource = null;

    public MenuDB() {
        dataSource = DataSource.getInstance();
    }

    public static Menu resultSetToMenu(ResultSet rs) throws SQLException {
        Menu menu = new Menu();
        menu.setMenuId(rs.getString(ID_COL));
        menu.setRoomId(rs.getString(ROOM_ID_COL));
        String note = rs.getString(NOTE_COL);
        menu.setNote(note == null ? "" : note);
        menu.setIsActive(rs.getBoolean(ISACTIVE_COL));
        menu.setAmount(rs.getLong(AMOUNT_COL));
        Timestamp create = rs.getTimestamp(CREATE_DATE_COL);
        menu.setCreateDate(convertTimestampToDate(create));
        return menu;
    }

    public ArrayList<Menu> getAllMenus() throws SQLException {
        ArrayList<Menu> menus = new ArrayList<>();
        try (Connection conn = dataSource.getConnection()){
            String query = "Select * from " + TABLE_NAME;
            PreparedStatement st = conn.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Menu menu = resultSetToMenu(rs);
                menus.add(menu);
            }
        } catch (SQLException ex) {
            Logger.getLogger(MenuDB.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
        return menus;
    }

    public Menu getMenu(String id) throws SQLException {
        Menu menu = null;
        try (Connection conn = dataSource.getConnection()){

            String query = "Select * from " + TABLE_NAME + " Where menuId Like ?";
            PreparedStatement st = conn.prepareStatement(query);
            st.setString(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                menu = resultSetToMenu(rs);
                return menu;
            } else {
                return null;
            }
        } catch (SQLException ex) {
            Logger.getLogger(RoomDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } 
    }

    public boolean addMenu(Menu menu) throws SQLException {
        try (Connection conn = dataSource.getConnection()){
            String query = "Insert into " + TABLE_NAME + " values (?,?,?,?,?,?)";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, menu.getMenuId());
            pst.setString(2, menu.getRoomId());
            pst.setLong(3, menu.getAmount());
            pst.setBoolean(4, menu.isIsActive());

            Date createDate = menu.getCreateDate();
            pst.setTimestamp(5, convertDateToTimestamp(createDate));
            pst.setString(6, menu.getNote());
            return pst.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(RoomDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }

    public boolean deleteMenu(String id) throws SQLException {
        try (Connection conn = dataSource.getConnection()){
            String query = "delete from " + TABLE_NAME + " where menuID like ?";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, id);
            return pst.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(RoomDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } 
    }

    public boolean updateMenu(Menu menu) throws SQLException {
        try (Connection conn = dataSource.getConnection()){
            String query = "update " + TABLE_NAME + " set roomID = ?,amount= ? "
                    + ",createDate = ?,isActive = ? ,note = ? "
                    + "where menuID like ?";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, menu.getRoomId());
            pst.setLong(2, menu.getAmount());
            Date createDate = menu.getCreateDate();
            pst.setTimestamp(3, convertDateToTimestamp(createDate));
            pst.setBoolean(4, menu.isIsActive());
            pst.setString(5, menu.getNote());
            pst.setString(6, menu.getMenuId());
            return pst.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(RoomDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } 
    }
    
    
    public void deactiveAllOfRoom(String roomId) throws SQLException{
        try (Connection conn = dataSource.getConnection()){
            String query = "update " + TABLE_NAME + " set isActive = false "
                    + "where roomId like ?";
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, roomId);
            pst.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(RoomDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        }
    }
    
    public Menu getActiveMenuOfRoom(String roomId) throws SQLException {
        try (Connection conn = dataSource.getConnection()){
            String query = "select * from " + MenuDB.TABLE_NAME + " where roomID like ? "
                    + "and isActive = true";
            
            PreparedStatement pst = conn.prepareStatement(query);
            pst.setString(1, roomId);
            ResultSet rs = pst.executeQuery();
            if(rs.next()){
                return MenuDB.resultSetToMenu(rs);
            }else{
                return null;
            }

        } catch (SQLException ex) {
            Logger.getLogger(RoomDAO.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } 
    }

}
