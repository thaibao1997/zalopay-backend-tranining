/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.app.model;

import baolt.hotel.api.grpc.room.RoomDeleteParams;
import baolt.hotel.api.grpc.room.RoomMsg;
import baolt.hotel.api.grpc.room.RoomServicesGrpc;
import baolt.hotel.api.grpc.room.RoomMsg.Builder;
import baolt.hotel.api.grpc.room.RoomMsgType;
import baolt.hotel.api.grpc.room.RoomGetAllParams;
import static baolt.hotel.app.model.ModelConstansts.API_ADDRESS;
import static baolt.hotel.app.model.ModelConstansts.API_PORT;
import baolt.hotel.app.utils.ServerHelper;
import static baolt.hotel.app.utils.ServerHelper.*;
import static baolt.hotel.app.model.ModelHelper.*;

import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusException;
import io.grpc.StatusRuntimeException;
import java.io.IOException;
import java.util.Date;
import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class RoomModel {

    private final ManagedChannel channel;
    private final RoomServicesGrpc.RoomServicesBlockingStub blockingStub;

    public RoomModel() {
        this.channel = ModelHelper.getChannel();
        this.blockingStub = RoomServicesGrpc.newBlockingStub(channel);
    }

    public JSONArray getAllRoom() {
        JSONArray arr = new JSONArray();
        if (channel.isShutdown() || channel.isTerminated()) {
            System.out.println("Server died");
            return arr;
        }
        RoomGetAllParams params = RoomGetAllParams.newBuilder().build();
        try {
            Iterator<RoomMsg> reply = blockingStub.getAll(params);
            while (reply.hasNext()) {
                RoomMsg room = reply.next();
                JSONObject json = convertRoomMsgToJSON(room);
                arr.put(json);
            }

        }catch (Exception ex) {
            ex.printStackTrace();
        }
        return arr;
    }

    public JSONObject addRoom(JSONObject json) {
        if (channel.isShutdown() || channel.isTerminated()) {
            return createErrorJSONObject("API Server died");
        }
        try {
            RoomMsg request = convertJSONObjectToMsg(json);
            RoomMsg response = blockingStub.add(request);
            return convertRoomMsgToJSON(response);
        }catch (Exception ex) {
            return createErrorJSONObject(ex.getMessage());
        }
    }

    public JSONObject updateRoom(JSONObject json) {
        if (channel.isShutdown() || channel.isTerminated()) {
            return createErrorJSONObject("API Server died");
        }
        try {
            RoomMsg request = convertJSONObjectToMsg(json);
            RoomMsg response = blockingStub.update(request);
            return convertRoomMsgToJSON(response);
        }catch (Exception ex) {
            return createErrorJSONObject(ex.getMessage());
        }
    }

    public JSONObject deleteRoom(String id) {
        if (channel.isShutdown() || channel.isTerminated()) {
            return createErrorJSONObject("API Server died");
        }
        try {
            RoomDeleteParams dlt = RoomDeleteParams.newBuilder().setId(id).build();
            RoomMsg response = blockingStub.delete(dlt);
            return convertRoomMsgToJSON(response);
        } catch (Exception ex) {
            return createErrorJSONObject(ex.getMessage());
        }

    }

    public static JSONObject convertRoomMsgToJSON(RoomMsg room) {
        JSONObject json = new JSONObject();
        Builder roomBuilder = room.toBuilder();
        json.put("roomId", room.getRoomId());
        json.put("roomName", room.getRoomName());
        json.put("roomType", room.getRoomType().getNumber() + 1);
        Date createDate = new Date(room.getCreateDate());
        json.put("createDate", ServerHelper.getDateStr(createDate));
        json.put("isActive", room.getIsActive());
        json.put("description", room.getDescription());
        return json;
    }

    public static RoomMsg convertJSONObjectToMsg(JSONObject json) {
        int roomtype = json.getInt("roomType") - 1;
        RoomMsg roomE = RoomMsg.newBuilder()
                .setRoomId(json.getString("roomId"))
                .setRoomName(json.getString("roomName"))
                .setDescription(json.getString("description"))
                .setRoomType(RoomMsgType.forNumber(roomtype))
                .setIsActive(json.getBoolean("isActive"))
                .setCreateDate(new Date().getTime())
                .build();
        return roomE;
    }

}
