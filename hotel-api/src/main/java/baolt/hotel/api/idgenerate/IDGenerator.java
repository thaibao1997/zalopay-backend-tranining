/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.idgenerate;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author baolt
 */
public class IDGenerator {

    private static final int QUEUE_CAPACITY = 20;
    private static final int ID_LENGTH = 9;
    //private static final int MAX_RANGE_ID = 999999999;
    //private static final Random radom = new Random(System.nanoTime());
    private static final IDGenerator instance = new IDGenerator();

    public static IDGenerator getInstance() {
        return instance;
    }

    private LinkedBlockingQueue<String> idQueue;
    private AtomicLong uid;

    private IDGenerator() {
        idQueue = new LinkedBlockingQueue<>(QUEUE_CAPACITY);
        uid = new AtomicLong(0);
    }

    private final Thread idProduceThread = new Thread(new Runnable() {
        @Override
        public void run() {
            while (true) {
                try {
                    long id = uid.incrementAndGet();
                    String nID = IDGenHelper.longToStringFixedLength(id, ID_LENGTH);
                    idQueue.put(nID);
                } catch (InterruptedException ex) {
                    Logger.getLogger(IDGenerator.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
    });

    private ScheduledExecutorService resetIDScheduler = Executors.newSingleThreadScheduledExecutor();

    public void startGenerateID() {
        if (!idProduceThread.isAlive()) {
            idProduceThread.start();

            //reset id ve 1 vao nua dem
            Long midnight = LocalDateTime.now().until(LocalDate.now().plusDays(1).atStartOfDay(), ChronoUnit.SECONDS);
            resetIDScheduler.scheduleAtFixedRate(()->{
                this.uid.set(1);
            }, midnight, 24*60*60, TimeUnit.SECONDS);
        }
    }

    public String getBookingID() {
        try {
            String nID = idQueue.take();
            return IDGenHelper.getBookingIDDateString() + nID;
        } catch (InterruptedException ex) {
            Logger.getLogger(IDGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
