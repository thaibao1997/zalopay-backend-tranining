/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.dto;

import baolt.hotel.api.util.APIHelper;
import static baolt.hotel.api.util.APIHelper.getDateFromStr;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author baolt
 */
public class Booking implements JSONable, Serializable {

    private String bookingId;
    private String customerId;
    private String roomId;
    private Date bookingDate;
    private Date fromDate;
    private Date toDate;
    private BookingStatus bookingStatus = BookingStatus.ACTIVE;

    public Booking() {

        bookingDate = new Date();
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId.toUpperCase();
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId.toUpperCase();
    }

    public String getRoomId() {
        return roomId;
    }

    public void setRoomId(String roomId) {
        this.roomId = roomId.toUpperCase();
    }

    public Date getBookingDate() {
        return bookingDate;
    }

    public void setBookingDate(Date bookingDate) {
        this.bookingDate = bookingDate;
    }

    public Date getFromDate() {
        return fromDate;
    }

    public void setFromDate(Date fromDate) {
        this.fromDate = fromDate;
    }

    public Date getToDate() {
        return toDate;
    }

    public void setToDate(Date toDate) {
        this.toDate = toDate;
    }

    public BookingStatus getBookingStatus() {
        return bookingStatus;
    }

    public void setBookingStatus(BookingStatus bookingStatus) {
        this.bookingStatus = bookingStatus;
    }

    @Override
    public JSONObject toJsonObject() {
        JSONObject jsonO = new JSONObject(this);
        jsonO.put("bookingDate", APIHelper.getDateStr(bookingDate));
        jsonO.put("fromDate", APIHelper.getShortDateStr(fromDate));
        jsonO.put("toDate", APIHelper.getShortDateStr(toDate));
        jsonO.put("bookingStatus", this.bookingStatus.toInt());
        return jsonO;
    }

    @Override
    public void fromJsonString(String jsonStr) throws JSONException, IllegalArgumentException {
        JSONObject jsonObject = new JSONObject(jsonStr);
        String bookingId = jsonObject.getString("bookingId");
        String customerId = jsonObject.getString("customerId");
        String roomId = jsonObject.getString("roomId");

        String dateStr = jsonObject.getString("fromDate");
        Date fromDate = getDateFromStr(dateStr);
        dateStr = jsonObject.getString("toDate");
        Date toDate = getDateFromStr(dateStr);

        if (jsonObject.has("bookingDate")) {
            dateStr = jsonObject.getString("bookingDate");
            Date bookingDate = getDateFromStr(dateStr);
            this.setBookingDate(bookingDate);
        }else{
            this.setBookingDate(new Date());
        }

        if (jsonObject.has("bookingStatus")) {
            int status = jsonObject.getInt("bookingStatus");
            if (status < 1 || status > 2) {
                throw new IllegalArgumentException("bookingStatus illegal");
            }
            this.setBookingStatus(BookingStatus.fromInt(status));
        }

        this.setBookingId(bookingId);
        this.setCustomerId(customerId);
        this.setRoomId(roomId);
        this.setFromDate(fromDate);
        this.setToDate(toDate);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 61 * hash + Objects.hashCode(this.bookingId);
        hash = 61 * hash + Objects.hashCode(this.customerId);
        hash = 61 * hash + Objects.hashCode(this.roomId);
        hash = 61 * hash + Objects.hashCode(this.bookingDate);
        hash = 61 * hash + Objects.hashCode(this.fromDate);
        hash = 61 * hash + Objects.hashCode(this.toDate);
        hash = 61 * hash + Objects.hashCode(this.bookingStatus);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Booking other = (Booking) obj;
        if (!Objects.equals(this.bookingId, other.bookingId)) {
            return false;
        }
        if (!Objects.equals(this.customerId, other.customerId)) {
            return false;
        }
        if (!Objects.equals(this.roomId, other.roomId)) {
            return false;
        }
        if (this.bookingStatus != other.bookingStatus) {
            return false;
        }
        if (Math.abs(this.bookingDate.getTime() - other.bookingDate.getTime()) > 1000) {
            return false;
        }
        if (Math.abs(this.fromDate.getTime() - other.fromDate.getTime()) > 1000) {
            return false;
        }
        if (Math.abs(this.toDate.getTime() - other.toDate.getTime()) > 1000) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Booking{" + "bookingId=" + bookingId + ", customerId=" + customerId + ", roomId=" + roomId + ", bookingDate=" + bookingDate + ", fromDate=" + fromDate + ", toDate=" + toDate + ", bookingStatus=" + bookingStatus + '}';
    }

}
