/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.kafka;

import static baolt.hotel.api.kafka.KafkaConstants.*;
import java.util.Properties;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;

/**
 *
 * @author baolt
 */
public class KafkaConnect {

    
   

    Producer<String, String> producer;

    public KafkaConnect() {
        Properties configProperties = new Properties();
        configProperties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KAFKA_SERVERS);
        configProperties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        configProperties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        producer = new KafkaProducer<>(configProperties);
    }

    public void writeLog(String log) {
        ProducerRecord<String, String> rec = new ProducerRecord<>(TOPIC, null, log);
        producer.send(rec, (RecordMetadata metadata, Exception exception) -> {
            System.out.println("Sent log: " + metadata.topic() + " ,parition: " + metadata.partition() + " stored at offset: "  + metadata.offset());
            ;
        });
    }
}
