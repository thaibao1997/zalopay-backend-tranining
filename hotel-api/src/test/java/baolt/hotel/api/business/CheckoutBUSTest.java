/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.business;

import baolt.hotel.api.database.DataSource;
import baolt.hotel.api.dto.Checkout;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import testUtil.TestUtil;

/**
 *
 * @author baolt
 */
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CheckoutBUSTest {

    CheckoutBUS instance;
    ArrayList<Checkout> testCheckouts;
    Checkout failCheckout;

    public CheckoutBUSTest() {
        instance = new CheckoutBUS();
        testCheckouts = new ArrayList<>();

        Checkout checkout = new Checkout();
        checkout.setBookingId("B01");
        checkout.setRoomId("R01");
        checkout.setCheckOutDate(new Date());
        checkout.setTotalAmount(500000);
        testCheckouts.add(checkout);

        checkout = new Checkout();
        checkout.setBookingId("B02");
        checkout.setRoomId("R01");
        checkout.setCheckOutDate(new Date());
        checkout.setTotalAmount(100000);
        testCheckouts.add(checkout);

        failCheckout = new Checkout();
        failCheckout.setCheckOutDate(new Date());
        failCheckout.setTotalAmount(100000);

    }

    @BeforeClass
    public static void setUpClass() {

        TestUtil.flushAllCaches();

        DataSource.switchToTestDatabase();
        DataSource dataSource = DataSource.getInstance();
        dataSource.dropAllData();

        try (Connection connection = dataSource.getConnection()) {
            Statement st = connection.createStatement();
            String query;

            st = connection.createStatement();
            query = "Insert into Room values ('R01','Room 01',1,NOW(),true,'Phong 001')";
            st.execute(query);

            st = connection.createStatement();
            query = "Insert into Customer values ('C01','Nguyen Van A','VN',NOW(),'123456',NOW())";
            st.execute(query);

            st = connection.createStatement();
            query = "Insert into Booking values ('B01','C01','R01',NOW(),NOW(),NOW(),2)";
            st.execute(query);

            st = connection.createStatement();
            query = "Insert into Booking values ('B02','C01','R01',NOW(),NOW(),NOW(),2)";
            st.execute(query);

        } catch (SQLException ex) {
            Logger.getLogger(RoomBUSTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @AfterClass
    public static void tearDownClass() {
        TestUtil.flushAllCaches();
        DataSource.swicthToMainDatabase();
    }

    /**
     * Test of addCheckout method, of class CheckoutBUS.
     */
    @Test
    public void AA_testAddCheckout() {
        System.out.println("addCheckout");
        BUSResult expResult = new BUSResult(testCheckouts.get(0));
        BUSResult result = instance.addCheckout(testCheckouts.get(0));

        BUSResult expResult1 = new BUSResult(testCheckouts.get(1));
        BUSResult result1 = instance.addCheckout(testCheckouts.get(1));

        assertEquals(expResult, result);
        assertEquals(expResult1, result1);
    }

    @Test
    public void AB_testAddCheckout_FAIL_BOOKING_ID_NOT_FOUND() {
        System.out.println("addCheckout");
        failCheckout.setRoomId("R01");
        failCheckout.setBookingId("B01zzz");

        BUSResult expResult = new BUSResult(CheckoutBUS.BOOKING_ERR_MSG);
        BUSResult result = instance.addCheckout(failCheckout);
        assertEquals(expResult, result);
    }

    @Test
    public void AC_testAddCheckout_FAIL_ROOM_ID_NOT_FOUND() {
        System.out.println("addCheckout");
        failCheckout.setRoomId("R01aaa");
        failCheckout.setBookingId("B01");

        BUSResult expResult = new BUSResult(CheckoutBUS.ROOM_ERR_MSG);
        BUSResult result = instance.addCheckout(failCheckout);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAllCheckout method, of class CheckoutBUS.
     */
    @Test
    public void B_testGetAllCheckout() {
        System.out.println("getAllCheckout");
        Collection<Checkout> result = instance.getAllCheckout();
        assertEquals(testCheckouts.size(), result.size());
    }

    /**
     * Test of getCheckoutByID method, of class CheckoutBUS.
     */
    @Test
    public void C_testGetCheckoutByID() {
        System.out.println("getCheckoutByID");
        Checkout checkout = testCheckouts.get(0);
        String bookingId = checkout.getBookingId();
        String roomId = checkout.getRoomId();
        BUSResult expResult = new BUSResult(checkout);
        BUSResult result = instance.getCheckoutByID(bookingId, roomId);
        assertEquals(expResult, result);
    }

    /**
     * Test of updateCheckout method, of class CheckoutBUS.
     */
    @Test
    public void D_testUpdateCheckout() {
        System.out.println("updateCheckout");
        Checkout checkout = testCheckouts.get(0);
        checkout.setTotalAmount(50000);
        BUSResult expResult = new BUSResult(checkout);
        BUSResult result = instance.updateCheckout(checkout);
        assertEquals(expResult, result);
    }

    /**
     * Test of deleteCheckout method, of class CheckoutBUS.
     */
    @Test
    public void D_testDeleteCheckout() {
        System.out.println("deleteCheckout");
        Checkout checkout = testCheckouts.get(1);
        String bookingId = checkout.getBookingId();
        String roomId = checkout.getRoomId();
        BUSResult expResult = new BUSResult(checkout);
        BUSResult result = instance.deleteCheckout(bookingId, roomId);
        assertEquals(expResult, result);
    }

}
