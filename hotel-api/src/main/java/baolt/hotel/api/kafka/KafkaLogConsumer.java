/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.kafka;

import static baolt.hotel.api.kafka.KafkaConstants.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collections;
import java.util.Properties;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.logging.log4j.*;
import org.apache.logging.log4j.core.config.ConfigurationSource;
import org.apache.logging.log4j.core.config.Configurator;

/**
 *
 * @author baolt gia lap kafka consumer ghi log xuong file
 */
public class KafkaLogConsumer {

    public static final String CONFIG_FILE = System.getProperty("user.dir") + File.separator +"config/log4j2.properties";

    private Thread consumerThread;
    private Consumer<String, String> consumer;
    private Logger logger;

    public KafkaLogConsumer() {
        final Properties props = new Properties();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
                KAFKA_SERVERS);
        props.put(ConsumerConfig.GROUP_ID_CONFIG, "log_file");
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer.class.getName());
        props.put(ConsumerConfig.CLIENT_ID_CONFIG, "log_consumer");

        consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Collections.singletonList(TOPIC));

//        consumer.poll(0);
//
//        consumer.seekToBeginning(consumer.assignment());

        Configurator.initialize(null, CONFIG_FILE);
        logger = LogManager.getLogger("kafkaConsumer");
        consumerThread = new Thread(() -> {
            while (true) {
                final ConsumerRecords<String, String> consumerRecords
                        = consumer.poll(100);
                consumerRecords.forEach(record -> {
                    String log = record.value();
                    logger.info(log);
                });
            }
        });
    }

    public void startConsume() {
        if (!consumerThread.isAlive()) {
            consumerThread.start();
        }
    }
}
