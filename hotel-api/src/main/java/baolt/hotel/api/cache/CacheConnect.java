/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.cache;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.redisson.Redisson;
import org.redisson.api.LocalCachedMapOptions;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

/**
 *
 * @author baolt
 */
public class CacheConnect {

    public static final int MAX_TRY_LOCK_TIME = 100;//Second
    public static final int UNLOCK_AFTER_TIME = 10;//Second

    private static RedissonClient client;
    private static LocalCachedMapOptions options;

    synchronized public static RedissonClient getRedissonClient() {
        if (client == null) {
            try {
                File configF = new File("config/redis.json");
                Config config = null;
                if (configF.exists()) {
                    config = Config.fromJSON(configF);
                } else {
                    config = new Config();
                    config.useSingleServer().setAddress("redis://localhost:6379");
                }
                client = Redisson.create(config);
            } catch (IOException ex) {
                Logger.getLogger(CacheConnect.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        return client;
    }

    synchronized public static LocalCachedMapOptions getLocalCachedMapOption() {
        if (options == null) {
            options = LocalCachedMapOptions.defaults()
                    .evictionPolicy(LocalCachedMapOptions.EvictionPolicy.LFU)
                    .cacheSize(1000).syncStrategy(LocalCachedMapOptions.SyncStrategy.UPDATE)
                    .timeToLive(100, TimeUnit.SECONDS).maxIdle(100, TimeUnit.SECONDS);
        }
        return options;
    }

}
