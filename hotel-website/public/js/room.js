
var table;
$(document).ready(function () {

    table= $('#contentTable').DataTable();



    $('#addForm').on('submit', function (e) {
        e.preventDefault();
        var roomId = $('#roomId').val();
        var roomType = parseInt($('#roomType').val());
        var roomName = $('#roomName').val();
        var isActive = $('#isActive').val() === "1" ? true : false;
        var description = $('#description').val();

        var object = {
            roomId: roomId, roomType: roomType, roomName: roomName
            , isActive: isActive, description: description
        }

    
        $.ajax({
            type: 'POST',
            data: JSON.stringify(object),
            url: '../room',
            success: function (result) {
                var obj = JSON.parse(result);
                if (obj.error) {
                    alert(obj.errorMsg);
                } else {
                    var html = "<tr id= :roomId>" +
                        "<td>:roomId</td>" +
                        "<td>:roomName</td>" +
                        "<td>:roomType</td>" +
                        "<td>:createDate</td>" +
                        '<td>:isActive</td>' +
                        "<td>:description</td>"
                        + '<td><button  onclick="updateClick(this)"  data-toggle="modal" data-target="#updateModal" class="btn btn-primary"><i class="fas fa-pencil-alt"></i> Edit</button>'
                        + '<button class="btn btn-danger" onclick="deleteClick(this)"><i class="fas fa-trash-alt"></i> Delete</button></td></tr>';
                    console.log(obj);
                    html = html.replace(':roomId', obj.roomId);
                    html = html.replace(':roomId', obj.roomId);
                    html = html.replace(':roomName', obj.roomName);
                    var roomType = getRoomType(obj.roomType);
                    html = html.replace(':roomType', roomType);
                    html = html.replace(':createDate', obj.createDate);
                    html = html.replace(':isActive', obj.isActive ? "Yes" : "No");
                    html = html.replace(':description', obj.description);
                    table.row.add($(html)).draw();
                    $('#closeModal').click();
                }
            }
        });
    });
    
    $('#updateForm').on('submit', function (e) {
        e.preventDefault();
        var roomId = $('#roomIdE').val();
        var roomType = parseInt($('#roomTypeE').val());
        var roomName = $('#roomNameE').val();
        var isActive = $('#isActiveE').val() === "1" ? true : false;
        var description = $('#descriptionE').val();
        var object = {
            roomId: roomId, roomType: roomType, roomName: roomName
            , isActive: isActive, description: description
        }
        console.log("AJAX");
        $.ajax({
            type: 'PATCH',
            data: JSON.stringify(object),
            url: '../room?id=' + roomId,
            success: function (result) {
                var obj = JSON.parse(result);
                if (obj.error) {
                    alert(obj.errorMsg);
                } else {
                    alert("sussess");
                    var node = document.getElementById(roomId);
                    var nodeList = node.getElementsByTagName("td");
                    nodeList[1].innerText = obj.roomName;
                    var roomType = getRoomType(obj.roomType);
                    nodeList[2].innerText = roomType;
                    nodeList[3].innerText = obj.createDate;
                    nodeList[4].innerText = obj.isActive ? "Yes" : "No";
                    nodeList[5].innerText = obj.description;   
                    $('#closeUpdateModal').click();

                }
            }
        });
    });
});

function deleteClick(e) {
    var id = e.parentNode.parentNode.getAttributeNode('id').value;
    if (confirm("Are you sure to delete room: " + id)) {

        console.log("delete room " + id);
        $.ajax({
            type: 'DELETE',
            url: '../room?id=' + id,
            success: function (result) {
                var obj = JSON.parse(result);
                if (obj.error) {
                    alert(obj.errorMsg);
                } else {
                    table.row('#' + id).remove().draw();
                }
            }

        });
    }
}

function getRoomType(number) {
    var roomType = "Tap The";
    if (number == 1)
        roomType = "Single";
    else if (number == 2)
        roomType = "Double";
    else if (number == 3)
        roomType = "Three";
    return roomType;
}


function updateClick(e) {
    var parentTr = e.parentNode.parentNode;
    var nodeList = parentTr.getElementsByTagName("td");
    console.log(nodeList);
    $('#roomIdE').val(nodeList[0].innerText.toString().trim());
    $("#roomNameE").val(nodeList[1].innerText.toString().trim());
    var type = nodeList[2].innerText.toString().trim().toLowerCase();
    if (type === 'single')
        type = 1;
    else if (type === 'double') {
        type = 2;
    } else if (type === 'three') {
        type = 3;
    } else if (type === 'tap the') {
        type = 4;
    }
    $('#roomTypeE').val(type);
    $('#isActiveE').val(nodeList[4].innerText.toString().trim() === "Yes" ? 1 : 0);
    $('#descriptionE').val(nodeList[5].innerText.toString().trim());
}


