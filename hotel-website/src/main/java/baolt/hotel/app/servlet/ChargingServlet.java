/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.app.servlet;

import baolt.hotel.app.model.ChargingModel;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author baolt
 */
public class ChargingServlet extends RestHttpServlet {

    ChargingModel chargingModel = new ChargingModel();

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        if (id == null || id.isEmpty()) {
            resp.sendError(400);
            return;
        }
        try {
            String res = chargingModel.getPaymentStatus(id).toString();
            resp.getWriter().print(res);
        } catch (Exception e) {
            resp.sendError(500);
        }
    }

}
