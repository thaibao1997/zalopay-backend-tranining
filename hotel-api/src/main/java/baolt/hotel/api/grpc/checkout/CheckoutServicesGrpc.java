package baolt.hotel.api.grpc.checkout;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.13.0-SNAPSHOT)",
    comments = "Source: Checkout.proto")
public final class CheckoutServicesGrpc {

  private CheckoutServicesGrpc() {}

  public static final String SERVICE_NAME = "CheckoutServices";

  // Static method descriptors that strictly reflect the proto.
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getGetAllMethod()} instead. 
  public static final io.grpc.MethodDescriptor<baolt.hotel.api.grpc.checkout.CheckoutGetAllParams,
      baolt.hotel.api.grpc.checkout.CheckoutMsg> METHOD_GET_ALL = getGetAllMethodHelper();

  private static volatile io.grpc.MethodDescriptor<baolt.hotel.api.grpc.checkout.CheckoutGetAllParams,
      baolt.hotel.api.grpc.checkout.CheckoutMsg> getGetAllMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.checkout.CheckoutGetAllParams,
      baolt.hotel.api.grpc.checkout.CheckoutMsg> getGetAllMethod() {
    return getGetAllMethodHelper();
  }

  private static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.checkout.CheckoutGetAllParams,
      baolt.hotel.api.grpc.checkout.CheckoutMsg> getGetAllMethodHelper() {
    io.grpc.MethodDescriptor<baolt.hotel.api.grpc.checkout.CheckoutGetAllParams, baolt.hotel.api.grpc.checkout.CheckoutMsg> getGetAllMethod;
    if ((getGetAllMethod = CheckoutServicesGrpc.getGetAllMethod) == null) {
      synchronized (CheckoutServicesGrpc.class) {
        if ((getGetAllMethod = CheckoutServicesGrpc.getGetAllMethod) == null) {
          CheckoutServicesGrpc.getGetAllMethod = getGetAllMethod = 
              io.grpc.MethodDescriptor.<baolt.hotel.api.grpc.checkout.CheckoutGetAllParams, baolt.hotel.api.grpc.checkout.CheckoutMsg>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(
                  "CheckoutServices", "getAll"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.checkout.CheckoutGetAllParams.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.checkout.CheckoutMsg.getDefaultInstance()))
                  .setSchemaDescriptor(new CheckoutServicesMethodDescriptorSupplier("getAll"))
                  .build();
          }
        }
     }
     return getGetAllMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getAddMethod()} instead. 
  public static final io.grpc.MethodDescriptor<baolt.hotel.api.grpc.checkout.CheckoutMsg,
      baolt.hotel.api.grpc.checkout.CheckoutMsg> METHOD_ADD = getAddMethodHelper();

  private static volatile io.grpc.MethodDescriptor<baolt.hotel.api.grpc.checkout.CheckoutMsg,
      baolt.hotel.api.grpc.checkout.CheckoutMsg> getAddMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.checkout.CheckoutMsg,
      baolt.hotel.api.grpc.checkout.CheckoutMsg> getAddMethod() {
    return getAddMethodHelper();
  }

  private static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.checkout.CheckoutMsg,
      baolt.hotel.api.grpc.checkout.CheckoutMsg> getAddMethodHelper() {
    io.grpc.MethodDescriptor<baolt.hotel.api.grpc.checkout.CheckoutMsg, baolt.hotel.api.grpc.checkout.CheckoutMsg> getAddMethod;
    if ((getAddMethod = CheckoutServicesGrpc.getAddMethod) == null) {
      synchronized (CheckoutServicesGrpc.class) {
        if ((getAddMethod = CheckoutServicesGrpc.getAddMethod) == null) {
          CheckoutServicesGrpc.getAddMethod = getAddMethod = 
              io.grpc.MethodDescriptor.<baolt.hotel.api.grpc.checkout.CheckoutMsg, baolt.hotel.api.grpc.checkout.CheckoutMsg>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "CheckoutServices", "add"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.checkout.CheckoutMsg.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.checkout.CheckoutMsg.getDefaultInstance()))
                  .setSchemaDescriptor(new CheckoutServicesMethodDescriptorSupplier("add"))
                  .build();
          }
        }
     }
     return getAddMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getUpdateMethod()} instead. 
  public static final io.grpc.MethodDescriptor<baolt.hotel.api.grpc.checkout.CheckoutMsg,
      baolt.hotel.api.grpc.checkout.CheckoutMsg> METHOD_UPDATE = getUpdateMethodHelper();

  private static volatile io.grpc.MethodDescriptor<baolt.hotel.api.grpc.checkout.CheckoutMsg,
      baolt.hotel.api.grpc.checkout.CheckoutMsg> getUpdateMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.checkout.CheckoutMsg,
      baolt.hotel.api.grpc.checkout.CheckoutMsg> getUpdateMethod() {
    return getUpdateMethodHelper();
  }

  private static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.checkout.CheckoutMsg,
      baolt.hotel.api.grpc.checkout.CheckoutMsg> getUpdateMethodHelper() {
    io.grpc.MethodDescriptor<baolt.hotel.api.grpc.checkout.CheckoutMsg, baolt.hotel.api.grpc.checkout.CheckoutMsg> getUpdateMethod;
    if ((getUpdateMethod = CheckoutServicesGrpc.getUpdateMethod) == null) {
      synchronized (CheckoutServicesGrpc.class) {
        if ((getUpdateMethod = CheckoutServicesGrpc.getUpdateMethod) == null) {
          CheckoutServicesGrpc.getUpdateMethod = getUpdateMethod = 
              io.grpc.MethodDescriptor.<baolt.hotel.api.grpc.checkout.CheckoutMsg, baolt.hotel.api.grpc.checkout.CheckoutMsg>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "CheckoutServices", "update"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.checkout.CheckoutMsg.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.checkout.CheckoutMsg.getDefaultInstance()))
                  .setSchemaDescriptor(new CheckoutServicesMethodDescriptorSupplier("update"))
                  .build();
          }
        }
     }
     return getUpdateMethod;
  }
  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  @java.lang.Deprecated // Use {@link #getDeleteMethod()} instead. 
  public static final io.grpc.MethodDescriptor<baolt.hotel.api.grpc.checkout.CheckoutDeleteParams,
      baolt.hotel.api.grpc.checkout.CheckoutMsg> METHOD_DELETE = getDeleteMethodHelper();

  private static volatile io.grpc.MethodDescriptor<baolt.hotel.api.grpc.checkout.CheckoutDeleteParams,
      baolt.hotel.api.grpc.checkout.CheckoutMsg> getDeleteMethod;

  @io.grpc.ExperimentalApi("https://github.com/grpc/grpc-java/issues/1901")
  public static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.checkout.CheckoutDeleteParams,
      baolt.hotel.api.grpc.checkout.CheckoutMsg> getDeleteMethod() {
    return getDeleteMethodHelper();
  }

  private static io.grpc.MethodDescriptor<baolt.hotel.api.grpc.checkout.CheckoutDeleteParams,
      baolt.hotel.api.grpc.checkout.CheckoutMsg> getDeleteMethodHelper() {
    io.grpc.MethodDescriptor<baolt.hotel.api.grpc.checkout.CheckoutDeleteParams, baolt.hotel.api.grpc.checkout.CheckoutMsg> getDeleteMethod;
    if ((getDeleteMethod = CheckoutServicesGrpc.getDeleteMethod) == null) {
      synchronized (CheckoutServicesGrpc.class) {
        if ((getDeleteMethod = CheckoutServicesGrpc.getDeleteMethod) == null) {
          CheckoutServicesGrpc.getDeleteMethod = getDeleteMethod = 
              io.grpc.MethodDescriptor.<baolt.hotel.api.grpc.checkout.CheckoutDeleteParams, baolt.hotel.api.grpc.checkout.CheckoutMsg>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(
                  "CheckoutServices", "delete"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.checkout.CheckoutDeleteParams.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  baolt.hotel.api.grpc.checkout.CheckoutMsg.getDefaultInstance()))
                  .setSchemaDescriptor(new CheckoutServicesMethodDescriptorSupplier("delete"))
                  .build();
          }
        }
     }
     return getDeleteMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static CheckoutServicesStub newStub(io.grpc.Channel channel) {
    return new CheckoutServicesStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static CheckoutServicesBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new CheckoutServicesBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static CheckoutServicesFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new CheckoutServicesFutureStub(channel);
  }

  /**
   */
  public static abstract class CheckoutServicesImplBase implements io.grpc.BindableService {

    /**
     */
    public void getAll(baolt.hotel.api.grpc.checkout.CheckoutGetAllParams request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.checkout.CheckoutMsg> responseObserver) {
      asyncUnimplementedUnaryCall(getGetAllMethodHelper(), responseObserver);
    }

    /**
     */
    public void add(baolt.hotel.api.grpc.checkout.CheckoutMsg request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.checkout.CheckoutMsg> responseObserver) {
      asyncUnimplementedUnaryCall(getAddMethodHelper(), responseObserver);
    }

    /**
     */
    public void update(baolt.hotel.api.grpc.checkout.CheckoutMsg request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.checkout.CheckoutMsg> responseObserver) {
      asyncUnimplementedUnaryCall(getUpdateMethodHelper(), responseObserver);
    }

    /**
     */
    public void delete(baolt.hotel.api.grpc.checkout.CheckoutDeleteParams request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.checkout.CheckoutMsg> responseObserver) {
      asyncUnimplementedUnaryCall(getDeleteMethodHelper(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetAllMethodHelper(),
            asyncServerStreamingCall(
              new MethodHandlers<
                baolt.hotel.api.grpc.checkout.CheckoutGetAllParams,
                baolt.hotel.api.grpc.checkout.CheckoutMsg>(
                  this, METHODID_GET_ALL)))
          .addMethod(
            getAddMethodHelper(),
            asyncUnaryCall(
              new MethodHandlers<
                baolt.hotel.api.grpc.checkout.CheckoutMsg,
                baolt.hotel.api.grpc.checkout.CheckoutMsg>(
                  this, METHODID_ADD)))
          .addMethod(
            getUpdateMethodHelper(),
            asyncUnaryCall(
              new MethodHandlers<
                baolt.hotel.api.grpc.checkout.CheckoutMsg,
                baolt.hotel.api.grpc.checkout.CheckoutMsg>(
                  this, METHODID_UPDATE)))
          .addMethod(
            getDeleteMethodHelper(),
            asyncUnaryCall(
              new MethodHandlers<
                baolt.hotel.api.grpc.checkout.CheckoutDeleteParams,
                baolt.hotel.api.grpc.checkout.CheckoutMsg>(
                  this, METHODID_DELETE)))
          .build();
    }
  }

  /**
   */
  public static final class CheckoutServicesStub extends io.grpc.stub.AbstractStub<CheckoutServicesStub> {
    private CheckoutServicesStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CheckoutServicesStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CheckoutServicesStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CheckoutServicesStub(channel, callOptions);
    }

    /**
     */
    public void getAll(baolt.hotel.api.grpc.checkout.CheckoutGetAllParams request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.checkout.CheckoutMsg> responseObserver) {
      asyncServerStreamingCall(
          getChannel().newCall(getGetAllMethodHelper(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void add(baolt.hotel.api.grpc.checkout.CheckoutMsg request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.checkout.CheckoutMsg> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getAddMethodHelper(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void update(baolt.hotel.api.grpc.checkout.CheckoutMsg request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.checkout.CheckoutMsg> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getUpdateMethodHelper(), getCallOptions()), request, responseObserver);
    }

    /**
     */
    public void delete(baolt.hotel.api.grpc.checkout.CheckoutDeleteParams request,
        io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.checkout.CheckoutMsg> responseObserver) {
      asyncUnaryCall(
          getChannel().newCall(getDeleteMethodHelper(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class CheckoutServicesBlockingStub extends io.grpc.stub.AbstractStub<CheckoutServicesBlockingStub> {
    private CheckoutServicesBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CheckoutServicesBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CheckoutServicesBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CheckoutServicesBlockingStub(channel, callOptions);
    }

    /**
     */
    public java.util.Iterator<baolt.hotel.api.grpc.checkout.CheckoutMsg> getAll(
        baolt.hotel.api.grpc.checkout.CheckoutGetAllParams request) {
      return blockingServerStreamingCall(
          getChannel(), getGetAllMethodHelper(), getCallOptions(), request);
    }

    /**
     */
    public baolt.hotel.api.grpc.checkout.CheckoutMsg add(baolt.hotel.api.grpc.checkout.CheckoutMsg request) {
      return blockingUnaryCall(
          getChannel(), getAddMethodHelper(), getCallOptions(), request);
    }

    /**
     */
    public baolt.hotel.api.grpc.checkout.CheckoutMsg update(baolt.hotel.api.grpc.checkout.CheckoutMsg request) {
      return blockingUnaryCall(
          getChannel(), getUpdateMethodHelper(), getCallOptions(), request);
    }

    /**
     */
    public baolt.hotel.api.grpc.checkout.CheckoutMsg delete(baolt.hotel.api.grpc.checkout.CheckoutDeleteParams request) {
      return blockingUnaryCall(
          getChannel(), getDeleteMethodHelper(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class CheckoutServicesFutureStub extends io.grpc.stub.AbstractStub<CheckoutServicesFutureStub> {
    private CheckoutServicesFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private CheckoutServicesFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected CheckoutServicesFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new CheckoutServicesFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<baolt.hotel.api.grpc.checkout.CheckoutMsg> add(
        baolt.hotel.api.grpc.checkout.CheckoutMsg request) {
      return futureUnaryCall(
          getChannel().newCall(getAddMethodHelper(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<baolt.hotel.api.grpc.checkout.CheckoutMsg> update(
        baolt.hotel.api.grpc.checkout.CheckoutMsg request) {
      return futureUnaryCall(
          getChannel().newCall(getUpdateMethodHelper(), getCallOptions()), request);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<baolt.hotel.api.grpc.checkout.CheckoutMsg> delete(
        baolt.hotel.api.grpc.checkout.CheckoutDeleteParams request) {
      return futureUnaryCall(
          getChannel().newCall(getDeleteMethodHelper(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_ALL = 0;
  private static final int METHODID_ADD = 1;
  private static final int METHODID_UPDATE = 2;
  private static final int METHODID_DELETE = 3;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final CheckoutServicesImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(CheckoutServicesImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_ALL:
          serviceImpl.getAll((baolt.hotel.api.grpc.checkout.CheckoutGetAllParams) request,
              (io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.checkout.CheckoutMsg>) responseObserver);
          break;
        case METHODID_ADD:
          serviceImpl.add((baolt.hotel.api.grpc.checkout.CheckoutMsg) request,
              (io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.checkout.CheckoutMsg>) responseObserver);
          break;
        case METHODID_UPDATE:
          serviceImpl.update((baolt.hotel.api.grpc.checkout.CheckoutMsg) request,
              (io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.checkout.CheckoutMsg>) responseObserver);
          break;
        case METHODID_DELETE:
          serviceImpl.delete((baolt.hotel.api.grpc.checkout.CheckoutDeleteParams) request,
              (io.grpc.stub.StreamObserver<baolt.hotel.api.grpc.checkout.CheckoutMsg>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class CheckoutServicesBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    CheckoutServicesBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return baolt.hotel.api.grpc.checkout.CheckoutProto.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("CheckoutServices");
    }
  }

  private static final class CheckoutServicesFileDescriptorSupplier
      extends CheckoutServicesBaseDescriptorSupplier {
    CheckoutServicesFileDescriptorSupplier() {}
  }

  private static final class CheckoutServicesMethodDescriptorSupplier
      extends CheckoutServicesBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    CheckoutServicesMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (CheckoutServicesGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new CheckoutServicesFileDescriptorSupplier())
              .addMethod(getGetAllMethodHelper())
              .addMethod(getAddMethodHelper())
              .addMethod(getUpdateMethodHelper())
              .addMethod(getDeleteMethodHelper())
              .build();
        }
      }
    }
    return result;
  }
}
