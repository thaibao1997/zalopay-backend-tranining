/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.business;

import java.util.Objects;

/**
 *
 * @author baolt
 */
public class BUSResult {
    public boolean err = true;
    public Object returnObj= null;
    public String errMsg = "";

    public BUSResult(Object returnObj,boolean err,String errMsg) {
        this.err = err;
        this.returnObj = returnObj;
        this.errMsg = errMsg;
    }
    
    //oke constructor
    public BUSResult(Object returnObj) {
        this(returnObj,false,null);
    }
    
    //err constructor
    public BUSResult(String errMsg){
        this(null,true,errMsg);
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 19 * hash + (this.err ? 1 : 0);
        hash = 19 * hash + Objects.hashCode(this.returnObj);
        hash = 19 * hash + Objects.hashCode(this.errMsg);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final BUSResult other = (BUSResult) obj;
        if (this.err != other.err) {
            return false;
        }
        if (!Objects.equals(this.errMsg, other.errMsg)) {
            return false;
        }
        if (!Objects.equals(this.returnObj, other.returnObj)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "BUSResult{" + "err=" + err + ", returnObj=" + returnObj + ", errMsg=" + errMsg + '}';
    }
    
    
}
