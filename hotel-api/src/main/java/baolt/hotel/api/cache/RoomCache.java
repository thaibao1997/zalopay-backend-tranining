/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.cache;

import static baolt.hotel.api.cache.CacheConnect.MAX_TRY_LOCK_TIME;
import static baolt.hotel.api.cache.CacheConnect.UNLOCK_AFTER_TIME;
import baolt.hotel.api.dto.Room;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.redisson.api.LocalCachedMapOptions;
import org.redisson.api.RLocalCachedMap;
import org.redisson.api.RReadWriteLock;
import org.redisson.api.RedissonClient;

/**
 *
 * @author baolt
 */
public class RoomCache {

    public static final String KEY = "HOTEL_API_ROOMS";
    public static final String LOCK_NAME = "ROOM_LOCK";

    private final RLocalCachedMap<String, Room> cachedRooms;
    private final RReadWriteLock rwlock;

    private static RoomCache instance = new RoomCache();

    public static RoomCache getInstance() {
        return instance;
    }

    private RoomCache() {
        RedissonClient client = CacheConnect.getRedissonClient();
        cachedRooms = client.getLocalCachedMap(KEY, CacheConnect.getLocalCachedMapOption());
        rwlock = client.getReadWriteLock(LOCK_NAME);
        flushCaches();
    }

    public void flushCaches() {
        if (cachedRooms.isExists()) {
            cachedRooms.clear();
            cachedRooms.expire(0, TimeUnit.SECONDS);
        }
    }

    private void unlockReadLock() {
        if (rwlock.readLock().isHeldByCurrentThread()) {
            rwlock.readLock().unlock();
        }
    }

    private void unlockWriteLock() {
        if (rwlock.writeLock().isHeldByCurrentThread()) {
            rwlock.writeLock().unlock();
        }
    }

    public boolean isEmpty() {
        return cachedRooms.isEmpty();
    }

    public boolean isExist() {
        return cachedRooms.isExists();
    }

    public boolean addRoom(Room room) {
        if (room == null) {
            return false;
        }
        try {
            if (rwlock.writeLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                return cachedRooms.fastPut(room.getRoomId(), room);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(RoomCache.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            unlockWriteLock();
        }
        return false;
    }

    public boolean updateRoom(Room room) {
        if (room == null) {
            return false;
        }
        try {
            if (rwlock.writeLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                return cachedRooms.fastReplace(room.getRoomId(), room);
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(RoomCache.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            unlockWriteLock();
        }
        return false;
    }

    public Room findRoomByID(String id) {
        try {
            if (rwlock.readLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                return cachedRooms.get(id);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            unlockReadLock();
        }
        return null;
    }

    public boolean deleteRoom(String id) {
        try {
            if (rwlock.writeLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                Room room = cachedRooms.get(id);
                return cachedRooms.fastRemove(id) > 0;
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(RoomCache.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            unlockWriteLock();
        }
        return false;
    }

    public Collection<Room> toCollection() {
        try {
            if (rwlock.readLock().tryLock(MAX_TRY_LOCK_TIME, UNLOCK_AFTER_TIME, TimeUnit.SECONDS)) {
                return cachedRooms.values();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            unlockReadLock();
        }
        return null;
    }

    public void fromList(ArrayList<Room> rooms) {
        for (Room room : rooms) {
            addRoom(room);
        }
    }

}
