/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.api.business;

import baolt.hotel.api.dataacess.RoomDAO;
import baolt.hotel.api.dto.Room;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author baolt
 */
public class RoomBUS {

    private RoomDAO roomDAO;

    public RoomBUS() {
        roomDAO = new RoomDAO();
    }

    public Collection<Room> getAllRooms() {
        try {
            return roomDAO.getAllRooms();
        } catch (SQLException ex) {
            Logger.getLogger(RoomBUS.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public BUSResult getRoomByID(String id) {
        if (id.isEmpty()) {
            return BUSHelper.NULL_ID_ERR;
        }
        try {
            Room room = roomDAO.getRoom(id);
            if(room != null)
                return new BUSResult(room);
            else
                return BUSHelper.ID_NOT_FOUND_ERR;
        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, "");
        }
    }

    public BUSResult addRoom(Room room) {
        try {
            if(roomDAO.getRoom(room.getRoomId()) != null){
                return BUSHelper.DUPLICATE_ID_ERR;
            }
            if (roomDAO.addRom(room)) {
                return new BUSResult(room);
            }
        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, "");
        }
        return BUSHelper.DB_ERR;
    }

    public BUSResult deleteRoom(String id) {
        try {
            Room room = roomDAO.getRoom(id);
            if (room == null) {
                return BUSHelper.ID_NOT_FOUND_ERR;
            }
            if(roomDAO.deleteRoom(id))
                return new BUSResult(room);
        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, "");
        }
        return BUSHelper.DB_ERR;

    }

    public BUSResult updateRoom(Room room) {
        try {
            String id = room.getRoomId();
            Room room_ = roomDAO.getRoom(id);
            if (room_ == null) {
                return BUSHelper.ID_NOT_FOUND_ERR;
            }
            room.setCreateDate(room_.getCreateDate());
            if(roomDAO.updateRoom(room))
                return new BUSResult(room);
        } catch (SQLException ex) {
            return BUSHelper.getResultFromException(ex, "");
        }
        return BUSHelper.DB_ERR;
    }

}
