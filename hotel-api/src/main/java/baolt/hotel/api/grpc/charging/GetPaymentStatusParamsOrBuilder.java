// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: Charging.proto

package baolt.hotel.api.grpc.charging;

public interface GetPaymentStatusParamsOrBuilder extends
    // @@protoc_insertion_point(interface_extends:GetPaymentStatusParams)
    com.google.protobuf.MessageOrBuilder {

  /**
   * <code>string chargeId = 1;</code>
   */
  java.lang.String getChargeId();
  /**
   * <code>string chargeId = 1;</code>
   */
  com.google.protobuf.ByteString
      getChargeIdBytes();
}
