    /*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package baolt.hotel.app.view;

import baolt.hotel.app.utils.ServerHelper;
import hapax.Template;
import hapax.TemplateDataDictionary;
import hapax.TemplateDictionary;
import hapax.TemplateException;
import hapax.TemplateLoader;
import hapax.TemplateResourceLoader;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;

/**
 *
 * @author baolt
 */
public class ViewRenderer {

    private static final String mainTemplateName = "maintemplate.xtm";
    public static final String VIEWS_FOLDER = "src/main/java/baolt/hotel/app/view/Views/";
    public static final String JS_SCRIPT_LINK = "<script src=':link_src'></script>";
    public static final String JS_FODLER = "./";

    public static String getMyPage(String title, String content, String footter, String user) {
        try {
            //Template load file
            TemplateLoader templateLoader = TemplateResourceLoader.create(VIEWS_FOLDER, true);
            Template template = templateLoader.getTemplate(mainTemplateName);
            TemplateDictionary templateDictionary = new TemplateDictionary();

            if (user != null && !user.isEmpty()) {
                templateDictionary.addSection("navbar")
                        .addSection("userlogout").setVariable("USER", user);
            }
            templateDictionary.setVariable("TITLE", title);
            templateDictionary.setVariable("CONTENT", content);
            templateDictionary.setVariable("FOOTER", footter);
            return template.renderToString(templateDictionary);

        } catch (TemplateException ex) {
            Logger.getLogger(ViewRenderer.class.getName()).log(Level.SEVERE, null, ex);
        }
        //Use TemplateDictionary to put to xtm

        return null;
    }

    public static String getJSLink(String jsfile) {
        String link = JS_SCRIPT_LINK.replace(":link_src", jsfile);
        return link;
    }
}
